'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Node = undefined;

var _service = require('../service');

var _service2 = _interopRequireDefault(_service);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @class
 * @classdesc Class representing a Sentence Tree Node
 */
class Node {
  constructor(pos = '', word = '', children = [], paren = null) {
    this._pos = pos;
    this._word = word;
    this._children = children;
    this._parent = paren;
  }

  pos() {
    return this._pos;
  }

  posInfo() {
    // itf it's a subtree
    if (this._children.length) {
      return _service2.default.getSentenceParseInfo(this._pos, this.getLanguageISO());
    }

    // if it's a leaf
    return _service2.default.getTokenPosInfo(this._pos, this.getLanguageISO());
  }

  token(token = null) {
    if (token) {
      this._token = token;
    }

    return this._token;
  }

  word() {
    return this._word;
  }

  /**
   * Sets the language ISO (given by the pipeline during the annotation process)
   * This is solely to keep track of the language chosen for further analysis
   * @return {string} text
   */
  setLanguageISO(iso) {
    this._language = iso;
  }

  /**
   * Retrieves the language ISO
   * @return {string} text
   */
  getLanguageISO() {
    return this._language;
  }

  sentimentValue(value = null) {
    if (value) {
      this._sentimentValue = value;
    }

    return this._sentimentValue;
  }

  sentimentProbability(value = null) {
    if (value) {
      this._sentimentProbability = value;
    }
    
    return this._sentimentProbability;
  }

  children() {
    return this._children;
  }

  appendChild(node) {
    this._children.push(node);
  }

  unshiftChild(node) {
    this._children.unshift(node);
  }

  removeChild(node) {
    this._children.splice(this._children.indexOf(node), 1);
  }

  parent(paren = null) {
    if (paren) {
      this._parent = paren;
    }

    return this._parent;
  }

  toJSON() {
    return {
      pos: this._pos,
      posInfo: this.posInfo(),
      word: this._word,
      sentimentValue: this._sentimentValue,
      sentimentProbability: this._sentimentProbability,
      token: this._token,
      children: this._children
    };
  }
}

exports.Node = Node; /**
                      * @class
                      * @classdesc Class representing a Parse tree structure
                      * @memberof CoreNLP/util
                      * @description
                      * The nodes are given in order left to right as the words in a sentence appears
                      * The leaves are grouped into semantic representations provided by the Annotator
                      * This class is pretty useful to use along with the ParserAnnotator
                      * @see inspired on {@link http://www.nltk.org/howto/tree.html|Tree}
                      * @see the lecture {@link http://www.cs.cornell.edu/courses/cs474/2004fa/lec1.pdf|Tree Syntax of Natural Language}
                      */

class Tree {
  /**
   * Create a Tree
   * @param {Node} node
   */
  constructor(node) {
    this.rootNode = node;
  }

  /**
   * Get a Tree string representation for debugging purposes
   * @returns {string} tree
   */
  dump() {
    return JSON.stringify(this.rootNode, (key, val) => {
      if (val instanceof Node) {
        return val.children().length ? {
          pos: val.pos(),
          info: val.info(),
          sentimentValue: val.sentimentValue(),
          sentimentProbability: val.sentimentProbability(),
          children: val.children()
        } : {
          pos: val.pos(),
          info: val.info(),
          word: val.word(),
          sentimentValue: val.sentimentValue(),
          sentimentProbability: val.sentimentProbability(),
          token: val.token()
        };
      }

      return val;
    }, 2);
  }

  /**
   * Performs Deep-first Search calling a visitor for each node
   * @see {@link https://en.wikipedia.org/wiki/Depth-first_search|DFS}
   */
  visitDeepFirst(visitor, node = this.rootNode) {
    node.children().forEach(childNode => {
      this.visitDeepFirst(visitor, childNode);
      //visitor(childNode);
    });

    visitor(node);
  }

  /**
   * Performs Deep-first Search calling a visitor for each node, from right to left
   * @see {@link https://en.wikipedia.org/wiki/Depth-first_search|DFS}
   */
  visitDeepFirstRight(visitor, node = this.rootNode) {
    node.children().reverse().forEach(childNode => {
      this.visitDeepFirstRight(visitor, childNode);
      visitor(childNode);
    });

    visitor(node);
  }

  /**
   * Performs Deep-first Search calling a visitor only over leaves
   * @see {@link https://en.wikipedia.org/wiki/Depth-first_search|DFS}
   */
  visitLeaves(visitor, node = this.rootNode) {
    node.children().forEach(childNode => {
      if (childNode.children().length) {
        this.visitLeaves(visitor, childNode);
      } else {
        visitor(childNode);
      }
    });

    if (!node.children().length) {
      visitor(node);
    }
  }

  reorganizeBinary() {
    this.visitDeepFirst(node => {
      const token = node.token();
      if (token) {
        //add sentimentValue and sentimentProbability to nodes containing tokens
        node.sentimentValue(node.token().sentimentValue()); 
        node.sentimentProbability(node.token().sentimentProbability());          
      }
      else if (node.pos().includes('@')) {
        const regex = new RegExp('(?:@|\\b)' + node.pos().replace('@', ''));
        const nodeToMove = node.parent().children().find(childNode => {
          return !childNode.pos().includes('@');
        });
        if (regex.test(node.parent().pos())) {            
          var children = []
          node.children().forEach(childNode => {
            children.push(childNode);
            childNode.parent(node.parent());
          });
          if (node.parent().children().indexOf(node) < node.parent().children().indexOf(nodeToMove)) {
            var newNodeToMove = new Tree(nodeToMove);
            newNodeToMove.reorganizeBinary();
            newNodeToMove.rootNode.parent(node.parent());
            children.push(newNodeToMove.rootNode);  
            node.parent().removeChild(nodeToMove);          
          }          
          children.forEach(child => {
            node.parent().appendChild(child);
          });          
          node.parent().removeChild(node);
        }
        else {            
          node.unshiftChild(nodeToMove);
          nodeToMove.parent(node);
          node.parent().removeChild(nodeToMove);
        }
        /**
        * Cannot currently pull Value and Probability trom sentimentTree for nodes that
        * do not have tokens. Tree format from parse string does not always match
        * sentimentTree string, so it is not possible to line them up directly. Value
        * and Probability for nodes without tokens is not calculated using an average
        * of the tokenized nodes, which is what the code below tested, so it is being
        * analyzed separately. Either need to find a way to get the trees' architectures 
        * to match on callback, afterwards using regex, or some other method.  
        */
        /*
        node.children().forEach(childNode => {
          const sentimentValue = node.sentimentValue();
          if (sentimentValue) {
            node.sentimentValue(node.sentimentValue() + childNode.sentimentValue());
          }
          else {
            node.sentimentValue(childNode.sentimentValue());
          }
        });
        node.sentimentValue(node.sentimentValue() / node.children().length);
        */
      }
    });
  }

  /**
   * @param {Sentence} sentence
   * @param {boolean} [doubleLink] whether the child nodes should have a reference
   * to their parent or not - this allows the use of {@link Node.parent()}
   * @returns {Tree} tree
   */
  static fromSentence(sentence, doubleLink = false) {
    const parse = sentence.parse();
    if (!parse) {
      throw new Error('Unable to create Tree from Sentence, did you run ParserAnnotator first?');
    }
    
    //const sentiment = sentence.sentimentTree();
    //const tree = sentiment ? Tree.fromString(sentiment, true) : Tree.fromString(parse, doubleLink);
    const tree = Tree.fromString(parse, doubleLink);

    // link nodes with tokens
    var visitedLeaves = 0;
    // eslint-disable-next-line no-plusplus
    tree.visitLeaves(node => node.token(sentence.token(visitedLeaves++)));

    if (false) {
      tree.reorganizeBinary();
    }
    

    const languageIso = sentence.getLanguageISO();
    if (languageIso) {
      tree.visitDeepFirst(node => node.setLanguageISO(languageIso));
    }

    return tree;
  }

  /**
   * @param {string} str
   * @param {boolean} [doubleLink] whether the child nodes should have a reference
   * to their parent or not - this allows the use of {@link Node.parent()}
   * @returns {Tree} tree
   */
  static fromString(str, doubleLink = false) {
    return new Tree(this._transformTree(this._buildTree(str), doubleLink));
  }

  static _buildTree(str) {
    let currentNode = { children: [] };
    const openNodes = [currentNode];
    const l = str.length;
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < l; i++) {
      if (str[i] === '(') {
        currentNode = { str: '', children: [] };
        openNodes[openNodes.length - 1].children.push(currentNode);
        openNodes.push(currentNode);
      } else if (str[i] === ')') {
        this._cleanNode(currentNode);
        openNodes.pop();
        currentNode = openNodes[openNodes.length - 1];
      } else {
        currentNode.str += str[i];
      }
    }
    return currentNode.children[0];
  }

  static _cleanNode(node) {
    const str = node.str.trim();
    const posDelimiter = str.includes('|') ? str.indexOf('|') : str.indexOf(' ');
    const wordDelimiter = str.indexOf(' ')
    if (posDelimiter > -1) {
      node.pos = str.substr(0, wordDelimiter);
    }
    else {
      node.pos = str;
    }
    if (wordDelimiter > -1) {
      node.word = str.substr(wordDelimiter + 1);
    } 
  }

  static _transformTree(node, doubleLink) {
    if (doubleLink) {
      const parentNode = new Node(node.pos, node.word);
      node.children.forEach(n => {
        const childNode = this._transformTree(n, doubleLink);
        childNode.parent(parentNode);
        parentNode.appendChild(childNode);
      });

      return parentNode;
    }

    return new Node(node.pos, node.word, node.children.map(n => this._transformTree(n, doubleLink)));
  }
}

exports.default = Tree;