(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.CoreNLP = f()}})(function(){var define,module,exports;return (function(){function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s}return e})()({1:[function(require,module,exports){
"use strict";

},{}],2:[function(require,module,exports){
(function (global){
/**
 * lodash (Custom Build) <https://lodash.com/>
 * Build: `lodash modularize exports="npm" -o ./`
 * Copyright jQuery Foundation and other contributors <https://jquery.org/>
 * Released under MIT license <https://lodash.com/license>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 */

/** Used as the size to enable large array optimizations. */
var LARGE_ARRAY_SIZE = 200;

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]';

/**
 * Used to match `RegExp`
 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
 */
var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;

/** Used to detect host constructors (Safari). */
var reIsHostCtor = /^\[object .+?Constructor\]$/;

/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

/**
 * A faster alternative to `Function#apply`, this function invokes `func`
 * with the `this` binding of `thisArg` and the arguments of `args`.
 *
 * @private
 * @param {Function} func The function to invoke.
 * @param {*} thisArg The `this` binding of `func`.
 * @param {Array} args The arguments to invoke `func` with.
 * @returns {*} Returns the result of `func`.
 */
function apply(func, thisArg, args) {
  switch (args.length) {
    case 0: return func.call(thisArg);
    case 1: return func.call(thisArg, args[0]);
    case 2: return func.call(thisArg, args[0], args[1]);
    case 3: return func.call(thisArg, args[0], args[1], args[2]);
  }
  return func.apply(thisArg, args);
}

/**
 * A specialized version of `_.includes` for arrays without support for
 * specifying an index to search from.
 *
 * @private
 * @param {Array} [array] The array to inspect.
 * @param {*} target The value to search for.
 * @returns {boolean} Returns `true` if `target` is found, else `false`.
 */
function arrayIncludes(array, value) {
  var length = array ? array.length : 0;
  return !!length && baseIndexOf(array, value, 0) > -1;
}

/**
 * This function is like `arrayIncludes` except that it accepts a comparator.
 *
 * @private
 * @param {Array} [array] The array to inspect.
 * @param {*} target The value to search for.
 * @param {Function} comparator The comparator invoked per element.
 * @returns {boolean} Returns `true` if `target` is found, else `false`.
 */
function arrayIncludesWith(array, value, comparator) {
  var index = -1,
      length = array ? array.length : 0;

  while (++index < length) {
    if (comparator(value, array[index])) {
      return true;
    }
  }
  return false;
}

/**
 * A specialized version of `_.map` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the new mapped array.
 */
function arrayMap(array, iteratee) {
  var index = -1,
      length = array ? array.length : 0,
      result = Array(length);

  while (++index < length) {
    result[index] = iteratee(array[index], index, array);
  }
  return result;
}

/**
 * Appends the elements of `values` to `array`.
 *
 * @private
 * @param {Array} array The array to modify.
 * @param {Array} values The values to append.
 * @returns {Array} Returns `array`.
 */
function arrayPush(array, values) {
  var index = -1,
      length = values.length,
      offset = array.length;

  while (++index < length) {
    array[offset + index] = values[index];
  }
  return array;
}

/**
 * The base implementation of `_.findIndex` and `_.findLastIndex` without
 * support for iteratee shorthands.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {Function} predicate The function invoked per iteration.
 * @param {number} fromIndex The index to search from.
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function baseFindIndex(array, predicate, fromIndex, fromRight) {
  var length = array.length,
      index = fromIndex + (fromRight ? 1 : -1);

  while ((fromRight ? index-- : ++index < length)) {
    if (predicate(array[index], index, array)) {
      return index;
    }
  }
  return -1;
}

/**
 * The base implementation of `_.indexOf` without `fromIndex` bounds checks.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} value The value to search for.
 * @param {number} fromIndex The index to search from.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function baseIndexOf(array, value, fromIndex) {
  if (value !== value) {
    return baseFindIndex(array, baseIsNaN, fromIndex);
  }
  var index = fromIndex - 1,
      length = array.length;

  while (++index < length) {
    if (array[index] === value) {
      return index;
    }
  }
  return -1;
}

/**
 * The base implementation of `_.isNaN` without support for number objects.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
 */
function baseIsNaN(value) {
  return value !== value;
}

/**
 * The base implementation of `_.unary` without support for storing metadata.
 *
 * @private
 * @param {Function} func The function to cap arguments for.
 * @returns {Function} Returns the new capped function.
 */
function baseUnary(func) {
  return function(value) {
    return func(value);
  };
}

/**
 * Checks if a cache value for `key` exists.
 *
 * @private
 * @param {Object} cache The cache to query.
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function cacheHas(cache, key) {
  return cache.has(key);
}

/**
 * Gets the value at `key` of `object`.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {string} key The key of the property to get.
 * @returns {*} Returns the property value.
 */
function getValue(object, key) {
  return object == null ? undefined : object[key];
}

/**
 * Checks if `value` is a host object in IE < 9.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a host object, else `false`.
 */
function isHostObject(value) {
  // Many host objects are `Object` objects that can coerce to strings
  // despite having improperly defined `toString` methods.
  var result = false;
  if (value != null && typeof value.toString != 'function') {
    try {
      result = !!(value + '');
    } catch (e) {}
  }
  return result;
}

/** Used for built-in method references. */
var arrayProto = Array.prototype,
    funcProto = Function.prototype,
    objectProto = Object.prototype;

/** Used to detect overreaching core-js shims. */
var coreJsData = root['__core-js_shared__'];

/** Used to detect methods masquerading as native. */
var maskSrcKey = (function() {
  var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || '');
  return uid ? ('Symbol(src)_1.' + uid) : '';
}());

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var objectToString = objectProto.toString;

/** Used to detect if a method is native. */
var reIsNative = RegExp('^' +
  funcToString.call(hasOwnProperty).replace(reRegExpChar, '\\$&')
  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
);

/** Built-in value references. */
var Symbol = root.Symbol,
    propertyIsEnumerable = objectProto.propertyIsEnumerable,
    splice = arrayProto.splice,
    spreadableSymbol = Symbol ? Symbol.isConcatSpreadable : undefined;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max;

/* Built-in method references that are verified to be native. */
var Map = getNative(root, 'Map'),
    nativeCreate = getNative(Object, 'create');

/**
 * Creates a hash object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function Hash(entries) {
  var index = -1,
      length = entries ? entries.length : 0;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

/**
 * Removes all key-value entries from the hash.
 *
 * @private
 * @name clear
 * @memberOf Hash
 */
function hashClear() {
  this.__data__ = nativeCreate ? nativeCreate(null) : {};
}

/**
 * Removes `key` and its value from the hash.
 *
 * @private
 * @name delete
 * @memberOf Hash
 * @param {Object} hash The hash to modify.
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function hashDelete(key) {
  return this.has(key) && delete this.__data__[key];
}

/**
 * Gets the hash value for `key`.
 *
 * @private
 * @name get
 * @memberOf Hash
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function hashGet(key) {
  var data = this.__data__;
  if (nativeCreate) {
    var result = data[key];
    return result === HASH_UNDEFINED ? undefined : result;
  }
  return hasOwnProperty.call(data, key) ? data[key] : undefined;
}

/**
 * Checks if a hash value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Hash
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function hashHas(key) {
  var data = this.__data__;
  return nativeCreate ? data[key] !== undefined : hasOwnProperty.call(data, key);
}

/**
 * Sets the hash `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Hash
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the hash instance.
 */
function hashSet(key, value) {
  var data = this.__data__;
  data[key] = (nativeCreate && value === undefined) ? HASH_UNDEFINED : value;
  return this;
}

// Add methods to `Hash`.
Hash.prototype.clear = hashClear;
Hash.prototype['delete'] = hashDelete;
Hash.prototype.get = hashGet;
Hash.prototype.has = hashHas;
Hash.prototype.set = hashSet;

/**
 * Creates an list cache object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function ListCache(entries) {
  var index = -1,
      length = entries ? entries.length : 0;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

/**
 * Removes all key-value entries from the list cache.
 *
 * @private
 * @name clear
 * @memberOf ListCache
 */
function listCacheClear() {
  this.__data__ = [];
}

/**
 * Removes `key` and its value from the list cache.
 *
 * @private
 * @name delete
 * @memberOf ListCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function listCacheDelete(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    return false;
  }
  var lastIndex = data.length - 1;
  if (index == lastIndex) {
    data.pop();
  } else {
    splice.call(data, index, 1);
  }
  return true;
}

/**
 * Gets the list cache value for `key`.
 *
 * @private
 * @name get
 * @memberOf ListCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function listCacheGet(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  return index < 0 ? undefined : data[index][1];
}

/**
 * Checks if a list cache value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf ListCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function listCacheHas(key) {
  return assocIndexOf(this.__data__, key) > -1;
}

/**
 * Sets the list cache `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf ListCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the list cache instance.
 */
function listCacheSet(key, value) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    data.push([key, value]);
  } else {
    data[index][1] = value;
  }
  return this;
}

// Add methods to `ListCache`.
ListCache.prototype.clear = listCacheClear;
ListCache.prototype['delete'] = listCacheDelete;
ListCache.prototype.get = listCacheGet;
ListCache.prototype.has = listCacheHas;
ListCache.prototype.set = listCacheSet;

/**
 * Creates a map cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function MapCache(entries) {
  var index = -1,
      length = entries ? entries.length : 0;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

/**
 * Removes all key-value entries from the map.
 *
 * @private
 * @name clear
 * @memberOf MapCache
 */
function mapCacheClear() {
  this.__data__ = {
    'hash': new Hash,
    'map': new (Map || ListCache),
    'string': new Hash
  };
}

/**
 * Removes `key` and its value from the map.
 *
 * @private
 * @name delete
 * @memberOf MapCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function mapCacheDelete(key) {
  return getMapData(this, key)['delete'](key);
}

/**
 * Gets the map value for `key`.
 *
 * @private
 * @name get
 * @memberOf MapCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function mapCacheGet(key) {
  return getMapData(this, key).get(key);
}

/**
 * Checks if a map value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf MapCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function mapCacheHas(key) {
  return getMapData(this, key).has(key);
}

/**
 * Sets the map `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf MapCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the map cache instance.
 */
function mapCacheSet(key, value) {
  getMapData(this, key).set(key, value);
  return this;
}

// Add methods to `MapCache`.
MapCache.prototype.clear = mapCacheClear;
MapCache.prototype['delete'] = mapCacheDelete;
MapCache.prototype.get = mapCacheGet;
MapCache.prototype.has = mapCacheHas;
MapCache.prototype.set = mapCacheSet;

/**
 *
 * Creates an array cache object to store unique values.
 *
 * @private
 * @constructor
 * @param {Array} [values] The values to cache.
 */
function SetCache(values) {
  var index = -1,
      length = values ? values.length : 0;

  this.__data__ = new MapCache;
  while (++index < length) {
    this.add(values[index]);
  }
}

/**
 * Adds `value` to the array cache.
 *
 * @private
 * @name add
 * @memberOf SetCache
 * @alias push
 * @param {*} value The value to cache.
 * @returns {Object} Returns the cache instance.
 */
function setCacheAdd(value) {
  this.__data__.set(value, HASH_UNDEFINED);
  return this;
}

/**
 * Checks if `value` is in the array cache.
 *
 * @private
 * @name has
 * @memberOf SetCache
 * @param {*} value The value to search for.
 * @returns {number} Returns `true` if `value` is found, else `false`.
 */
function setCacheHas(value) {
  return this.__data__.has(value);
}

// Add methods to `SetCache`.
SetCache.prototype.add = SetCache.prototype.push = setCacheAdd;
SetCache.prototype.has = setCacheHas;

/**
 * Gets the index at which the `key` is found in `array` of key-value pairs.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} key The key to search for.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function assocIndexOf(array, key) {
  var length = array.length;
  while (length--) {
    if (eq(array[length][0], key)) {
      return length;
    }
  }
  return -1;
}

/**
 * The base implementation of methods like `_.difference` without support
 * for excluding multiple arrays or iteratee shorthands.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {Array} values The values to exclude.
 * @param {Function} [iteratee] The iteratee invoked per element.
 * @param {Function} [comparator] The comparator invoked per element.
 * @returns {Array} Returns the new array of filtered values.
 */
function baseDifference(array, values, iteratee, comparator) {
  var index = -1,
      includes = arrayIncludes,
      isCommon = true,
      length = array.length,
      result = [],
      valuesLength = values.length;

  if (!length) {
    return result;
  }
  if (iteratee) {
    values = arrayMap(values, baseUnary(iteratee));
  }
  if (comparator) {
    includes = arrayIncludesWith;
    isCommon = false;
  }
  else if (values.length >= LARGE_ARRAY_SIZE) {
    includes = cacheHas;
    isCommon = false;
    values = new SetCache(values);
  }
  outer:
  while (++index < length) {
    var value = array[index],
        computed = iteratee ? iteratee(value) : value;

    value = (comparator || value !== 0) ? value : 0;
    if (isCommon && computed === computed) {
      var valuesIndex = valuesLength;
      while (valuesIndex--) {
        if (values[valuesIndex] === computed) {
          continue outer;
        }
      }
      result.push(value);
    }
    else if (!includes(values, computed, comparator)) {
      result.push(value);
    }
  }
  return result;
}

/**
 * The base implementation of `_.flatten` with support for restricting flattening.
 *
 * @private
 * @param {Array} array The array to flatten.
 * @param {number} depth The maximum recursion depth.
 * @param {boolean} [predicate=isFlattenable] The function invoked per iteration.
 * @param {boolean} [isStrict] Restrict to values that pass `predicate` checks.
 * @param {Array} [result=[]] The initial result value.
 * @returns {Array} Returns the new flattened array.
 */
function baseFlatten(array, depth, predicate, isStrict, result) {
  var index = -1,
      length = array.length;

  predicate || (predicate = isFlattenable);
  result || (result = []);

  while (++index < length) {
    var value = array[index];
    if (depth > 0 && predicate(value)) {
      if (depth > 1) {
        // Recursively flatten arrays (susceptible to call stack limits).
        baseFlatten(value, depth - 1, predicate, isStrict, result);
      } else {
        arrayPush(result, value);
      }
    } else if (!isStrict) {
      result[result.length] = value;
    }
  }
  return result;
}

/**
 * The base implementation of `_.isNative` without bad shim checks.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function,
 *  else `false`.
 */
function baseIsNative(value) {
  if (!isObject(value) || isMasked(value)) {
    return false;
  }
  var pattern = (isFunction(value) || isHostObject(value)) ? reIsNative : reIsHostCtor;
  return pattern.test(toSource(value));
}

/**
 * The base implementation of `_.rest` which doesn't validate or coerce arguments.
 *
 * @private
 * @param {Function} func The function to apply a rest parameter to.
 * @param {number} [start=func.length-1] The start position of the rest parameter.
 * @returns {Function} Returns the new function.
 */
function baseRest(func, start) {
  start = nativeMax(start === undefined ? (func.length - 1) : start, 0);
  return function() {
    var args = arguments,
        index = -1,
        length = nativeMax(args.length - start, 0),
        array = Array(length);

    while (++index < length) {
      array[index] = args[start + index];
    }
    index = -1;
    var otherArgs = Array(start + 1);
    while (++index < start) {
      otherArgs[index] = args[index];
    }
    otherArgs[start] = array;
    return apply(func, this, otherArgs);
  };
}

/**
 * Gets the data for `map`.
 *
 * @private
 * @param {Object} map The map to query.
 * @param {string} key The reference key.
 * @returns {*} Returns the map data.
 */
function getMapData(map, key) {
  var data = map.__data__;
  return isKeyable(key)
    ? data[typeof key == 'string' ? 'string' : 'hash']
    : data.map;
}

/**
 * Gets the native function at `key` of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the method to get.
 * @returns {*} Returns the function if it's native, else `undefined`.
 */
function getNative(object, key) {
  var value = getValue(object, key);
  return baseIsNative(value) ? value : undefined;
}

/**
 * Checks if `value` is a flattenable `arguments` object or array.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is flattenable, else `false`.
 */
function isFlattenable(value) {
  return isArray(value) || isArguments(value) ||
    !!(spreadableSymbol && value && value[spreadableSymbol]);
}

/**
 * Checks if `value` is suitable for use as unique object key.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
 */
function isKeyable(value) {
  var type = typeof value;
  return (type == 'string' || type == 'number' || type == 'symbol' || type == 'boolean')
    ? (value !== '__proto__')
    : (value === null);
}

/**
 * Checks if `func` has its source masked.
 *
 * @private
 * @param {Function} func The function to check.
 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
 */
function isMasked(func) {
  return !!maskSrcKey && (maskSrcKey in func);
}

/**
 * Converts `func` to its source code.
 *
 * @private
 * @param {Function} func The function to process.
 * @returns {string} Returns the source code.
 */
function toSource(func) {
  if (func != null) {
    try {
      return funcToString.call(func);
    } catch (e) {}
    try {
      return (func + '');
    } catch (e) {}
  }
  return '';
}

/**
 * Creates an array of `array` values not included in the other given arrays
 * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * for equality comparisons. The order of result values is determined by the
 * order they occur in the first array.
 *
 * **Note:** Unlike `_.pullAll`, this method returns a new array.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Array
 * @param {Array} array The array to inspect.
 * @param {...Array} [values] The values to exclude.
 * @returns {Array} Returns the new array of filtered values.
 * @see _.without, _.xor
 * @example
 *
 * _.difference([2, 1], [2, 3]);
 * // => [1]
 */
var difference = baseRest(function(array, values) {
  return isArrayLikeObject(array)
    ? baseDifference(array, baseFlatten(values, 1, isArrayLikeObject, true))
    : [];
});

/**
 * Performs a
 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * comparison between two values to determine if they are equivalent.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.eq(object, object);
 * // => true
 *
 * _.eq(object, other);
 * // => false
 *
 * _.eq('a', 'a');
 * // => true
 *
 * _.eq('a', Object('a'));
 * // => false
 *
 * _.eq(NaN, NaN);
 * // => true
 */
function eq(value, other) {
  return value === other || (value !== value && other !== other);
}

/**
 * Checks if `value` is likely an `arguments` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 *  else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
function isArguments(value) {
  // Safari 8.1 makes `arguments.callee` enumerable in strict mode.
  return isArrayLikeObject(value) && hasOwnProperty.call(value, 'callee') &&
    (!propertyIsEnumerable.call(value, 'callee') || objectToString.call(value) == argsTag);
}

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

/**
 * Checks if `value` is array-like. A value is considered array-like if it's
 * not a function and has a `value.length` that's an integer greater than or
 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 * @example
 *
 * _.isArrayLike([1, 2, 3]);
 * // => true
 *
 * _.isArrayLike(document.body.children);
 * // => true
 *
 * _.isArrayLike('abc');
 * // => true
 *
 * _.isArrayLike(_.noop);
 * // => false
 */
function isArrayLike(value) {
  return value != null && isLength(value.length) && !isFunction(value);
}

/**
 * This method is like `_.isArrayLike` except that it also checks if `value`
 * is an object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array-like object,
 *  else `false`.
 * @example
 *
 * _.isArrayLikeObject([1, 2, 3]);
 * // => true
 *
 * _.isArrayLikeObject(document.body.children);
 * // => true
 *
 * _.isArrayLikeObject('abc');
 * // => false
 *
 * _.isArrayLikeObject(_.noop);
 * // => false
 */
function isArrayLikeObject(value) {
  return isObjectLike(value) && isArrayLike(value);
}

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 8-9 which returns 'object' for typed array and other constructors.
  var tag = isObject(value) ? objectToString.call(value) : '';
  return tag == funcTag || tag == genTag;
}

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This method is loosely based on
 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 * @example
 *
 * _.isLength(3);
 * // => true
 *
 * _.isLength(Number.MIN_VALUE);
 * // => false
 *
 * _.isLength(Infinity);
 * // => false
 *
 * _.isLength('3');
 * // => false
 */
function isLength(value) {
  return typeof value == 'number' &&
    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return !!value && (type == 'object' || type == 'function');
}

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return !!value && typeof value == 'object';
}

module.exports = difference;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],3:[function(require,module,exports){
(function (global){
/**
 * lodash (Custom Build) <https://lodash.com/>
 * Build: `lodash modularize exports="npm" -o ./`
 * Copyright jQuery Foundation and other contributors <https://jquery.org/>
 * Released under MIT license <https://lodash.com/license>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 */

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]';

/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

/**
 * Appends the elements of `values` to `array`.
 *
 * @private
 * @param {Array} array The array to modify.
 * @param {Array} values The values to append.
 * @returns {Array} Returns `array`.
 */
function arrayPush(array, values) {
  var index = -1,
      length = values.length,
      offset = array.length;

  while (++index < length) {
    array[offset + index] = values[index];
  }
  return array;
}

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var objectToString = objectProto.toString;

/** Built-in value references. */
var Symbol = root.Symbol,
    propertyIsEnumerable = objectProto.propertyIsEnumerable,
    spreadableSymbol = Symbol ? Symbol.isConcatSpreadable : undefined;

/**
 * The base implementation of `_.flatten` with support for restricting flattening.
 *
 * @private
 * @param {Array} array The array to flatten.
 * @param {number} depth The maximum recursion depth.
 * @param {boolean} [predicate=isFlattenable] The function invoked per iteration.
 * @param {boolean} [isStrict] Restrict to values that pass `predicate` checks.
 * @param {Array} [result=[]] The initial result value.
 * @returns {Array} Returns the new flattened array.
 */
function baseFlatten(array, depth, predicate, isStrict, result) {
  var index = -1,
      length = array.length;

  predicate || (predicate = isFlattenable);
  result || (result = []);

  while (++index < length) {
    var value = array[index];
    if (depth > 0 && predicate(value)) {
      if (depth > 1) {
        // Recursively flatten arrays (susceptible to call stack limits).
        baseFlatten(value, depth - 1, predicate, isStrict, result);
      } else {
        arrayPush(result, value);
      }
    } else if (!isStrict) {
      result[result.length] = value;
    }
  }
  return result;
}

/**
 * Checks if `value` is a flattenable `arguments` object or array.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is flattenable, else `false`.
 */
function isFlattenable(value) {
  return isArray(value) || isArguments(value) ||
    !!(spreadableSymbol && value && value[spreadableSymbol]);
}

/**
 * Flattens `array` a single level deep.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Array
 * @param {Array} array The array to flatten.
 * @returns {Array} Returns the new flattened array.
 * @example
 *
 * _.flatten([1, [2, [3, [4]], 5]]);
 * // => [1, 2, [3, [4]], 5]
 */
function flatten(array) {
  var length = array ? array.length : 0;
  return length ? baseFlatten(array, 1) : [];
}

/**
 * Checks if `value` is likely an `arguments` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 *  else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
function isArguments(value) {
  // Safari 8.1 makes `arguments.callee` enumerable in strict mode.
  return isArrayLikeObject(value) && hasOwnProperty.call(value, 'callee') &&
    (!propertyIsEnumerable.call(value, 'callee') || objectToString.call(value) == argsTag);
}

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

/**
 * Checks if `value` is array-like. A value is considered array-like if it's
 * not a function and has a `value.length` that's an integer greater than or
 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 * @example
 *
 * _.isArrayLike([1, 2, 3]);
 * // => true
 *
 * _.isArrayLike(document.body.children);
 * // => true
 *
 * _.isArrayLike('abc');
 * // => true
 *
 * _.isArrayLike(_.noop);
 * // => false
 */
function isArrayLike(value) {
  return value != null && isLength(value.length) && !isFunction(value);
}

/**
 * This method is like `_.isArrayLike` except that it also checks if `value`
 * is an object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array-like object,
 *  else `false`.
 * @example
 *
 * _.isArrayLikeObject([1, 2, 3]);
 * // => true
 *
 * _.isArrayLikeObject(document.body.children);
 * // => true
 *
 * _.isArrayLikeObject('abc');
 * // => false
 *
 * _.isArrayLikeObject(_.noop);
 * // => false
 */
function isArrayLikeObject(value) {
  return isObjectLike(value) && isArrayLike(value);
}

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 8-9 which returns 'object' for typed array and other constructors.
  var tag = isObject(value) ? objectToString.call(value) : '';
  return tag == funcTag || tag == genTag;
}

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This method is loosely based on
 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 * @example
 *
 * _.isLength(3);
 * // => true
 *
 * _.isLength(Number.MIN_VALUE);
 * // => false
 *
 * _.isLength(Infinity);
 * // => false
 *
 * _.isLength('3');
 * // => false
 */
function isLength(value) {
  return typeof value == 'number' &&
    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return !!value && (type == 'object' || type == 'function');
}

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return !!value && typeof value == 'object';
}

module.exports = flatten;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],4:[function(require,module,exports){
/**
 * lodash 4.0.1 (Custom Build) <https://lodash.com/>
 * Build: `lodash modularize exports="npm" -o ./`
 * Copyright jQuery Foundation and other contributors <https://jquery.org/>
 * Released under MIT license <https://lodash.com/license>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 */

/**
 * Gets the first element of `array`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @alias first
 * @category Array
 * @param {Array} array The array to query.
 * @returns {*} Returns the first element of `array`.
 * @example
 *
 * _.head([1, 2, 3]);
 * // => 1
 *
 * _.head([]);
 * // => undefined
 */
function head(array) {
  return (array && array.length) ? array[0] : undefined;
}

module.exports = head;

},{}],5:[function(require,module,exports){
(function (global){
/**
 * lodash (Custom Build) <https://lodash.com/>
 * Build: `lodash modularize exports="npm" -o ./`
 * Copyright jQuery Foundation and other contributors <https://jquery.org/>
 * Released under MIT license <https://lodash.com/license>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 */

/** Used as the size to enable large array optimizations. */
var LARGE_ARRAY_SIZE = 200;

/** Used as the `TypeError` message for "Functions" methods. */
var FUNC_ERROR_TEXT = 'Expected a function';

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';

/** Used to compose bitmasks for comparison styles. */
var UNORDERED_COMPARE_FLAG = 1,
    PARTIAL_COMPARE_FLAG = 2;

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0,
    MAX_SAFE_INTEGER = 9007199254740991;

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    objectTag = '[object Object]',
    promiseTag = '[object Promise]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    symbolTag = '[object Symbol]',
    weakMapTag = '[object WeakMap]';

var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]',
    float32Tag = '[object Float32Array]',
    float64Tag = '[object Float64Array]',
    int8Tag = '[object Int8Array]',
    int16Tag = '[object Int16Array]',
    int32Tag = '[object Int32Array]',
    uint8Tag = '[object Uint8Array]',
    uint8ClampedTag = '[object Uint8ClampedArray]',
    uint16Tag = '[object Uint16Array]',
    uint32Tag = '[object Uint32Array]';

/** Used to match property names within property paths. */
var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
    reIsPlainProp = /^\w*$/,
    reLeadingDot = /^\./,
    rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;

/**
 * Used to match `RegExp`
 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
 */
var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;

/** Used to match backslashes in property paths. */
var reEscapeChar = /\\(\\)?/g;

/** Used to detect host constructors (Safari). */
var reIsHostCtor = /^\[object .+?Constructor\]$/;

/** Used to detect unsigned integer values. */
var reIsUint = /^(?:0|[1-9]\d*)$/;

/** Used to identify `toStringTag` values of typed arrays. */
var typedArrayTags = {};
typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
typedArrayTags[uint32Tag] = true;
typedArrayTags[argsTag] = typedArrayTags[arrayTag] =
typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
typedArrayTags[dataViewTag] = typedArrayTags[dateTag] =
typedArrayTags[errorTag] = typedArrayTags[funcTag] =
typedArrayTags[mapTag] = typedArrayTags[numberTag] =
typedArrayTags[objectTag] = typedArrayTags[regexpTag] =
typedArrayTags[setTag] = typedArrayTags[stringTag] =
typedArrayTags[weakMapTag] = false;

/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

/** Detect free variable `exports`. */
var freeExports = typeof exports == 'object' && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Detect free variable `process` from Node.js. */
var freeProcess = moduleExports && freeGlobal.process;

/** Used to access faster Node.js helpers. */
var nodeUtil = (function() {
  try {
    return freeProcess && freeProcess.binding('util');
  } catch (e) {}
}());

/* Node.js helper references. */
var nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray;

/**
 * A specialized version of `_.reduce` for arrays without support for
 * iteratee shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {*} [accumulator] The initial value.
 * @param {boolean} [initAccum] Specify using the first element of `array` as
 *  the initial value.
 * @returns {*} Returns the accumulated value.
 */
function arrayReduce(array, iteratee, accumulator, initAccum) {
  var index = -1,
      length = array ? array.length : 0;

  if (initAccum && length) {
    accumulator = array[++index];
  }
  while (++index < length) {
    accumulator = iteratee(accumulator, array[index], index, array);
  }
  return accumulator;
}

/**
 * A specialized version of `_.some` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {boolean} Returns `true` if any element passes the predicate check,
 *  else `false`.
 */
function arraySome(array, predicate) {
  var index = -1,
      length = array ? array.length : 0;

  while (++index < length) {
    if (predicate(array[index], index, array)) {
      return true;
    }
  }
  return false;
}

/**
 * The base implementation of `_.property` without support for deep paths.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function baseProperty(key) {
  return function(object) {
    return object == null ? undefined : object[key];
  };
}

/**
 * The base implementation of `_.reduce` and `_.reduceRight`, without support
 * for iteratee shorthands, which iterates over `collection` using `eachFunc`.
 *
 * @private
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {*} accumulator The initial value.
 * @param {boolean} initAccum Specify using the first or last element of
 *  `collection` as the initial value.
 * @param {Function} eachFunc The function to iterate over `collection`.
 * @returns {*} Returns the accumulated value.
 */
function baseReduce(collection, iteratee, accumulator, initAccum, eachFunc) {
  eachFunc(collection, function(value, index, collection) {
    accumulator = initAccum
      ? (initAccum = false, value)
      : iteratee(accumulator, value, index, collection);
  });
  return accumulator;
}

/**
 * The base implementation of `_.times` without support for iteratee shorthands
 * or max array length checks.
 *
 * @private
 * @param {number} n The number of times to invoke `iteratee`.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the array of results.
 */
function baseTimes(n, iteratee) {
  var index = -1,
      result = Array(n);

  while (++index < n) {
    result[index] = iteratee(index);
  }
  return result;
}

/**
 * The base implementation of `_.unary` without support for storing metadata.
 *
 * @private
 * @param {Function} func The function to cap arguments for.
 * @returns {Function} Returns the new capped function.
 */
function baseUnary(func) {
  return function(value) {
    return func(value);
  };
}

/**
 * Gets the value at `key` of `object`.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {string} key The key of the property to get.
 * @returns {*} Returns the property value.
 */
function getValue(object, key) {
  return object == null ? undefined : object[key];
}

/**
 * Checks if `value` is a host object in IE < 9.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a host object, else `false`.
 */
function isHostObject(value) {
  // Many host objects are `Object` objects that can coerce to strings
  // despite having improperly defined `toString` methods.
  var result = false;
  if (value != null && typeof value.toString != 'function') {
    try {
      result = !!(value + '');
    } catch (e) {}
  }
  return result;
}

/**
 * Converts `map` to its key-value pairs.
 *
 * @private
 * @param {Object} map The map to convert.
 * @returns {Array} Returns the key-value pairs.
 */
function mapToArray(map) {
  var index = -1,
      result = Array(map.size);

  map.forEach(function(value, key) {
    result[++index] = [key, value];
  });
  return result;
}

/**
 * Creates a unary function that invokes `func` with its argument transformed.
 *
 * @private
 * @param {Function} func The function to wrap.
 * @param {Function} transform The argument transform.
 * @returns {Function} Returns the new function.
 */
function overArg(func, transform) {
  return function(arg) {
    return func(transform(arg));
  };
}

/**
 * Converts `set` to an array of its values.
 *
 * @private
 * @param {Object} set The set to convert.
 * @returns {Array} Returns the values.
 */
function setToArray(set) {
  var index = -1,
      result = Array(set.size);

  set.forEach(function(value) {
    result[++index] = value;
  });
  return result;
}

/** Used for built-in method references. */
var arrayProto = Array.prototype,
    funcProto = Function.prototype,
    objectProto = Object.prototype;

/** Used to detect overreaching core-js shims. */
var coreJsData = root['__core-js_shared__'];

/** Used to detect methods masquerading as native. */
var maskSrcKey = (function() {
  var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || '');
  return uid ? ('Symbol(src)_1.' + uid) : '';
}());

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var objectToString = objectProto.toString;

/** Used to detect if a method is native. */
var reIsNative = RegExp('^' +
  funcToString.call(hasOwnProperty).replace(reRegExpChar, '\\$&')
  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
);

/** Built-in value references. */
var Symbol = root.Symbol,
    Uint8Array = root.Uint8Array,
    propertyIsEnumerable = objectProto.propertyIsEnumerable,
    splice = arrayProto.splice;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeKeys = overArg(Object.keys, Object);

/* Built-in method references that are verified to be native. */
var DataView = getNative(root, 'DataView'),
    Map = getNative(root, 'Map'),
    Promise = getNative(root, 'Promise'),
    Set = getNative(root, 'Set'),
    WeakMap = getNative(root, 'WeakMap'),
    nativeCreate = getNative(Object, 'create');

/** Used to detect maps, sets, and weakmaps. */
var dataViewCtorString = toSource(DataView),
    mapCtorString = toSource(Map),
    promiseCtorString = toSource(Promise),
    setCtorString = toSource(Set),
    weakMapCtorString = toSource(WeakMap);

/** Used to convert symbols to primitives and strings. */
var symbolProto = Symbol ? Symbol.prototype : undefined,
    symbolValueOf = symbolProto ? symbolProto.valueOf : undefined,
    symbolToString = symbolProto ? symbolProto.toString : undefined;

/**
 * Creates a hash object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function Hash(entries) {
  var index = -1,
      length = entries ? entries.length : 0;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

/**
 * Removes all key-value entries from the hash.
 *
 * @private
 * @name clear
 * @memberOf Hash
 */
function hashClear() {
  this.__data__ = nativeCreate ? nativeCreate(null) : {};
}

/**
 * Removes `key` and its value from the hash.
 *
 * @private
 * @name delete
 * @memberOf Hash
 * @param {Object} hash The hash to modify.
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function hashDelete(key) {
  return this.has(key) && delete this.__data__[key];
}

/**
 * Gets the hash value for `key`.
 *
 * @private
 * @name get
 * @memberOf Hash
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function hashGet(key) {
  var data = this.__data__;
  if (nativeCreate) {
    var result = data[key];
    return result === HASH_UNDEFINED ? undefined : result;
  }
  return hasOwnProperty.call(data, key) ? data[key] : undefined;
}

/**
 * Checks if a hash value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Hash
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function hashHas(key) {
  var data = this.__data__;
  return nativeCreate ? data[key] !== undefined : hasOwnProperty.call(data, key);
}

/**
 * Sets the hash `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Hash
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the hash instance.
 */
function hashSet(key, value) {
  var data = this.__data__;
  data[key] = (nativeCreate && value === undefined) ? HASH_UNDEFINED : value;
  return this;
}

// Add methods to `Hash`.
Hash.prototype.clear = hashClear;
Hash.prototype['delete'] = hashDelete;
Hash.prototype.get = hashGet;
Hash.prototype.has = hashHas;
Hash.prototype.set = hashSet;

/**
 * Creates an list cache object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function ListCache(entries) {
  var index = -1,
      length = entries ? entries.length : 0;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

/**
 * Removes all key-value entries from the list cache.
 *
 * @private
 * @name clear
 * @memberOf ListCache
 */
function listCacheClear() {
  this.__data__ = [];
}

/**
 * Removes `key` and its value from the list cache.
 *
 * @private
 * @name delete
 * @memberOf ListCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function listCacheDelete(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    return false;
  }
  var lastIndex = data.length - 1;
  if (index == lastIndex) {
    data.pop();
  } else {
    splice.call(data, index, 1);
  }
  return true;
}

/**
 * Gets the list cache value for `key`.
 *
 * @private
 * @name get
 * @memberOf ListCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function listCacheGet(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  return index < 0 ? undefined : data[index][1];
}

/**
 * Checks if a list cache value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf ListCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function listCacheHas(key) {
  return assocIndexOf(this.__data__, key) > -1;
}

/**
 * Sets the list cache `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf ListCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the list cache instance.
 */
function listCacheSet(key, value) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    data.push([key, value]);
  } else {
    data[index][1] = value;
  }
  return this;
}

// Add methods to `ListCache`.
ListCache.prototype.clear = listCacheClear;
ListCache.prototype['delete'] = listCacheDelete;
ListCache.prototype.get = listCacheGet;
ListCache.prototype.has = listCacheHas;
ListCache.prototype.set = listCacheSet;

/**
 * Creates a map cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function MapCache(entries) {
  var index = -1,
      length = entries ? entries.length : 0;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

/**
 * Removes all key-value entries from the map.
 *
 * @private
 * @name clear
 * @memberOf MapCache
 */
function mapCacheClear() {
  this.__data__ = {
    'hash': new Hash,
    'map': new (Map || ListCache),
    'string': new Hash
  };
}

/**
 * Removes `key` and its value from the map.
 *
 * @private
 * @name delete
 * @memberOf MapCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function mapCacheDelete(key) {
  return getMapData(this, key)['delete'](key);
}

/**
 * Gets the map value for `key`.
 *
 * @private
 * @name get
 * @memberOf MapCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function mapCacheGet(key) {
  return getMapData(this, key).get(key);
}

/**
 * Checks if a map value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf MapCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function mapCacheHas(key) {
  return getMapData(this, key).has(key);
}

/**
 * Sets the map `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf MapCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the map cache instance.
 */
function mapCacheSet(key, value) {
  getMapData(this, key).set(key, value);
  return this;
}

// Add methods to `MapCache`.
MapCache.prototype.clear = mapCacheClear;
MapCache.prototype['delete'] = mapCacheDelete;
MapCache.prototype.get = mapCacheGet;
MapCache.prototype.has = mapCacheHas;
MapCache.prototype.set = mapCacheSet;

/**
 *
 * Creates an array cache object to store unique values.
 *
 * @private
 * @constructor
 * @param {Array} [values] The values to cache.
 */
function SetCache(values) {
  var index = -1,
      length = values ? values.length : 0;

  this.__data__ = new MapCache;
  while (++index < length) {
    this.add(values[index]);
  }
}

/**
 * Adds `value` to the array cache.
 *
 * @private
 * @name add
 * @memberOf SetCache
 * @alias push
 * @param {*} value The value to cache.
 * @returns {Object} Returns the cache instance.
 */
function setCacheAdd(value) {
  this.__data__.set(value, HASH_UNDEFINED);
  return this;
}

/**
 * Checks if `value` is in the array cache.
 *
 * @private
 * @name has
 * @memberOf SetCache
 * @param {*} value The value to search for.
 * @returns {number} Returns `true` if `value` is found, else `false`.
 */
function setCacheHas(value) {
  return this.__data__.has(value);
}

// Add methods to `SetCache`.
SetCache.prototype.add = SetCache.prototype.push = setCacheAdd;
SetCache.prototype.has = setCacheHas;

/**
 * Creates a stack cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function Stack(entries) {
  this.__data__ = new ListCache(entries);
}

/**
 * Removes all key-value entries from the stack.
 *
 * @private
 * @name clear
 * @memberOf Stack
 */
function stackClear() {
  this.__data__ = new ListCache;
}

/**
 * Removes `key` and its value from the stack.
 *
 * @private
 * @name delete
 * @memberOf Stack
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function stackDelete(key) {
  return this.__data__['delete'](key);
}

/**
 * Gets the stack value for `key`.
 *
 * @private
 * @name get
 * @memberOf Stack
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function stackGet(key) {
  return this.__data__.get(key);
}

/**
 * Checks if a stack value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Stack
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function stackHas(key) {
  return this.__data__.has(key);
}

/**
 * Sets the stack `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Stack
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the stack cache instance.
 */
function stackSet(key, value) {
  var cache = this.__data__;
  if (cache instanceof ListCache) {
    var pairs = cache.__data__;
    if (!Map || (pairs.length < LARGE_ARRAY_SIZE - 1)) {
      pairs.push([key, value]);
      return this;
    }
    cache = this.__data__ = new MapCache(pairs);
  }
  cache.set(key, value);
  return this;
}

// Add methods to `Stack`.
Stack.prototype.clear = stackClear;
Stack.prototype['delete'] = stackDelete;
Stack.prototype.get = stackGet;
Stack.prototype.has = stackHas;
Stack.prototype.set = stackSet;

/**
 * Creates an array of the enumerable property names of the array-like `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @param {boolean} inherited Specify returning inherited property names.
 * @returns {Array} Returns the array of property names.
 */
function arrayLikeKeys(value, inherited) {
  // Safari 8.1 makes `arguments.callee` enumerable in strict mode.
  // Safari 9 makes `arguments.length` enumerable in strict mode.
  var result = (isArray(value) || isArguments(value))
    ? baseTimes(value.length, String)
    : [];

  var length = result.length,
      skipIndexes = !!length;

  for (var key in value) {
    if ((inherited || hasOwnProperty.call(value, key)) &&
        !(skipIndexes && (key == 'length' || isIndex(key, length)))) {
      result.push(key);
    }
  }
  return result;
}

/**
 * Gets the index at which the `key` is found in `array` of key-value pairs.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} key The key to search for.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function assocIndexOf(array, key) {
  var length = array.length;
  while (length--) {
    if (eq(array[length][0], key)) {
      return length;
    }
  }
  return -1;
}

/**
 * The base implementation of `_.forEach` without support for iteratee shorthands.
 *
 * @private
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array|Object} Returns `collection`.
 */
var baseEach = createBaseEach(baseForOwn);

/**
 * The base implementation of `baseForOwn` which iterates over `object`
 * properties returned by `keysFunc` and invokes `iteratee` for each property.
 * Iteratee functions may exit iteration early by explicitly returning `false`.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @returns {Object} Returns `object`.
 */
var baseFor = createBaseFor();

/**
 * The base implementation of `_.forOwn` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Object} Returns `object`.
 */
function baseForOwn(object, iteratee) {
  return object && baseFor(object, iteratee, keys);
}

/**
 * The base implementation of `_.get` without support for default values.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @returns {*} Returns the resolved value.
 */
function baseGet(object, path) {
  path = isKey(path, object) ? [path] : castPath(path);

  var index = 0,
      length = path.length;

  while (object != null && index < length) {
    object = object[toKey(path[index++])];
  }
  return (index && index == length) ? object : undefined;
}

/**
 * The base implementation of `getTag`.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
function baseGetTag(value) {
  return objectToString.call(value);
}

/**
 * The base implementation of `_.hasIn` without support for deep paths.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {Array|string} key The key to check.
 * @returns {boolean} Returns `true` if `key` exists, else `false`.
 */
function baseHasIn(object, key) {
  return object != null && key in Object(object);
}

/**
 * The base implementation of `_.isEqual` which supports partial comparisons
 * and tracks traversed objects.
 *
 * @private
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @param {Function} [customizer] The function to customize comparisons.
 * @param {boolean} [bitmask] The bitmask of comparison flags.
 *  The bitmask may be composed of the following flags:
 *     1 - Unordered comparison
 *     2 - Partial comparison
 * @param {Object} [stack] Tracks traversed `value` and `other` objects.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 */
function baseIsEqual(value, other, customizer, bitmask, stack) {
  if (value === other) {
    return true;
  }
  if (value == null || other == null || (!isObject(value) && !isObjectLike(other))) {
    return value !== value && other !== other;
  }
  return baseIsEqualDeep(value, other, baseIsEqual, customizer, bitmask, stack);
}

/**
 * A specialized version of `baseIsEqual` for arrays and objects which performs
 * deep comparisons and tracks traversed objects enabling objects with circular
 * references to be compared.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Function} [customizer] The function to customize comparisons.
 * @param {number} [bitmask] The bitmask of comparison flags. See `baseIsEqual`
 *  for more details.
 * @param {Object} [stack] Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function baseIsEqualDeep(object, other, equalFunc, customizer, bitmask, stack) {
  var objIsArr = isArray(object),
      othIsArr = isArray(other),
      objTag = arrayTag,
      othTag = arrayTag;

  if (!objIsArr) {
    objTag = getTag(object);
    objTag = objTag == argsTag ? objectTag : objTag;
  }
  if (!othIsArr) {
    othTag = getTag(other);
    othTag = othTag == argsTag ? objectTag : othTag;
  }
  var objIsObj = objTag == objectTag && !isHostObject(object),
      othIsObj = othTag == objectTag && !isHostObject(other),
      isSameTag = objTag == othTag;

  if (isSameTag && !objIsObj) {
    stack || (stack = new Stack);
    return (objIsArr || isTypedArray(object))
      ? equalArrays(object, other, equalFunc, customizer, bitmask, stack)
      : equalByTag(object, other, objTag, equalFunc, customizer, bitmask, stack);
  }
  if (!(bitmask & PARTIAL_COMPARE_FLAG)) {
    var objIsWrapped = objIsObj && hasOwnProperty.call(object, '__wrapped__'),
        othIsWrapped = othIsObj && hasOwnProperty.call(other, '__wrapped__');

    if (objIsWrapped || othIsWrapped) {
      var objUnwrapped = objIsWrapped ? object.value() : object,
          othUnwrapped = othIsWrapped ? other.value() : other;

      stack || (stack = new Stack);
      return equalFunc(objUnwrapped, othUnwrapped, customizer, bitmask, stack);
    }
  }
  if (!isSameTag) {
    return false;
  }
  stack || (stack = new Stack);
  return equalObjects(object, other, equalFunc, customizer, bitmask, stack);
}

/**
 * The base implementation of `_.isMatch` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The object to inspect.
 * @param {Object} source The object of property values to match.
 * @param {Array} matchData The property names, values, and compare flags to match.
 * @param {Function} [customizer] The function to customize comparisons.
 * @returns {boolean} Returns `true` if `object` is a match, else `false`.
 */
function baseIsMatch(object, source, matchData, customizer) {
  var index = matchData.length,
      length = index,
      noCustomizer = !customizer;

  if (object == null) {
    return !length;
  }
  object = Object(object);
  while (index--) {
    var data = matchData[index];
    if ((noCustomizer && data[2])
          ? data[1] !== object[data[0]]
          : !(data[0] in object)
        ) {
      return false;
    }
  }
  while (++index < length) {
    data = matchData[index];
    var key = data[0],
        objValue = object[key],
        srcValue = data[1];

    if (noCustomizer && data[2]) {
      if (objValue === undefined && !(key in object)) {
        return false;
      }
    } else {
      var stack = new Stack;
      if (customizer) {
        var result = customizer(objValue, srcValue, key, object, source, stack);
      }
      if (!(result === undefined
            ? baseIsEqual(srcValue, objValue, customizer, UNORDERED_COMPARE_FLAG | PARTIAL_COMPARE_FLAG, stack)
            : result
          )) {
        return false;
      }
    }
  }
  return true;
}

/**
 * The base implementation of `_.isNative` without bad shim checks.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function,
 *  else `false`.
 */
function baseIsNative(value) {
  if (!isObject(value) || isMasked(value)) {
    return false;
  }
  var pattern = (isFunction(value) || isHostObject(value)) ? reIsNative : reIsHostCtor;
  return pattern.test(toSource(value));
}

/**
 * The base implementation of `_.isTypedArray` without Node.js optimizations.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 */
function baseIsTypedArray(value) {
  return isObjectLike(value) &&
    isLength(value.length) && !!typedArrayTags[objectToString.call(value)];
}

/**
 * The base implementation of `_.iteratee`.
 *
 * @private
 * @param {*} [value=_.identity] The value to convert to an iteratee.
 * @returns {Function} Returns the iteratee.
 */
function baseIteratee(value) {
  // Don't store the `typeof` result in a variable to avoid a JIT bug in Safari 9.
  // See https://bugs.webkit.org/show_bug.cgi?id=156034 for more details.
  if (typeof value == 'function') {
    return value;
  }
  if (value == null) {
    return identity;
  }
  if (typeof value == 'object') {
    return isArray(value)
      ? baseMatchesProperty(value[0], value[1])
      : baseMatches(value);
  }
  return property(value);
}

/**
 * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function baseKeys(object) {
  if (!isPrototype(object)) {
    return nativeKeys(object);
  }
  var result = [];
  for (var key in Object(object)) {
    if (hasOwnProperty.call(object, key) && key != 'constructor') {
      result.push(key);
    }
  }
  return result;
}

/**
 * The base implementation of `_.matches` which doesn't clone `source`.
 *
 * @private
 * @param {Object} source The object of property values to match.
 * @returns {Function} Returns the new spec function.
 */
function baseMatches(source) {
  var matchData = getMatchData(source);
  if (matchData.length == 1 && matchData[0][2]) {
    return matchesStrictComparable(matchData[0][0], matchData[0][1]);
  }
  return function(object) {
    return object === source || baseIsMatch(object, source, matchData);
  };
}

/**
 * The base implementation of `_.matchesProperty` which doesn't clone `srcValue`.
 *
 * @private
 * @param {string} path The path of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */
function baseMatchesProperty(path, srcValue) {
  if (isKey(path) && isStrictComparable(srcValue)) {
    return matchesStrictComparable(toKey(path), srcValue);
  }
  return function(object) {
    var objValue = get(object, path);
    return (objValue === undefined && objValue === srcValue)
      ? hasIn(object, path)
      : baseIsEqual(srcValue, objValue, undefined, UNORDERED_COMPARE_FLAG | PARTIAL_COMPARE_FLAG);
  };
}

/**
 * A specialized version of `baseProperty` which supports deep paths.
 *
 * @private
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function basePropertyDeep(path) {
  return function(object) {
    return baseGet(object, path);
  };
}

/**
 * The base implementation of `_.toString` which doesn't convert nullish
 * values to empty strings.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {string} Returns the string.
 */
function baseToString(value) {
  // Exit early for strings to avoid a performance hit in some environments.
  if (typeof value == 'string') {
    return value;
  }
  if (isSymbol(value)) {
    return symbolToString ? symbolToString.call(value) : '';
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
}

/**
 * Casts `value` to a path array if it's not one.
 *
 * @private
 * @param {*} value The value to inspect.
 * @returns {Array} Returns the cast property path array.
 */
function castPath(value) {
  return isArray(value) ? value : stringToPath(value);
}

/**
 * Creates a `baseEach` or `baseEachRight` function.
 *
 * @private
 * @param {Function} eachFunc The function to iterate over a collection.
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseEach(eachFunc, fromRight) {
  return function(collection, iteratee) {
    if (collection == null) {
      return collection;
    }
    if (!isArrayLike(collection)) {
      return eachFunc(collection, iteratee);
    }
    var length = collection.length,
        index = fromRight ? length : -1,
        iterable = Object(collection);

    while ((fromRight ? index-- : ++index < length)) {
      if (iteratee(iterable[index], index, iterable) === false) {
        break;
      }
    }
    return collection;
  };
}

/**
 * Creates a base function for methods like `_.forIn` and `_.forOwn`.
 *
 * @private
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseFor(fromRight) {
  return function(object, iteratee, keysFunc) {
    var index = -1,
        iterable = Object(object),
        props = keysFunc(object),
        length = props.length;

    while (length--) {
      var key = props[fromRight ? length : ++index];
      if (iteratee(iterable[key], key, iterable) === false) {
        break;
      }
    }
    return object;
  };
}

/**
 * A specialized version of `baseIsEqualDeep` for arrays with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Array} array The array to compare.
 * @param {Array} other The other array to compare.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Function} customizer The function to customize comparisons.
 * @param {number} bitmask The bitmask of comparison flags. See `baseIsEqual`
 *  for more details.
 * @param {Object} stack Tracks traversed `array` and `other` objects.
 * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
 */
function equalArrays(array, other, equalFunc, customizer, bitmask, stack) {
  var isPartial = bitmask & PARTIAL_COMPARE_FLAG,
      arrLength = array.length,
      othLength = other.length;

  if (arrLength != othLength && !(isPartial && othLength > arrLength)) {
    return false;
  }
  // Assume cyclic values are equal.
  var stacked = stack.get(array);
  if (stacked && stack.get(other)) {
    return stacked == other;
  }
  var index = -1,
      result = true,
      seen = (bitmask & UNORDERED_COMPARE_FLAG) ? new SetCache : undefined;

  stack.set(array, other);
  stack.set(other, array);

  // Ignore non-index properties.
  while (++index < arrLength) {
    var arrValue = array[index],
        othValue = other[index];

    if (customizer) {
      var compared = isPartial
        ? customizer(othValue, arrValue, index, other, array, stack)
        : customizer(arrValue, othValue, index, array, other, stack);
    }
    if (compared !== undefined) {
      if (compared) {
        continue;
      }
      result = false;
      break;
    }
    // Recursively compare arrays (susceptible to call stack limits).
    if (seen) {
      if (!arraySome(other, function(othValue, othIndex) {
            if (!seen.has(othIndex) &&
                (arrValue === othValue || equalFunc(arrValue, othValue, customizer, bitmask, stack))) {
              return seen.add(othIndex);
            }
          })) {
        result = false;
        break;
      }
    } else if (!(
          arrValue === othValue ||
            equalFunc(arrValue, othValue, customizer, bitmask, stack)
        )) {
      result = false;
      break;
    }
  }
  stack['delete'](array);
  stack['delete'](other);
  return result;
}

/**
 * A specialized version of `baseIsEqualDeep` for comparing objects of
 * the same `toStringTag`.
 *
 * **Note:** This function only supports comparing values with tags of
 * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {string} tag The `toStringTag` of the objects to compare.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Function} customizer The function to customize comparisons.
 * @param {number} bitmask The bitmask of comparison flags. See `baseIsEqual`
 *  for more details.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalByTag(object, other, tag, equalFunc, customizer, bitmask, stack) {
  switch (tag) {
    case dataViewTag:
      if ((object.byteLength != other.byteLength) ||
          (object.byteOffset != other.byteOffset)) {
        return false;
      }
      object = object.buffer;
      other = other.buffer;

    case arrayBufferTag:
      if ((object.byteLength != other.byteLength) ||
          !equalFunc(new Uint8Array(object), new Uint8Array(other))) {
        return false;
      }
      return true;

    case boolTag:
    case dateTag:
    case numberTag:
      // Coerce booleans to `1` or `0` and dates to milliseconds.
      // Invalid dates are coerced to `NaN`.
      return eq(+object, +other);

    case errorTag:
      return object.name == other.name && object.message == other.message;

    case regexpTag:
    case stringTag:
      // Coerce regexes to strings and treat strings, primitives and objects,
      // as equal. See http://www.ecma-international.org/ecma-262/7.0/#sec-regexp.prototype.tostring
      // for more details.
      return object == (other + '');

    case mapTag:
      var convert = mapToArray;

    case setTag:
      var isPartial = bitmask & PARTIAL_COMPARE_FLAG;
      convert || (convert = setToArray);

      if (object.size != other.size && !isPartial) {
        return false;
      }
      // Assume cyclic values are equal.
      var stacked = stack.get(object);
      if (stacked) {
        return stacked == other;
      }
      bitmask |= UNORDERED_COMPARE_FLAG;

      // Recursively compare objects (susceptible to call stack limits).
      stack.set(object, other);
      var result = equalArrays(convert(object), convert(other), equalFunc, customizer, bitmask, stack);
      stack['delete'](object);
      return result;

    case symbolTag:
      if (symbolValueOf) {
        return symbolValueOf.call(object) == symbolValueOf.call(other);
      }
  }
  return false;
}

/**
 * A specialized version of `baseIsEqualDeep` for objects with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Function} customizer The function to customize comparisons.
 * @param {number} bitmask The bitmask of comparison flags. See `baseIsEqual`
 *  for more details.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalObjects(object, other, equalFunc, customizer, bitmask, stack) {
  var isPartial = bitmask & PARTIAL_COMPARE_FLAG,
      objProps = keys(object),
      objLength = objProps.length,
      othProps = keys(other),
      othLength = othProps.length;

  if (objLength != othLength && !isPartial) {
    return false;
  }
  var index = objLength;
  while (index--) {
    var key = objProps[index];
    if (!(isPartial ? key in other : hasOwnProperty.call(other, key))) {
      return false;
    }
  }
  // Assume cyclic values are equal.
  var stacked = stack.get(object);
  if (stacked && stack.get(other)) {
    return stacked == other;
  }
  var result = true;
  stack.set(object, other);
  stack.set(other, object);

  var skipCtor = isPartial;
  while (++index < objLength) {
    key = objProps[index];
    var objValue = object[key],
        othValue = other[key];

    if (customizer) {
      var compared = isPartial
        ? customizer(othValue, objValue, key, other, object, stack)
        : customizer(objValue, othValue, key, object, other, stack);
    }
    // Recursively compare objects (susceptible to call stack limits).
    if (!(compared === undefined
          ? (objValue === othValue || equalFunc(objValue, othValue, customizer, bitmask, stack))
          : compared
        )) {
      result = false;
      break;
    }
    skipCtor || (skipCtor = key == 'constructor');
  }
  if (result && !skipCtor) {
    var objCtor = object.constructor,
        othCtor = other.constructor;

    // Non `Object` object instances with different constructors are not equal.
    if (objCtor != othCtor &&
        ('constructor' in object && 'constructor' in other) &&
        !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
          typeof othCtor == 'function' && othCtor instanceof othCtor)) {
      result = false;
    }
  }
  stack['delete'](object);
  stack['delete'](other);
  return result;
}

/**
 * Gets the data for `map`.
 *
 * @private
 * @param {Object} map The map to query.
 * @param {string} key The reference key.
 * @returns {*} Returns the map data.
 */
function getMapData(map, key) {
  var data = map.__data__;
  return isKeyable(key)
    ? data[typeof key == 'string' ? 'string' : 'hash']
    : data.map;
}

/**
 * Gets the property names, values, and compare flags of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the match data of `object`.
 */
function getMatchData(object) {
  var result = keys(object),
      length = result.length;

  while (length--) {
    var key = result[length],
        value = object[key];

    result[length] = [key, value, isStrictComparable(value)];
  }
  return result;
}

/**
 * Gets the native function at `key` of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the method to get.
 * @returns {*} Returns the function if it's native, else `undefined`.
 */
function getNative(object, key) {
  var value = getValue(object, key);
  return baseIsNative(value) ? value : undefined;
}

/**
 * Gets the `toStringTag` of `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
var getTag = baseGetTag;

// Fallback for data views, maps, sets, and weak maps in IE 11,
// for data views in Edge < 14, and promises in Node.js.
if ((DataView && getTag(new DataView(new ArrayBuffer(1))) != dataViewTag) ||
    (Map && getTag(new Map) != mapTag) ||
    (Promise && getTag(Promise.resolve()) != promiseTag) ||
    (Set && getTag(new Set) != setTag) ||
    (WeakMap && getTag(new WeakMap) != weakMapTag)) {
  getTag = function(value) {
    var result = objectToString.call(value),
        Ctor = result == objectTag ? value.constructor : undefined,
        ctorString = Ctor ? toSource(Ctor) : undefined;

    if (ctorString) {
      switch (ctorString) {
        case dataViewCtorString: return dataViewTag;
        case mapCtorString: return mapTag;
        case promiseCtorString: return promiseTag;
        case setCtorString: return setTag;
        case weakMapCtorString: return weakMapTag;
      }
    }
    return result;
  };
}

/**
 * Checks if `path` exists on `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @param {Function} hasFunc The function to check properties.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 */
function hasPath(object, path, hasFunc) {
  path = isKey(path, object) ? [path] : castPath(path);

  var result,
      index = -1,
      length = path.length;

  while (++index < length) {
    var key = toKey(path[index]);
    if (!(result = object != null && hasFunc(object, key))) {
      break;
    }
    object = object[key];
  }
  if (result) {
    return result;
  }
  var length = object ? object.length : 0;
  return !!length && isLength(length) && isIndex(key, length) &&
    (isArray(object) || isArguments(object));
}

/**
 * Checks if `value` is a valid array-like index.
 *
 * @private
 * @param {*} value The value to check.
 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
function isIndex(value, length) {
  length = length == null ? MAX_SAFE_INTEGER : length;
  return !!length &&
    (typeof value == 'number' || reIsUint.test(value)) &&
    (value > -1 && value % 1 == 0 && value < length);
}

/**
 * Checks if `value` is a property name and not a property path.
 *
 * @private
 * @param {*} value The value to check.
 * @param {Object} [object] The object to query keys on.
 * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
 */
function isKey(value, object) {
  if (isArray(value)) {
    return false;
  }
  var type = typeof value;
  if (type == 'number' || type == 'symbol' || type == 'boolean' ||
      value == null || isSymbol(value)) {
    return true;
  }
  return reIsPlainProp.test(value) || !reIsDeepProp.test(value) ||
    (object != null && value in Object(object));
}

/**
 * Checks if `value` is suitable for use as unique object key.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
 */
function isKeyable(value) {
  var type = typeof value;
  return (type == 'string' || type == 'number' || type == 'symbol' || type == 'boolean')
    ? (value !== '__proto__')
    : (value === null);
}

/**
 * Checks if `func` has its source masked.
 *
 * @private
 * @param {Function} func The function to check.
 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
 */
function isMasked(func) {
  return !!maskSrcKey && (maskSrcKey in func);
}

/**
 * Checks if `value` is likely a prototype object.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
 */
function isPrototype(value) {
  var Ctor = value && value.constructor,
      proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto;

  return value === proto;
}

/**
 * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` if suitable for strict
 *  equality comparisons, else `false`.
 */
function isStrictComparable(value) {
  return value === value && !isObject(value);
}

/**
 * A specialized version of `matchesProperty` for source values suitable
 * for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */
function matchesStrictComparable(key, srcValue) {
  return function(object) {
    if (object == null) {
      return false;
    }
    return object[key] === srcValue &&
      (srcValue !== undefined || (key in Object(object)));
  };
}

/**
 * Converts `string` to a property path array.
 *
 * @private
 * @param {string} string The string to convert.
 * @returns {Array} Returns the property path array.
 */
var stringToPath = memoize(function(string) {
  string = toString(string);

  var result = [];
  if (reLeadingDot.test(string)) {
    result.push('');
  }
  string.replace(rePropName, function(match, number, quote, string) {
    result.push(quote ? string.replace(reEscapeChar, '$1') : (number || match));
  });
  return result;
});

/**
 * Converts `value` to a string key if it's not a string or symbol.
 *
 * @private
 * @param {*} value The value to inspect.
 * @returns {string|symbol} Returns the key.
 */
function toKey(value) {
  if (typeof value == 'string' || isSymbol(value)) {
    return value;
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
}

/**
 * Converts `func` to its source code.
 *
 * @private
 * @param {Function} func The function to process.
 * @returns {string} Returns the source code.
 */
function toSource(func) {
  if (func != null) {
    try {
      return funcToString.call(func);
    } catch (e) {}
    try {
      return (func + '');
    } catch (e) {}
  }
  return '';
}

/**
 * Reduces `collection` to a value which is the accumulated result of running
 * each element in `collection` thru `iteratee`, where each successive
 * invocation is supplied the return value of the previous. If `accumulator`
 * is not given, the first element of `collection` is used as the initial
 * value. The iteratee is invoked with four arguments:
 * (accumulator, value, index|key, collection).
 *
 * Many lodash methods are guarded to work as iteratees for methods like
 * `_.reduce`, `_.reduceRight`, and `_.transform`.
 *
 * The guarded methods are:
 * `assign`, `defaults`, `defaultsDeep`, `includes`, `merge`, `orderBy`,
 * and `sortBy`
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Collection
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @param {*} [accumulator] The initial value.
 * @returns {*} Returns the accumulated value.
 * @see _.reduceRight
 * @example
 *
 * _.reduce([1, 2], function(sum, n) {
 *   return sum + n;
 * }, 0);
 * // => 3
 *
 * _.reduce({ 'a': 1, 'b': 2, 'c': 1 }, function(result, value, key) {
 *   (result[value] || (result[value] = [])).push(key);
 *   return result;
 * }, {});
 * // => { '1': ['a', 'c'], '2': ['b'] } (iteration order is not guaranteed)
 */
function reduce(collection, iteratee, accumulator) {
  var func = isArray(collection) ? arrayReduce : baseReduce,
      initAccum = arguments.length < 3;

  return func(collection, baseIteratee(iteratee, 4), accumulator, initAccum, baseEach);
}

/**
 * Creates a function that memoizes the result of `func`. If `resolver` is
 * provided, it determines the cache key for storing the result based on the
 * arguments provided to the memoized function. By default, the first argument
 * provided to the memoized function is used as the map cache key. The `func`
 * is invoked with the `this` binding of the memoized function.
 *
 * **Note:** The cache is exposed as the `cache` property on the memoized
 * function. Its creation may be customized by replacing the `_.memoize.Cache`
 * constructor with one whose instances implement the
 * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
 * method interface of `delete`, `get`, `has`, and `set`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to have its output memoized.
 * @param {Function} [resolver] The function to resolve the cache key.
 * @returns {Function} Returns the new memoized function.
 * @example
 *
 * var object = { 'a': 1, 'b': 2 };
 * var other = { 'c': 3, 'd': 4 };
 *
 * var values = _.memoize(_.values);
 * values(object);
 * // => [1, 2]
 *
 * values(other);
 * // => [3, 4]
 *
 * object.a = 2;
 * values(object);
 * // => [1, 2]
 *
 * // Modify the result cache.
 * values.cache.set(object, ['a', 'b']);
 * values(object);
 * // => ['a', 'b']
 *
 * // Replace `_.memoize.Cache`.
 * _.memoize.Cache = WeakMap;
 */
function memoize(func, resolver) {
  if (typeof func != 'function' || (resolver && typeof resolver != 'function')) {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  var memoized = function() {
    var args = arguments,
        key = resolver ? resolver.apply(this, args) : args[0],
        cache = memoized.cache;

    if (cache.has(key)) {
      return cache.get(key);
    }
    var result = func.apply(this, args);
    memoized.cache = cache.set(key, result);
    return result;
  };
  memoized.cache = new (memoize.Cache || MapCache);
  return memoized;
}

// Assign cache to `_.memoize`.
memoize.Cache = MapCache;

/**
 * Performs a
 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * comparison between two values to determine if they are equivalent.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.eq(object, object);
 * // => true
 *
 * _.eq(object, other);
 * // => false
 *
 * _.eq('a', 'a');
 * // => true
 *
 * _.eq('a', Object('a'));
 * // => false
 *
 * _.eq(NaN, NaN);
 * // => true
 */
function eq(value, other) {
  return value === other || (value !== value && other !== other);
}

/**
 * Checks if `value` is likely an `arguments` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 *  else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
function isArguments(value) {
  // Safari 8.1 makes `arguments.callee` enumerable in strict mode.
  return isArrayLikeObject(value) && hasOwnProperty.call(value, 'callee') &&
    (!propertyIsEnumerable.call(value, 'callee') || objectToString.call(value) == argsTag);
}

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

/**
 * Checks if `value` is array-like. A value is considered array-like if it's
 * not a function and has a `value.length` that's an integer greater than or
 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 * @example
 *
 * _.isArrayLike([1, 2, 3]);
 * // => true
 *
 * _.isArrayLike(document.body.children);
 * // => true
 *
 * _.isArrayLike('abc');
 * // => true
 *
 * _.isArrayLike(_.noop);
 * // => false
 */
function isArrayLike(value) {
  return value != null && isLength(value.length) && !isFunction(value);
}

/**
 * This method is like `_.isArrayLike` except that it also checks if `value`
 * is an object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array-like object,
 *  else `false`.
 * @example
 *
 * _.isArrayLikeObject([1, 2, 3]);
 * // => true
 *
 * _.isArrayLikeObject(document.body.children);
 * // => true
 *
 * _.isArrayLikeObject('abc');
 * // => false
 *
 * _.isArrayLikeObject(_.noop);
 * // => false
 */
function isArrayLikeObject(value) {
  return isObjectLike(value) && isArrayLike(value);
}

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 8-9 which returns 'object' for typed array and other constructors.
  var tag = isObject(value) ? objectToString.call(value) : '';
  return tag == funcTag || tag == genTag;
}

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This method is loosely based on
 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 * @example
 *
 * _.isLength(3);
 * // => true
 *
 * _.isLength(Number.MIN_VALUE);
 * // => false
 *
 * _.isLength(Infinity);
 * // => false
 *
 * _.isLength('3');
 * // => false
 */
function isLength(value) {
  return typeof value == 'number' &&
    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return !!value && (type == 'object' || type == 'function');
}

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return !!value && typeof value == 'object';
}

/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */
function isSymbol(value) {
  return typeof value == 'symbol' ||
    (isObjectLike(value) && objectToString.call(value) == symbolTag);
}

/**
 * Checks if `value` is classified as a typed array.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 * @example
 *
 * _.isTypedArray(new Uint8Array);
 * // => true
 *
 * _.isTypedArray([]);
 * // => false
 */
var isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;

/**
 * Converts `value` to a string. An empty string is returned for `null`
 * and `undefined` values. The sign of `-0` is preserved.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to process.
 * @returns {string} Returns the string.
 * @example
 *
 * _.toString(null);
 * // => ''
 *
 * _.toString(-0);
 * // => '-0'
 *
 * _.toString([1, 2, 3]);
 * // => '1,2,3'
 */
function toString(value) {
  return value == null ? '' : baseToString(value);
}

/**
 * Gets the value at `path` of `object`. If the resolved value is
 * `undefined`, the `defaultValue` is returned in its place.
 *
 * @static
 * @memberOf _
 * @since 3.7.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @param {*} [defaultValue] The value returned for `undefined` resolved values.
 * @returns {*} Returns the resolved value.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c': 3 } }] };
 *
 * _.get(object, 'a[0].b.c');
 * // => 3
 *
 * _.get(object, ['a', '0', 'b', 'c']);
 * // => 3
 *
 * _.get(object, 'a.b.c', 'default');
 * // => 'default'
 */
function get(object, path, defaultValue) {
  var result = object == null ? undefined : baseGet(object, path);
  return result === undefined ? defaultValue : result;
}

/**
 * Checks if `path` is a direct or inherited property of `object`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 * @example
 *
 * var object = _.create({ 'a': _.create({ 'b': 2 }) });
 *
 * _.hasIn(object, 'a');
 * // => true
 *
 * _.hasIn(object, 'a.b');
 * // => true
 *
 * _.hasIn(object, ['a', 'b']);
 * // => true
 *
 * _.hasIn(object, 'b');
 * // => false
 */
function hasIn(object, path) {
  return object != null && hasPath(object, path, baseHasIn);
}

/**
 * Creates an array of the own enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects. See the
 * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
 * for more details.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keys(new Foo);
 * // => ['a', 'b'] (iteration order is not guaranteed)
 *
 * _.keys('hi');
 * // => ['0', '1']
 */
function keys(object) {
  return isArrayLike(object) ? arrayLikeKeys(object) : baseKeys(object);
}

/**
 * This method returns the first argument it receives.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Util
 * @param {*} value Any value.
 * @returns {*} Returns `value`.
 * @example
 *
 * var object = { 'a': 1 };
 *
 * console.log(_.identity(object) === object);
 * // => true
 */
function identity(value) {
  return value;
}

/**
 * Creates a function that returns the value at `path` of a given object.
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Util
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 * @example
 *
 * var objects = [
 *   { 'a': { 'b': 2 } },
 *   { 'a': { 'b': 1 } }
 * ];
 *
 * _.map(objects, _.property('a.b'));
 * // => [2, 1]
 *
 * _.map(_.sortBy(objects, _.property(['a', 'b'])), 'a.b');
 * // => [1, 2]
 */
function property(path) {
  return isKey(path) ? baseProperty(toKey(path)) : basePropertyDeep(path);
}

module.exports = reduce;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],6:[function(require,module,exports){
(function (global){
/**
 * lodash (Custom Build) <https://lodash.com/>
 * Build: `lodash modularize exports="npm" -o ./`
 * Copyright jQuery Foundation and other contributors <https://jquery.org/>
 * Released under MIT license <https://lodash.com/license>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 */

/** Used as the size to enable large array optimizations. */
var LARGE_ARRAY_SIZE = 200;

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0;

/** `Object#toString` result references. */
var funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]';

/**
 * Used to match `RegExp`
 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
 */
var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;

/** Used to detect host constructors (Safari). */
var reIsHostCtor = /^\[object .+?Constructor\]$/;

/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

/**
 * A specialized version of `_.includes` for arrays without support for
 * specifying an index to search from.
 *
 * @private
 * @param {Array} [array] The array to inspect.
 * @param {*} target The value to search for.
 * @returns {boolean} Returns `true` if `target` is found, else `false`.
 */
function arrayIncludes(array, value) {
  var length = array ? array.length : 0;
  return !!length && baseIndexOf(array, value, 0) > -1;
}

/**
 * This function is like `arrayIncludes` except that it accepts a comparator.
 *
 * @private
 * @param {Array} [array] The array to inspect.
 * @param {*} target The value to search for.
 * @param {Function} comparator The comparator invoked per element.
 * @returns {boolean} Returns `true` if `target` is found, else `false`.
 */
function arrayIncludesWith(array, value, comparator) {
  var index = -1,
      length = array ? array.length : 0;

  while (++index < length) {
    if (comparator(value, array[index])) {
      return true;
    }
  }
  return false;
}

/**
 * The base implementation of `_.findIndex` and `_.findLastIndex` without
 * support for iteratee shorthands.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {Function} predicate The function invoked per iteration.
 * @param {number} fromIndex The index to search from.
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function baseFindIndex(array, predicate, fromIndex, fromRight) {
  var length = array.length,
      index = fromIndex + (fromRight ? 1 : -1);

  while ((fromRight ? index-- : ++index < length)) {
    if (predicate(array[index], index, array)) {
      return index;
    }
  }
  return -1;
}

/**
 * The base implementation of `_.indexOf` without `fromIndex` bounds checks.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} value The value to search for.
 * @param {number} fromIndex The index to search from.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function baseIndexOf(array, value, fromIndex) {
  if (value !== value) {
    return baseFindIndex(array, baseIsNaN, fromIndex);
  }
  var index = fromIndex - 1,
      length = array.length;

  while (++index < length) {
    if (array[index] === value) {
      return index;
    }
  }
  return -1;
}

/**
 * The base implementation of `_.isNaN` without support for number objects.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
 */
function baseIsNaN(value) {
  return value !== value;
}

/**
 * Checks if a cache value for `key` exists.
 *
 * @private
 * @param {Object} cache The cache to query.
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function cacheHas(cache, key) {
  return cache.has(key);
}

/**
 * Gets the value at `key` of `object`.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {string} key The key of the property to get.
 * @returns {*} Returns the property value.
 */
function getValue(object, key) {
  return object == null ? undefined : object[key];
}

/**
 * Checks if `value` is a host object in IE < 9.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a host object, else `false`.
 */
function isHostObject(value) {
  // Many host objects are `Object` objects that can coerce to strings
  // despite having improperly defined `toString` methods.
  var result = false;
  if (value != null && typeof value.toString != 'function') {
    try {
      result = !!(value + '');
    } catch (e) {}
  }
  return result;
}

/**
 * Converts `set` to an array of its values.
 *
 * @private
 * @param {Object} set The set to convert.
 * @returns {Array} Returns the values.
 */
function setToArray(set) {
  var index = -1,
      result = Array(set.size);

  set.forEach(function(value) {
    result[++index] = value;
  });
  return result;
}

/** Used for built-in method references. */
var arrayProto = Array.prototype,
    funcProto = Function.prototype,
    objectProto = Object.prototype;

/** Used to detect overreaching core-js shims. */
var coreJsData = root['__core-js_shared__'];

/** Used to detect methods masquerading as native. */
var maskSrcKey = (function() {
  var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || '');
  return uid ? ('Symbol(src)_1.' + uid) : '';
}());

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var objectToString = objectProto.toString;

/** Used to detect if a method is native. */
var reIsNative = RegExp('^' +
  funcToString.call(hasOwnProperty).replace(reRegExpChar, '\\$&')
  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
);

/** Built-in value references. */
var splice = arrayProto.splice;

/* Built-in method references that are verified to be native. */
var Map = getNative(root, 'Map'),
    Set = getNative(root, 'Set'),
    nativeCreate = getNative(Object, 'create');

/**
 * Creates a hash object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function Hash(entries) {
  var index = -1,
      length = entries ? entries.length : 0;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

/**
 * Removes all key-value entries from the hash.
 *
 * @private
 * @name clear
 * @memberOf Hash
 */
function hashClear() {
  this.__data__ = nativeCreate ? nativeCreate(null) : {};
}

/**
 * Removes `key` and its value from the hash.
 *
 * @private
 * @name delete
 * @memberOf Hash
 * @param {Object} hash The hash to modify.
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function hashDelete(key) {
  return this.has(key) && delete this.__data__[key];
}

/**
 * Gets the hash value for `key`.
 *
 * @private
 * @name get
 * @memberOf Hash
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function hashGet(key) {
  var data = this.__data__;
  if (nativeCreate) {
    var result = data[key];
    return result === HASH_UNDEFINED ? undefined : result;
  }
  return hasOwnProperty.call(data, key) ? data[key] : undefined;
}

/**
 * Checks if a hash value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Hash
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function hashHas(key) {
  var data = this.__data__;
  return nativeCreate ? data[key] !== undefined : hasOwnProperty.call(data, key);
}

/**
 * Sets the hash `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Hash
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the hash instance.
 */
function hashSet(key, value) {
  var data = this.__data__;
  data[key] = (nativeCreate && value === undefined) ? HASH_UNDEFINED : value;
  return this;
}

// Add methods to `Hash`.
Hash.prototype.clear = hashClear;
Hash.prototype['delete'] = hashDelete;
Hash.prototype.get = hashGet;
Hash.prototype.has = hashHas;
Hash.prototype.set = hashSet;

/**
 * Creates an list cache object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function ListCache(entries) {
  var index = -1,
      length = entries ? entries.length : 0;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

/**
 * Removes all key-value entries from the list cache.
 *
 * @private
 * @name clear
 * @memberOf ListCache
 */
function listCacheClear() {
  this.__data__ = [];
}

/**
 * Removes `key` and its value from the list cache.
 *
 * @private
 * @name delete
 * @memberOf ListCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function listCacheDelete(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    return false;
  }
  var lastIndex = data.length - 1;
  if (index == lastIndex) {
    data.pop();
  } else {
    splice.call(data, index, 1);
  }
  return true;
}

/**
 * Gets the list cache value for `key`.
 *
 * @private
 * @name get
 * @memberOf ListCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function listCacheGet(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  return index < 0 ? undefined : data[index][1];
}

/**
 * Checks if a list cache value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf ListCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function listCacheHas(key) {
  return assocIndexOf(this.__data__, key) > -1;
}

/**
 * Sets the list cache `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf ListCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the list cache instance.
 */
function listCacheSet(key, value) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    data.push([key, value]);
  } else {
    data[index][1] = value;
  }
  return this;
}

// Add methods to `ListCache`.
ListCache.prototype.clear = listCacheClear;
ListCache.prototype['delete'] = listCacheDelete;
ListCache.prototype.get = listCacheGet;
ListCache.prototype.has = listCacheHas;
ListCache.prototype.set = listCacheSet;

/**
 * Creates a map cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function MapCache(entries) {
  var index = -1,
      length = entries ? entries.length : 0;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

/**
 * Removes all key-value entries from the map.
 *
 * @private
 * @name clear
 * @memberOf MapCache
 */
function mapCacheClear() {
  this.__data__ = {
    'hash': new Hash,
    'map': new (Map || ListCache),
    'string': new Hash
  };
}

/**
 * Removes `key` and its value from the map.
 *
 * @private
 * @name delete
 * @memberOf MapCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function mapCacheDelete(key) {
  return getMapData(this, key)['delete'](key);
}

/**
 * Gets the map value for `key`.
 *
 * @private
 * @name get
 * @memberOf MapCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function mapCacheGet(key) {
  return getMapData(this, key).get(key);
}

/**
 * Checks if a map value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf MapCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function mapCacheHas(key) {
  return getMapData(this, key).has(key);
}

/**
 * Sets the map `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf MapCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the map cache instance.
 */
function mapCacheSet(key, value) {
  getMapData(this, key).set(key, value);
  return this;
}

// Add methods to `MapCache`.
MapCache.prototype.clear = mapCacheClear;
MapCache.prototype['delete'] = mapCacheDelete;
MapCache.prototype.get = mapCacheGet;
MapCache.prototype.has = mapCacheHas;
MapCache.prototype.set = mapCacheSet;

/**
 *
 * Creates an array cache object to store unique values.
 *
 * @private
 * @constructor
 * @param {Array} [values] The values to cache.
 */
function SetCache(values) {
  var index = -1,
      length = values ? values.length : 0;

  this.__data__ = new MapCache;
  while (++index < length) {
    this.add(values[index]);
  }
}

/**
 * Adds `value` to the array cache.
 *
 * @private
 * @name add
 * @memberOf SetCache
 * @alias push
 * @param {*} value The value to cache.
 * @returns {Object} Returns the cache instance.
 */
function setCacheAdd(value) {
  this.__data__.set(value, HASH_UNDEFINED);
  return this;
}

/**
 * Checks if `value` is in the array cache.
 *
 * @private
 * @name has
 * @memberOf SetCache
 * @param {*} value The value to search for.
 * @returns {number} Returns `true` if `value` is found, else `false`.
 */
function setCacheHas(value) {
  return this.__data__.has(value);
}

// Add methods to `SetCache`.
SetCache.prototype.add = SetCache.prototype.push = setCacheAdd;
SetCache.prototype.has = setCacheHas;

/**
 * Gets the index at which the `key` is found in `array` of key-value pairs.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} key The key to search for.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function assocIndexOf(array, key) {
  var length = array.length;
  while (length--) {
    if (eq(array[length][0], key)) {
      return length;
    }
  }
  return -1;
}

/**
 * The base implementation of `_.isNative` without bad shim checks.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function,
 *  else `false`.
 */
function baseIsNative(value) {
  if (!isObject(value) || isMasked(value)) {
    return false;
  }
  var pattern = (isFunction(value) || isHostObject(value)) ? reIsNative : reIsHostCtor;
  return pattern.test(toSource(value));
}

/**
 * The base implementation of `_.uniqBy` without support for iteratee shorthands.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {Function} [iteratee] The iteratee invoked per element.
 * @param {Function} [comparator] The comparator invoked per element.
 * @returns {Array} Returns the new duplicate free array.
 */
function baseUniq(array, iteratee, comparator) {
  var index = -1,
      includes = arrayIncludes,
      length = array.length,
      isCommon = true,
      result = [],
      seen = result;

  if (comparator) {
    isCommon = false;
    includes = arrayIncludesWith;
  }
  else if (length >= LARGE_ARRAY_SIZE) {
    var set = iteratee ? null : createSet(array);
    if (set) {
      return setToArray(set);
    }
    isCommon = false;
    includes = cacheHas;
    seen = new SetCache;
  }
  else {
    seen = iteratee ? [] : result;
  }
  outer:
  while (++index < length) {
    var value = array[index],
        computed = iteratee ? iteratee(value) : value;

    value = (comparator || value !== 0) ? value : 0;
    if (isCommon && computed === computed) {
      var seenIndex = seen.length;
      while (seenIndex--) {
        if (seen[seenIndex] === computed) {
          continue outer;
        }
      }
      if (iteratee) {
        seen.push(computed);
      }
      result.push(value);
    }
    else if (!includes(seen, computed, comparator)) {
      if (seen !== result) {
        seen.push(computed);
      }
      result.push(value);
    }
  }
  return result;
}

/**
 * Creates a set object of `values`.
 *
 * @private
 * @param {Array} values The values to add to the set.
 * @returns {Object} Returns the new set.
 */
var createSet = !(Set && (1 / setToArray(new Set([,-0]))[1]) == INFINITY) ? noop : function(values) {
  return new Set(values);
};

/**
 * Gets the data for `map`.
 *
 * @private
 * @param {Object} map The map to query.
 * @param {string} key The reference key.
 * @returns {*} Returns the map data.
 */
function getMapData(map, key) {
  var data = map.__data__;
  return isKeyable(key)
    ? data[typeof key == 'string' ? 'string' : 'hash']
    : data.map;
}

/**
 * Gets the native function at `key` of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the method to get.
 * @returns {*} Returns the function if it's native, else `undefined`.
 */
function getNative(object, key) {
  var value = getValue(object, key);
  return baseIsNative(value) ? value : undefined;
}

/**
 * Checks if `value` is suitable for use as unique object key.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
 */
function isKeyable(value) {
  var type = typeof value;
  return (type == 'string' || type == 'number' || type == 'symbol' || type == 'boolean')
    ? (value !== '__proto__')
    : (value === null);
}

/**
 * Checks if `func` has its source masked.
 *
 * @private
 * @param {Function} func The function to check.
 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
 */
function isMasked(func) {
  return !!maskSrcKey && (maskSrcKey in func);
}

/**
 * Converts `func` to its source code.
 *
 * @private
 * @param {Function} func The function to process.
 * @returns {string} Returns the source code.
 */
function toSource(func) {
  if (func != null) {
    try {
      return funcToString.call(func);
    } catch (e) {}
    try {
      return (func + '');
    } catch (e) {}
  }
  return '';
}

/**
 * Creates a duplicate-free version of an array, using
 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * for equality comparisons, in which only the first occurrence of each
 * element is kept.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Array
 * @param {Array} array The array to inspect.
 * @returns {Array} Returns the new duplicate free array.
 * @example
 *
 * _.uniq([2, 1, 2]);
 * // => [2, 1]
 */
function uniq(array) {
  return (array && array.length)
    ? baseUniq(array)
    : [];
}

/**
 * Performs a
 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * comparison between two values to determine if they are equivalent.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.eq(object, object);
 * // => true
 *
 * _.eq(object, other);
 * // => false
 *
 * _.eq('a', 'a');
 * // => true
 *
 * _.eq('a', Object('a'));
 * // => false
 *
 * _.eq(NaN, NaN);
 * // => true
 */
function eq(value, other) {
  return value === other || (value !== value && other !== other);
}

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 8-9 which returns 'object' for typed array and other constructors.
  var tag = isObject(value) ? objectToString.call(value) : '';
  return tag == funcTag || tag == genTag;
}

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return !!value && (type == 'object' || type == 'function');
}

/**
 * This method returns `undefined`.
 *
 * @static
 * @memberOf _
 * @since 2.3.0
 * @category Util
 * @example
 *
 * _.times(2, _.noop);
 * // => [undefined, undefined]
 */
function noop() {
  // No operation performed.
}

module.exports = uniq;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],7:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _childProcessPromise = require('child-process-promise');

var _tmpFile = require('tmp-file');

var _tmpFile2 = _interopRequireDefault(_tmpFile);

var _loadJsonFile = require('load-json-file');

var _loadJsonFile2 = _interopRequireDefault(_loadJsonFile);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

const config = {
  classPath: 'corenlp/stanford-corenlp-full-2017-06-09/*',
  mainClass: 'edu.stanford.nlp.pipeline.StanfordCoreNLP',
  props: 'StanfordCoreNLP-spanish.properties'
};

/**
 * @class
 * @classdesc Class representing a Connector CLI (command line interface client)
 */
class ConnectorCli {
  /**
   * Create a ConnectorCli
   * @param {Object} config
   * @param {string} config.classPath - The path to the Jar files to be included
   * @param {string} config.mainClass - The name of the Java class that represents the main program
   * @param {string} config.props The - path to the properties file (for example, language specific)
   */
  constructor({
    classPath = config.classPath,
    mainClass = config.mainClass,
    props = config.props
  } = {}) {
    this._classPath = classPath;
    this._mainClass = mainClass;
    this._props = props;
    this._tmpFile = _tmpFile2.default;
    this._exec = _childProcessPromise.exec;
    this._loadJSONFile = _loadJsonFile2.default;
  }

  /**
   * @returns {Promise<Object>}
   */
  get({
    annotators,
    text
    // TODO options,
    // TODO language
  }) {
    const params = ['-cp', `'${this._classPath}'`, `${this._mainClass}`, '-props', `${this._props}`, '-annotators', `${annotators.join()}`, '-outputFormat', 'json'];

    return this._tmpFile(text).then(file => this._exec(`java ${params.concat([`-file ${file.path}`]).join(' ')}`).then(result => {
      const stdout = result.stdout || result.stderr;
      const outfile = stdout.match(/writing to (.*\.json)/)[1];
      return this._loadJSONFile(outfile);
    }));
  }
}

exports.default = ConnectorCli;

},{"child-process-promise":1,"load-json-file":1,"tmp-file":1}],8:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _requestPromiseNative = require('request-promise-native');

var _requestPromiseNative2 = _interopRequireDefault(_requestPromiseNative);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

const config = {
  dsn: 'http://localhost:9000'
};

/**
 * @class
 * @classdesc Class representing a Connector Server (web server client)
 */
class ConnectorServer {
  /**
   * Create a ConnectorServer
   * @param {Object} config
   * @param {string} config.dsn - The StanfordCoreNLPServer dsn (example: 'http://localhost:9000')
   */
  constructor({ dsn = config.dsn } = {}) {
    this._dsn = dsn;
    this._rp = _requestPromiseNative2.default;
  }

  /**
   * @param {Object} config
   * @param {Array.<string>} config.annotators - The list of annotators that defines the pipeline
   * @param {string} config.text - The text to run the pipeline against
   * @param {Object} config.options - Additinal options (properties) for the pipeline
   * @param {string} config.language - Language full name in CamelCase (eg. Spanish)
   * @param {(''|'tokensregex'|'semgrex'|'tregex')} [utility] - Name of the utility to use
   * NOTE: most of the utilities receives properties, these should be passed via the options param
   * @returns {Promise<Object>}
   */
  get({ annotators, text, options, language, utility = '' }) {
    const properties = _extends({
      annotators: annotators.join()
    }, options, {
      outputFormat: 'json'
    });

    let baseUrl = this._dsn;
    let queryString = `pipelineLanguage=${language}&properties=${JSON.stringify(properties)}`;

    /**
     * @description
     * The conenctor should support extensibility to special tools:
     * - For example, Semgrex is an utility that runs in a separate url Hanlder
     *   in StanfordCoreNLPServer
     *   This url is /semgrex, and apart of the normal options, it expects the
     *   query-string `pattern` as a must.  This `pattern` option is taken here from
     *   the options object, form the key `semgrex.pattern`.
     */
    if (utility) {
      // https://stanfordnlp.github.io/CoreNLP/corenlp-server.html#query-tokensregex-tokensregex
      baseUrl += `/${utility}`;
      queryString += `&${Object.keys(options).filter(opt => opt.indexOf(`${utility}.`) === 0).map(opt => `${opt.replace(`${utility}.`, '')}=${encodeURI(options[opt])}`).join('&')}`;
    }

    return this._makeRequest(baseUrl, queryString, text);
  }

  // eslint-disable-next-line class-methods-use-this
  _makeRequest(baseUrl, queryString, text) {
    const rpOpts = {
      method: 'POST',
      uri: `${baseUrl}?${queryString}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      body: text,
      json: true
    };

    return this._rp(rpOpts);
  }
}

exports.default = ConnectorServer;

},{"request-promise-native":11}],9:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ConnectorServer = exports.ConnectorCli = exports.Service = exports.Pipeline = exports.Properties = undefined;

var _document = require('./simple/document');

var _document2 = _interopRequireDefault(_document);

var _sentence = require('./simple/sentence');

var _sentence2 = _interopRequireDefault(_sentence);

var _token = require('./simple/token');

var _token2 = _interopRequireDefault(_token);

var _expression = require('./simple/expression');

var _expression2 = _interopRequireDefault(_expression);

var _annotable = require('./simple/annotable');

var _annotable2 = _interopRequireDefault(_annotable);

var _annotator = require('./simple/annotator');

var _annotator2 = _interopRequireDefault(_annotator);

var _tokenize = require('./simple/annotator/tokenize');

var _tokenize2 = _interopRequireDefault(_tokenize);

var _ssplit = require('./simple/annotator/ssplit');

var _ssplit2 = _interopRequireDefault(_ssplit);

var _pos = require('./simple/annotator/pos');

var _pos2 = _interopRequireDefault(_pos);

var _lemma = require('./simple/annotator/lemma');

var _lemma2 = _interopRequireDefault(_lemma);

var _ner = require('./simple/annotator/ner');

var _ner2 = _interopRequireDefault(_ner);

var _parse = require('./simple/annotator/parse');

var _parse2 = _interopRequireDefault(_parse);

var _depparse = require('./simple/annotator/depparse');

var _depparse2 = _interopRequireDefault(_depparse);

var _relation = require('./simple/annotator/relation');

var _relation2 = _interopRequireDefault(_relation);

var _regexner = require('./simple/annotator/regexner');

var _regexner2 = _interopRequireDefault(_regexner);

var _coref = require('./simple/annotator/coref');

var _coref2 = _interopRequireDefault(_coref);

var _tree = require('./util/tree');

var _tree2 = _interopRequireDefault(_tree);

var _properties = require('./properties');

var _properties2 = _interopRequireDefault(_properties);

var _pipeline = require('./pipeline');

var _pipeline2 = _interopRequireDefault(_pipeline);

var _service = require('./service');

var _service2 = _interopRequireDefault(_service);

var _connectorCli = require('./connector/connector-cli');

var _connectorCli2 = _interopRequireDefault(_connectorCli);

var _connectorServer = require('./connector/connector-server');

var _connectorServer2 = _interopRequireDefault(_connectorServer);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

const Properties = exports.Properties = _properties2.default;
const Pipeline = exports.Pipeline = _pipeline2.default;
const Service = exports.Service = _service2.default;
const ConnectorCli = exports.ConnectorCli = _connectorCli2.default;
const ConnectorServer = exports.ConnectorServer = _connectorServer2.default;

/**
 * CoreNLP NodeJS Interface
 * @namespace CoreNLP
 */
exports.default = {
  /**
   * @namespace CoreNLP/simple
   * @description NodeJS API that emulates {@link https://stanfordnlp.github.io/CoreNLP/simple.html}
   */
  simple: {
    Annotable: _annotable2.default,
    Annotator: _annotator2.default,
    Document: _document2.default,
    Sentence: _sentence2.default,
    Token: _token2.default,
    Expression: _expression2.default,
    /**
     * @namespace CoreNLP/simple/annotator
     * @description Predefined annotators {@link https://stanfordnlp.github.io/CoreNLP/annotators.html}
     */
    annotator: {
      TokenizerAnnotator: _tokenize2.default,
      WordsToSentenceAnnotator: _ssplit2.default,
      POSTaggerAnnotator: _pos2.default,
      MorphaAnnotator: _lemma2.default,
      NERClassifierCombiner: _ner2.default,
      ParserAnnotator: _parse2.default,
      DependencyParseAnnotator: _depparse2.default,
      RelationExtractorAnnotator: _relation2.default,
      RegexNERAnnotator: _regexner2.default,
      CorefAnnotator: _coref2.default
    }
  },
  /**
   * @namespace CoreNLP/util
   * @description Utilities
   */
  util: {
    Tree: _tree2.default
  }
};

},{"./connector/connector-cli":7,"./connector/connector-server":8,"./pipeline":10,"./properties":12,"./service":13,"./simple/annotable":14,"./simple/annotator":15,"./simple/annotator/coref":16,"./simple/annotator/depparse":17,"./simple/annotator/lemma":19,"./simple/annotator/ner":20,"./simple/annotator/parse":21,"./simple/annotator/pos":24,"./simple/annotator/regexner":27,"./simple/annotator/relation":28,"./simple/annotator/ssplit":29,"./simple/annotator/tokenize":30,"./simple/document":33,"./simple/expression":36,"./simple/sentence":38,"./simple/token":39,"./util/tree":40}],10:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _lodash = require('lodash.difference');

var _lodash2 = _interopRequireDefault(_lodash);

var _service = require('./service');

var _service2 = _interopRequireDefault(_service);

var _connectorServer = require('./connector/connector-server');

var _connectorServer2 = _interopRequireDefault(_connectorServer);

var _tokenize = require('./simple/annotator/tokenize');

var _tokenize2 = _interopRequireDefault(_tokenize);

var _ssplit = require('./simple/annotator/ssplit');

var _ssplit2 = _interopRequireDefault(_ssplit);

var _pos = require('./simple/annotator/pos');

var _pos2 = _interopRequireDefault(_pos);

var _lemma = require('./simple/annotator/lemma');

var _lemma2 = _interopRequireDefault(_lemma);

var _ner = require('./simple/annotator/ner');

var _ner2 = _interopRequireDefault(_ner);

var _parse = require('./simple/annotator/parse');

var _parse2 = _interopRequireDefault(_parse);

var _depparse = require('./simple/annotator/depparse');

var _depparse2 = _interopRequireDefault(_depparse);

var _relation = require('./simple/annotator/relation');

var _relation2 = _interopRequireDefault(_relation);

var _regexner = require('./simple/annotator/regexner');

var _regexner2 = _interopRequireDefault(_regexner);

var _coref = require('./simple/annotator/coref');

var _coref2 = _interopRequireDefault(_coref);

var _document = require('./simple/document');

var _document2 = _interopRequireDefault(_document);

var _expression = require('./simple/expression');

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _asyncToGenerator(fn) {
  return function () {
    var gen = fn.apply(this, arguments);return new Promise(function (resolve, reject) {
      function step(key, arg) {
        try {
          var info = gen[key](arg);var value = info.value;
        } catch (error) {
          reject(error);return;
        }if (info.done) {
          resolve(value);
        } else {
          return Promise.resolve(value).then(function (value) {
            step("next", value);
          }, function (err) {
            step("throw", err);
          });
        }
      }return step("next");
    });
  };
}

const ANNOTATORS_BY_KEY = {
  tokenize: _tokenize2.default,
  ssplit: _ssplit2.default,
  pos: _pos2.default,
  lemma: _lemma2.default,
  ner: _ner2.default,
  parse: _parse2.default,
  depparse: _depparse2.default,
  relation: _relation2.default,
  regexner: _regexner2.default,
  coref: _coref2.default
};

const LANGUAGE_TO_ISO2 = {
  English: 'en',
  French: 'fr',
  German: 'de',
  Spanish: 'es',
  Chinese: 'zh',
  Arabic: 'ar'
};

/**
 * @class
 * @classdesc Class representing a Pipeline.
 */
class Pipeline {
  /**
   * Create a Pipeline
   * @param {Properties} properties
   * @param {string} [language] - in CamelCase (i.e. English, Spanish)
   * @param {ConnectorServer|ConnectorCli} [connector]
   */
  constructor(properties, language = 'Unspecified', connector = null) {
    this._properties = properties;
    this._language = language;
    this._connector = connector || new _connectorServer2.default({});
    this._service = new _service2.default(this._connector, this._language);
  }

  /**
   * Retrieves the current Service used by the pipeline
   * @param {Service} service
   */
  getService() {
    return this._service;
  }

  /**
   * Execute the pipeline against the annotable object, adding annotations to it.
   * Calls the service and loads the associated response metadata into the Annotable model
   * @async
   * @param {Annotable} annotable - the document or sentence to be annotated
   * @returns {Promise<Annotable>} annotated document / sentence
   */
  annotate(annotable) {
    var _this = this;

    return _asyncToGenerator(function* () {
      annotable.fromJSON((yield _this._service.getAnnotationData(annotable.text(), _this._getAnnotatorsKeys(), _this._getAnnotatrosOptions())));

      annotable.setLanguageISO(LANGUAGE_TO_ISO2[_this._language]);
      annotable.addAnnotators(_this._getAnnotators());

      return annotable;
    })();
  }

  /**
   * @param {Array.<Annotator>} requiredAnnotators
   */
  assert(methodName = '', requiredAnnotators = []) {
    if ((0, _lodash2.default)(requiredAnnotators.map(Annotator => new Annotator().toString()), this._getAnnotatorsKeys()).length > 0) {
      throw new Error(`Assert: ${methodName} requires ${requiredAnnotators.join()} within the annotators list.`);
    }
  }

  /**
   * Annotates the given Expression instance with matching groups and/or Tokens
   * @param {Expression} expression - An annotable expression containing a TokensRegex pattern
   * @param {boolean} [annotateExpression] - Whether to hydrate the annotations with tokens or not.
   * IMPORTANT: The optional parameter `annotateExpression` if true, will run the CoreNLP pipeline
   *            twice.  First for the TokensRegex annotation, and one more for the standard pipeline
   *            Token annotations (pos, ner, lemma, etc).
   * @returns {Expression} expression - The current expression instance
   */
  annotateTokensRegex(annotable, annotateExpression = false) {
    var _this2 = this;

    return _asyncToGenerator(function* () {
      _this2.assert('TokensRegex', [_tokenize2.default, _ssplit2.default]);
      annotable.fromJSON((yield _this2._service.getTokensRegexData(annotable.text(), annotable.pattern(), _this2._getAnnotatorsKeys(), _this2._getAnnotatrosOptions())));

      annotable.setLanguageISO(LANGUAGE_TO_ISO2[_this2._language]);
      annotable.addAnnotator(_expression.TokensRegexAnnotator);

      if (annotateExpression) {
        return _this2._annotateExpression(annotable);
      }
      return annotable;
    })();
  }

  /**
   * Annotates the given Expression instance with matching groups and/or Tokens
   * @param {Expression} expression - An annotable expression containing a Semgrex pattern
   * @param {boolean} [annotateExpression] - Whether to hydrate the annotations with tokens or not.
   * IMPORTANT: The optional parameter `annotateExpression` if true, will run the CoreNLP pipeline
   *            twice.  First for the Semgrex annotation, and one more for the standard pipeline
   *            Token annotations (pos, ner, lemma, etc).
   * @returns {Expression} expression - The current expression instance
   */
  annotateSemgrex(annotable, annotateExpression = false) {
    var _this3 = this;

    return _asyncToGenerator(function* () {
      _this3.assert('Semgrex', [_tokenize2.default, _ssplit2.default, _depparse2.default]);
      annotable.fromJSON((yield _this3._service.getSemgrexData(annotable.text(), annotable.pattern(), _this3._getAnnotatorsKeys(), _this3._getAnnotatrosOptions())));

      annotable.setLanguageISO(LANGUAGE_TO_ISO2[_this3._language]);
      annotable.addAnnotator(_expression.SemgrexAnnotator);

      if (annotateExpression) {
        return _this3._annotateExpression(annotable);
      }
      return annotable;
    })();
  }

  /**
   * Annotates the given Expression instance with matching groups and/or Tokens
   * @param {Expression} expression - An annotable expression containing a Tregex pattern
   * @param {boolean} [annotateExpression] - Whether to hydrate the annotations with tokens or not.
   * IMPORTANT: The optional parameter `annotateExpression` if true, will run the CoreNLP pipeline
   *            twice.  First for the Tregex annotation, and one more for the standard pipeline
   *            Token annotations (pos, ner, lemma, etc).
   * @returns {Expression} expression - The current expression instance
   */
  annotateTregex(annotable, annotateExpression = false) {
    var _this4 = this;

    return _asyncToGenerator(function* () {
      _this4.assert('Tregex', [_tokenize2.default, _ssplit2.default, _parse2.default]);
      annotable.fromJSON((yield _this4._service.getTregexData(annotable.text(), annotable.pattern(), _this4._getAnnotatorsKeys(), _this4._getAnnotatrosOptions())));

      annotable.setLanguageISO(LANGUAGE_TO_ISO2[_this4._language]);
      annotable.addAnnotator(_expression.TregexAnnotator);

      if (annotateExpression) {
        return _this4._annotateExpression(annotable);
      }
      return annotable;
    })();
  }

  /**
   * @private
   * @description
   * Runs the default pipeline over the same text of the expression, and merges the results
   */
  _annotateExpression(annotableExpression) {
    var _this5 = this;

    return _asyncToGenerator(function* () {
      const doc = yield _this5.annotate(new _document2.default(annotableExpression.text()));
      doc.setLanguageISO(LANGUAGE_TO_ISO2[_this5._language]);
      annotableExpression.mergeTokensFromDocument(doc);
      return annotableExpression;
    })();
  }

  /**
   * @private
   */
  _semgrex(text, pattern) {
    var _this6 = this;

    return _asyncToGenerator(function* () {
      const data = yield _this6._service.getSemgrexData(text, pattern, _this6._getAnnotatorsKeys(), _this6._getAnnotatrosOptions());

      return data;
    })();
  }

  /**
   * @private
   * @returns {Aray.<string>} annotators - those set for this pipeline
   */
  _getAnnotatorsKeys() {
    return this._properties.getProperty('annotators', '').split(',').map(annotatorKey => annotatorKey.trim());
  }

  /**
   * @private
   * @returns {Aray.<Annotator>} annotators - those set for this pipeline
   */
  _getAnnotators() {
    return this._getAnnotatorsKeys().map(annotatorKey => ANNOTATORS_BY_KEY[annotatorKey]);
  }

  /**
   * Only given options are those related to the annotators in the pipeline
   * @private
   * @returns {Aray.<Annotator>} annotators - those set for this pipeline
   */
  _getAnnotatrosOptions() {
    const pipelineProps = this._properties.getProperties();
    const validPrfixes = Object.keys(ANNOTATORS_BY_KEY);
    return Object.keys(pipelineProps).filter(propName => validPrfixes.indexOf(propName) === 0).reduce((acc, val, key) => _extends({}, acc, { [key]: val }), {});
  }
}

exports.default = Pipeline;

},{"./connector/connector-server":8,"./service":13,"./simple/annotator/coref":16,"./simple/annotator/depparse":17,"./simple/annotator/lemma":19,"./simple/annotator/ner":20,"./simple/annotator/parse":21,"./simple/annotator/pos":24,"./simple/annotator/regexner":27,"./simple/annotator/relation":28,"./simple/annotator/ssplit":29,"./simple/annotator/tokenize":30,"./simple/document":33,"./simple/expression":36,"lodash.difference":2}],11:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

// eslint-disable-next-line no-undef
exports.default = req => fetch(encodeURI(req.uri), _extends({}, req, {
  withCredentials: true
})).then(response => response.json());

},{}],12:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

/**
 * @class
 * @classdesc Class representing a Properties set.
 */
class Properties {
  /**
   * Create an Properties
   * @param {Object} props
   */
  constructor(props) {
    this._props = _extends({}, props);
  }

  /**
   * Property setter
   * @param {string} name - the property name
   * @param {*} value - the property value
   */
  setProperty(name, value) {
    this._props[name] = value;
  }

  /**
   * Property getter
   * @param {string} name - the property name
   * @param {*} default - the defaut value to return if not set
   * @returns {*} value - the property value
   */
  getProperty(name, defaultValue = undefined) {
    if (typeof this._props[name] !== 'undefined') {
      return this._props[name];
    }
    return defaultValue;
  }

  /**
   * Returns an Object map of the given properties
   * @returns {Object} properties - the properties object
   */
  getProperties() {
    return _extends({}, this._props);
  }

  /**
   * Returns a JSON object of the given properties
   * @returns {Object} json - the properties object
   */
  toJSON() {
    return _extends({}, this._props);
  }

  /**
   * Returns a properties file-like string of the given properties
   * @returns {string} properties - the properties content
   */
  toPropertiessFileContent() {
    return Object.keys(this._props).map(propName => `${propName} = ${this._props[propName]}`).join('\n');
  }
}

exports.default = Properties;

},{}],13:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _dependencies = require('./simple/annotator/depparse/dependencies.json');

var _dependencies2 = _interopRequireDefault(_dependencies);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

// preload for browserify
require('./simple/annotator/pos/en.json');
require('./simple/annotator/pos/es.json');
require('./simple/annotator/parse/en.json');
require('./simple/annotator/parse/es.json');

/**
 * @class
 * @classdesc Middleware that interfaces between the pipeline and the connector strategies
 */
class Service {
  /**
   * Create a Service
   * @param {ConnectorServer|ConnectorCli} connector
   * @param {('English'|'French'|'German'|'Spanish'|'Unspecified'|'Whitesapce')} [language]
   */
  constructor(connector, language = 'Unspecified') {
    this._connector = connector;
    this._language = language;
  }

  getAnnotationData(text, annotators, options = {}) {
    return this._connector.get({
      annotators,
      text,
      options,
      language: this._language.toLowerCase()
    });
  }

  getTokensRegexData(text, pattern, annotators, options = {}) {
    return this._connector.get({
      annotators,
      text,
      options: _extends({}, options, {
        'tokensregex.pattern': pattern
      }),
      language: this._language.toLowerCase(),
      utility: 'tokensregex'
    });
  }

  getSemgrexData(text, pattern, annotators, options = {}) {
    return this._connector.get({
      annotators,
      text,
      options: _extends({}, options, {
        'semgrex.pattern': pattern
      }),
      language: this._language.toLowerCase(),
      utility: 'semgrex'
    });
  }

  getTregexData(text, pattern, annotators, options = {}) {
    return this._connector.get({
      annotators,
      text,
      options: _extends({}, options, {
        'tregex.pattern': pattern
      }),
      language: this._language.toLowerCase(),
      utility: 'tregex'
    });
  }

  static getTokenPosInfo(pos, languageISO) {
    try {
      // eslint-disable-next-line global-require, import/no-dynamic-require
      return require(`./simple/annotator/pos/${languageISO}.json`).tagset[pos];
    } catch (err) {
      return undefined;
    }
  }

  static getSentenceParseInfo(group, languageISO) {
    try {
      // eslint-disable-next-line global-require, import/no-dynamic-require
      return require(`./simple/annotator/parse/${languageISO}.json`).multiword[group];
    } catch (err) {
      return undefined;
    }
  }

  static getGovernorDepInfo(dep) {
    return _dependencies2.default.dependencies[dep];
  }
}

exports.default = Service;

},{"./simple/annotator/depparse/dependencies.json":18,"./simple/annotator/parse/en.json":22,"./simple/annotator/parse/es.json":23,"./simple/annotator/pos/en.json":25,"./simple/annotator/pos/es.json":26}],14:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * @class
 * @classdesc Class representing an Annotable
 * @memberof CoreNLP/simple
 */
class Annotable {
  /**
   * Create an Annotable
   * @param {string} text
   */
  constructor(text) {
    this._text = text;
    this._annotators = {};
  }

  /**
   * Get a string representation of the raw text
   * @return {string} text
   */
  text() {
    return this._text;
  }

  /**
   * Sets the language ISO (given by the pipeline during the annotation process)
   * This is solely to keep track of the language chosen for further analysis
   * The language string should be passed in lowercase ISO2 format
   * @return {string} text
   */
  setLanguageISO(iso) {
    this._language = iso;
  }

  /**
   * Retrieves the language ISO (in lowercase ISO2 format)
   * @return {string} iso
   */
  getLanguageISO() {
    return this._language;
  }

  /**
   * Marks an annotator as a met dependency
   * @param {Annotator|function} annotator
   */
  addAnnotator(annotator) {
    this._annotators[annotator.toString()] = annotator;
  }

  /**
   * Marks multiple annotators as a met dependencies
   * @param {Array.<Annotator|function>} annotators
   */
  addAnnotators(annotators) {
    annotators.forEach(annotator => this.addAnnotator(annotator));
  }

  /**
   * Unmarks an annotator as a met dependency
   * @param {Annotator|function} annotator
   */
  removeAnnotator(annotator) {
    delete this._annotators[annotator.toString()];
  }

  /**
   * Tells you if an annotator is a met dependency
   * @param {Annotator|function} annotator
   * @returns {boolean} hasAnnotator
   */
  hasAnnotator(annotator) {
    return !!this._annotators[annotator.toString()];
  }

  /**
   * Tells you if at least on of a list of annotators is a met dependency
   * @param {Array.<Annotator|function>} annotators
   * @returns {boolean} hasAnyAnnotator
   */
  hasAnyAnnotator(annotators) {
    return annotators.some(annotator =>
    // eslint-disable-next-line no-bitwise
    !!~Object.keys(this._annotators).indexOf(annotator.toString()));
  }
}

exports.default = Annotable;

},{}],15:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _lodash = require('lodash.uniq');

var _lodash2 = _interopRequireDefault(_lodash);

var _lodash3 = require('lodash.flatten');

var _lodash4 = _interopRequireDefault(_lodash3);

var _lodash5 = require('lodash.reduce');

var _lodash6 = _interopRequireDefault(_lodash5);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @class
 * @classdesc Class representing an Annotatror
 * @extends Annotator
 * @memberof CoreNLP/simple
 */
class Annotator {
  /**
   * Create an Annotator
   * @param {string} name
   * @param {Object} [options] a key-value map of options, without the annotator prefix
   * @param {Array.<Annotator>} [dependencies]
   */
  constructor(name, options = {}, dependencies = []) {
    this._name = name;
    this._options = options;
    this._dependencies = dependencies;
  }

  /**
   * Get a string representation
   * @return {string} annotator
   */
  toString() {
    return this._name;
  }

  /**
   * Defines whether a given annotator is the same as current, using shallow compare.
   * This is useful for a Document or Sentence to validate if the minimum of annotators required
   * were already applied to them.  Allows at the same time the users to instantiate new annotators
   * and configure them as needed.
   * @param {Annotator} annotator
   * @return {boolean}
   */
  equalsTo(annotator) {
    return this._name === annotator.toString();
  }

  /**
   * Get an Object key-value representation of the annotor's options (excluding prefix)
   * @return {Object} options
   */
  options() {
    return this._options;
  }

  /**
   * Get/Set an option value
   * @param {string} key
   * @param {string|boolean} [value]
   * @return {string} value
   */
  option(key, value = null) {
    if (value === null) {
      return this._options[key];
    }
    this._options[key] = value;
    return value;
  }

  /**
   * Get a list of annotators dependencies
   * @return {Array.<Annotator>} dependencies
   */
  dependencies() {
    return this._dependencies;
  }

  /**
   * Get a list of annotators dependencies, following by this annotator, all this as
   * a list of strings
   * This is useful to fulfill the `annotators` param in CoreNLP API properties.
   * @return {Array.<string>} pipeline
   */
  pipeline() {
    return (0, _lodash2.default)((0, _lodash4.default)(this.dependencies().map(annotator => annotator.pipeline())).concat([this.toString()]));
  }

  /**
   * Get an object of all the Annotator options including the current and all its
   * dependencies, prefixed by the annotator names
   * This is useful to fulfill the options params in CoreNLP API properties.
   * @return {Array.<string>} pipelineOptions
   */
  pipelineOptions() {
    return (0, _lodash6.default)(this.dependencies().map(annotator => annotator.pipelineOptions()).concat(Object.keys(this.options()).map(opt => ({ [`${this}.${opt}`]: this.option(opt) }))), (ac, option) => _extends({}, ac, option), {});
  }
}

exports.default = Annotator;

},{"lodash.flatten":3,"lodash.reduce":5,"lodash.uniq":6}],16:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _annotator = require('../annotator');

var _annotator2 = _interopRequireDefault(_annotator);

var _tokenize = require('./tokenize');

var _tokenize2 = _interopRequireDefault(_tokenize);

var _ssplit = require('./ssplit');

var _ssplit2 = _interopRequireDefault(_ssplit);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @class
 * @classdesc Class representing an CorefAnnotator.
 * @extends Annotator
 * @memberof CoreNLP/simple/annotator
 * @requires tokenize, ssplit, coref
 * @see {@link https://stanfordnlp.github.io/CoreNLP/coref.html|CorefAnnotator}
 */
class CorefAnnotator extends _annotator2.default {
  /**
   * Create an Annotator
   * @param {Object} [options] a key-value map of options, without the annotator prefix
   */
  constructor(options = {}) {
    super('coref', _extends({}, options), [new _tokenize2.default(), new _ssplit2.default()]);
  }
}

exports.default = CorefAnnotator;

},{"../annotator":15,"./ssplit":29,"./tokenize":30}],17:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _annotator = require('../annotator');

var _annotator2 = _interopRequireDefault(_annotator);

var _tokenize = require('./tokenize');

var _tokenize2 = _interopRequireDefault(_tokenize);

var _ssplit = require('./ssplit');

var _ssplit2 = _interopRequireDefault(_ssplit);

var _pos = require('./pos');

var _pos2 = _interopRequireDefault(_pos);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @class
 * @classdesc Class representing an DependencyParseAnnotator. Hydrates {@link Sentence.governors()}
 * @extends Annotator
 * @memberof CoreNLP/simple/annotator
 * @requires tokenize, ssplit, pos, lemma, ner, parse, depparse
 * @see {@link https://stanfordnlp.github.io/CoreNLP/depparse.html|DependencyParseAnnotator}
 */
class DependencyParseAnnotator extends _annotator2.default {
  /**
   * Create an Annotator
   * @param {Object} [options] a key-value map of options, without the annotator prefix
   */
  constructor(options = {}) {
    super('depparse', _extends({}, options), [new _tokenize2.default(), new _ssplit2.default(), new _pos2.default()]);
  }
}

exports.default = DependencyParseAnnotator;

},{"../annotator":15,"./pos":24,"./ssplit":29,"./tokenize":30}],18:[function(require,module,exports){
module.exports={
  "source": "http://universaldependencies.org/u/dep/index.html",
  "dependencies": {
    "acl": {
      "type": "clausal modifier of noun (adjectival clause)",
      "description": "",
      "examples": [
      ]
    },
    "advcl": {
      "type": "adverbial clause modifier",
      "description": "",
      "examples": [
      ]
    },
    "advmod": {
      "type": "adverbial modifier",
      "description": "",
      "examples": [
      ]
    },
    "amod": {
      "type": "adjectival modifier",
      "description": "",
      "examples": [
      ]
    },
    "appos": {
      "type": "appositional modifier",
      "description": "",
      "examples": [
      ]
    },
    "aux": {
      "type": "auxiliary",
      "description": "",
      "examples": [
      ]
    },
    "case": {
      "type": "case marking",
      "description": "",
      "examples": [
      ]
    },
    "cc": {
      "type": "coordinating conjunction",
      "description": "",
      "examples": [
      ]
    },
    "ccomp": {
      "type": "clausal complement",
      "description": "",
      "examples": [
      ]
    },
    "clf": {
      "type": "classifier",
      "description": "",
      "examples": [
      ]
    },
    "compound": {
      "type": "compound",
      "description": "",
      "examples": [
      ]
    },
    "conj": {
      "type": "conjunct",
      "description": "",
      "examples": [
      ]
    },
    "cop": {
      "type": "copula",
      "description": "",
      "examples": [
      ]
    },
    "csubj": {
      "type": "clausal subject",
      "description": "",
      "examples": [
      ]
    },
    "dep": {
      "type": "unspecified dependency",
      "description": "",
      "examples": [
      ]
    },
    "det": {
      "type": "determiner",
      "description": "",
      "examples": [
      ]
    },
    "discourse": {
      "type": "discourse element",
      "description": "",
      "examples": [
      ]
    },
    "dislocated": {
      "type": "dislocated elements",
      "description": "",
      "examples": [
      ]
    },
    "expl": {
      "type": "expletive",
      "description": "",
      "examples": [
      ]
    },
    "fixed": {
      "type": "fixed multiword expression",
      "description": "",
      "examples": [
      ]
    },
    "flat": {
      "type": "flat multiword expression",
      "description": "",
      "examples": [
      ]
    },
    "goeswith": {
      "type": "goes with",
      "description": "",
      "examples": [
      ]
    },
    "dobj": {
      "type": "direct object",
      "description": "",
      "examples": [
      ]
    },
    "iobj": {
      "type": "indirect object",
      "description": "",
      "examples": [
      ]
    },
    "list": {
      "type": "list",
      "description": "",
      "examples": [
      ]
    },
    "mark": {
      "type": "marker",
      "description": "",
      "examples": [
      ]
    },
    "nmod": {
      "type": "nominal modifier",
      "description": "",
      "examples": [
      ]
    },
    "nsubj": {
      "type": "nominal subject",
      "description": "",
      "examples": [
      ]
    },
    "nummod": {
      "type": "numeric modifier",
      "description": "",
      "examples": [
      ]
    },
    "obj": {
      "type": "object",
      "description": "",
      "examples": [
      ]
    },
    "obl": {
      "type": "oblique nominal",
      "description": "",
      "examples": [
      ]
    },
    "orphan": {
      "type": "orphan",
      "description": "",
      "examples": [
      ]
    },
    "parataxis": {
      "type": "parataxis",
      "description": "",
      "examples": [
      ]
    },
    "punct": {
      "type": "punctuation",
      "description": "",
      "examples": [
      ]
    },
    "reparandum": {
      "type": "overridden disfluency",
      "description": "",
      "examples": [
      ]
    },
    "root": {
      "type": "root",
      "description": "",
      "examples": [
      ]
    },
    "vocative": {
      "type": "vocative",
      "description": "",
      "examples": [
      ]
    },
    "xcomp": {
      "type": "open clausal complement",
      "description": "",
      "examples": [
      ]
    }
  }
}

},{}],19:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _annotator = require('../annotator');

var _annotator2 = _interopRequireDefault(_annotator);

var _tokenize = require('./tokenize');

var _tokenize2 = _interopRequireDefault(_tokenize);

var _ssplit = require('./ssplit');

var _ssplit2 = _interopRequireDefault(_ssplit);

var _pos = require('./pos');

var _pos2 = _interopRequireDefault(_pos);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @class
 * @classdesc Class representing an MorphaAnnotator. Hydrates {@link Token.lemma()}
 * @extends Annotator
 * @memberof CoreNLP/simple/annotator
 * @requires tokenize, ssplit, pos, lemma
 * @see {@link https://stanfordnlp.github.io/CoreNLP/lemma.html|MorphaAnnotator}
 */
class MorphaAnnotator extends _annotator2.default {
  /**
   * Create an Annotator
   * @param {Object} [options] a key-value map of options, without the annotator prefix
   */
  constructor(options = {}) {
    super('lemma', _extends({}, options), [new _tokenize2.default(), new _ssplit2.default(), new _pos2.default()]);
  }
}

exports.default = MorphaAnnotator;

},{"../annotator":15,"./pos":24,"./ssplit":29,"./tokenize":30}],20:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _annotator = require('../annotator');

var _annotator2 = _interopRequireDefault(_annotator);

var _tokenize = require('./tokenize');

var _tokenize2 = _interopRequireDefault(_tokenize);

var _ssplit = require('./ssplit');

var _ssplit2 = _interopRequireDefault(_ssplit);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @class
 * @classdesc Class representing an NERClassifierCombiner. Hydrates {@link Token.ner()}
 * @extends Annotator
 * @memberof CoreNLP/simple/annotator
 * @requires tokenize, ssplit, pos, lemma, ner
 * @see {@link https://stanfordnlp.github.io/CoreNLP/ner.html|NERClassifierCombiner}
 */
class NERClassifierCombiner extends _annotator2.default {
  /**
   * Create an Annotator
   * @param {Object} [options] a key-value map of options, without the annotator prefix
   */
  constructor(options = {}) {
    super('ner', _extends({
      useSUTime: true,
      // model: null, // throws error on CoreNLP server
      applyNumericClassifiers: true
    }, options), [new _tokenize2.default(), new _ssplit2.default()]);
  }
}

exports.default = NERClassifierCombiner;

},{"../annotator":15,"./ssplit":29,"./tokenize":30}],21:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _annotator = require('../annotator');

var _annotator2 = _interopRequireDefault(_annotator);

var _tokenize = require('./tokenize');

var _tokenize2 = _interopRequireDefault(_tokenize);

var _ssplit = require('./ssplit');

var _ssplit2 = _interopRequireDefault(_ssplit);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @class
 * @class Class representing an ParserAnnotator. Hydrates {@link Token.parse()}
 * @extends Annotator
 * @memberof CoreNLP/simple/annotator
 * @requires tokenize, ssplit, pos, lemma, ner, parse
 * @see {@link https://stanfordnlp.github.io/CoreNLP/parse.html|ParserAnnotator}
 */
class ParserAnnotator extends _annotator2.default {
  /**
   * Create an Annotator
   * @param {Object} [options] a key-value map of options, without the annotator prefix
   */
  constructor(options = {}) {
    super('parse', _extends({}, options), [new _tokenize2.default(), new _ssplit2.default()]);
  }
}

exports.default = ParserAnnotator;

},{"../annotator":15,"./ssplit":29,"./tokenize":30}],22:[function(require,module,exports){
module.exports={
  "source": "http://web.mit.edu/6.863/www/PennTreebankTags.html",
  "multiword": {
    "S": {
      "description": "simple declarative clause, i.e. one that is not introduced by a (possible empty) subordinating conjunction or a wh-word and that does not exhibit subject-verb inversion.",
      "examples": []
    },
    "SBAR": {
      "description": "Clause introduced by a (possibly empty) subordinating conjunction.",
      "examples": []
    },
    "SBARQ": {
      "description": "Direct question introduced by a wh-word or a wh-phrase. Indirect questions and relative clauses should be bracketed as SBAR, not SBARQ.",
      "examples": []
    },
    "SINV": {
      "description": "Inverted declarative sentence, i.e. one in which the subject follows the tensed verb or modal.",
      "examples": []
    },
    "SQ": {
      "description": "Inverted yes/no question, or main clause of a wh-question, following the wh-phrase in SBARQ.",
      "examples": []
    },
    "ADJP": {
      "description": "Adjective Phrase.",
      "examples": []
    },
    "ADVP": {
      "description": "Adverb Phrase.",
      "examples": []
    },
    "CONJP": {
      "description": "Conjunction Phrase.",
      "examples": []
    },
    "FRAG": {
      "description": "Fragment.",
      "examples": []
    },
    "INTJ": {
      "description": "Interjection. Corresponds approximately to the part-of-speech tag UH.",
      "examples": []
    },
    "LST": {
      "description": "List marker. Includes surrounding punctuation.",
      "examples": []
    },
    "NAC": {
      "description": "Not a Constituent; used to show the scope of certain prenominal modifiers within an NP.",
      "examples": []
    },
    "NP": {
      "description": "Noun Phrase. ",
      "examples": []
    },
    "NX": {
      "description": "Used within certain complex NPs to mark the head of the NP. Corresponds very roughly to N-bar level but used quite differently.",
      "examples": []
    },
    "PP": {
      "description": "Prepositional Phrase.",
      "examples": []
    },
    "PRN": {
      "description": "Parenthetical. ",
      "examples": []
    },
    "PRT": {
      "description": "Particle. Category for words that should be tagged RP. ",
      "examples": []
    },
    "QP": {
      "description": "Quantifier Phrase (i.e. complex measure/amount phrase); used within NP.",
      "examples": []
    },
    "RRC": {
      "description": "Reduced Relative Clause. ",
      "examples": []
    },
    "UCP": {
      "description": "Unlike Coordinated Phrase. ",
      "examples": []
    },
    "VP": {
      "description": "Vereb Phrase. ",
      "examples": []
    },
    "WHADJP": {
      "description": "Wh-adjective Phrase. Adjectival phrase containing a wh-adverb, as in how hot.",
      "examples": []
    },
    "WHAVP": {
      "description": "Wh-adverb Phrase. Introduces a clause with an NP gap. May be null (containing the 0 complementizer) or lexical, containing a wh-adverb such as how or why.",
      "examples": []
    },
    "WHNP": {
      "description": "Wh-noun Phrase. Introduces a clause with an NP gap. May be null (containing the 0 complementizer) or lexical, containing some wh-word, e.g. who, which book, whose daughter, none of which, or how many leopards.",
      "examples": []
    },
    "WHPP": {
      "description": "Wh-prepositional Phrase. Prepositional phrase containing a wh-noun phrase (such as of which or by whose authority) that either introduces a PP gap or is contained by a WHNP.",
      "examples": []
    },
    "X": {
      "description": "Unknown, uncertain, or unbracketable. X is often used for bracketing typos and in bracketing the...the-constructions.",
      "examples": []
    }
  }
}



},{}],23:[function(require,module,exports){
module.exports={
  "source": "https://nlp.stanford.edu/software/spanish-faq.shtml#corpus-modifications",
  "multiword": {
    "conj": {
      "description": "Conjunction",
      "examples": ["No obstante, en el mercado …", "la fauna y la flora"]
    },
    "gerundi": {
      "description": "Gerund",
      "examples": ["responder atacando"]
    },
    "grup.a": {
      "description": "Adjective group",
      "examples": [""]
    },
    "grup.adv": {
      "description": "Adverb group",
      "examples": [""]
    },
    "grup.cc *": {
      "description": "Coordinating conjunction group",
      "examples": ["sino que lo pongo"]
    },
    "grup.cs *": {
      "description": "Subordinating conjunction group",
      "examples": ["mientras que el Ibex …"]
    },
    "grup.nom": {
      "description": "Noun group",
      "examples": ["un foro en Internet recogerá …", "permitieron a los mercados bursátiles europeos dar …"]
    },
    "grup.prep *": {
      "description": "Preposition group",
      "examples": ["a partir de hoy", "están por encima de los demás"]
    },
    "grup.pron *": {
      "description": "Pronoun group",
      "examples": ["El mío está …"]
    },
    "grup.verb": {
      "description": "Verb group",
      "examples": ["Francfort perdió el 0,05%", "las voces empiezan a subir de tono"]
    },
    "grup.w *": {
      "description": "Date group",
      "examples": ["en el pleno del pasado viernes", "desde el 19 de mayo por …"]
    },
    "grup.z *": {
      "description": "Numeral group",
      "examples": ["el 2,85 %"]
    },
    "inc": {
      "description": "Inserted element",
      "examples": ["El Gobierno , apuntó Pimentel, se …", "Testimoni silenciós (foto derecha)"]
    },
    "infinitiu": {
      "description": "Infinitive",
      "examples": ["permitiría regular actividades …", "para no empezar a regalar un tiempo"]
    },
    "interjeccio": {
      "description": "Interjection",
      "examples": ["Ojalá Aznar", "Bueno, hay una …"]
    },
    "morfema.pronominal": {
      "description": "Pronominal morpheme",
      "examples": ["él se reafirmó", "a oírse con más frecuencia"]
    },
    "morfema.verbal": {
      "description": "Verbal morpheme",
      "examples": ["se habla español"]
    },
    "neg": {
      "description": "Negation",
      "examples": ["no sabía exactamente"]
    },
    "participi": {
      "description": "Participle",
      "examples": ["espléndidamente decorada"]
    },
    "prep": {
      "description": "Preposition",
      "examples": ["la sostuvo con cuidado"]
    },
    "relatiu": {
      "description": "Relative pronoun (complementizer)",
      "examples": ["lo que iba a hacer"]
    },
    "ROOT": {
      "description": "Sentence root",
      "examples": [""]
    },
    "S": {
      "description": "Clause",
      "examples": ["lo que iba a hacer"]
    },
    "s.a": {
      "description": "Adjective phrase",
      "examples": ["un hermoso trabajo"]
    },
    "sadv": {
      "description": "Adverbial phrase",
      "examples": ["Al2 final se realizó … se comprometió anoche"]
    },
    "sentence": {
      "description": "Main sentence",
      "examples": [""]
    },
    "sn": {
      "description": "Noun phrase",
      "examples": ["En este sentido …", "por lo que aseguró"]
    },
    "sp": {
      "description": "Prepositional phrase",
      "examples": ["… decisión tomada por las autoridades"]
    },
    "spec": {
      "description": "Specifier",
      "examples": ["el término", "todos los objetivos"]
    }
  }
}

},{}],24:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _annotator = require('../annotator');

var _annotator2 = _interopRequireDefault(_annotator);

var _tokenize = require('./tokenize');

var _tokenize2 = _interopRequireDefault(_tokenize);

var _ssplit = require('./ssplit');

var _ssplit2 = _interopRequireDefault(_ssplit);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @class
 * @classdesc Class representing an POSTaggerAnnotator. Hydrates {@link Token.pos()}
 * @extends Annotator
 * @memberof CoreNLP/simple/annotator
 * @requires tokenize, ssplit, pos
 * @see {@link https://stanfordnlp.github.io/CoreNLP/pos.html|POSTaggerAnnotator}
 */
class POSTaggerAnnotator extends _annotator2.default {
  /**
   * Create an Annotator
   * @param {Object} [options] a key-value map of options, without the annotator prefix
   */
  constructor(options = {}) {
    super('pos', _extends({}, options), [new _tokenize2.default(), new _ssplit2.default()]);
  }
}

exports.default = POSTaggerAnnotator;

},{"../annotator":15,"./ssplit":29,"./tokenize":30}],25:[function(require,module,exports){
module.exports={
  "source": "http://web.mit.edu/6.863/www/PennTreebankTags.html",
  "tagset": {
    "CC": {
      "group": "Coordinating conjunction",
      "tag": "Coordinating conjunction",
      "examples": []
    },
    "CD": {
      "group": "Cardinal number",
      "tag": "Cardinal number",
      "examples": []
    },
    "DT": {
      "group": "Determiner",
      "tag": "Determiner",
      "examples": []
    },
    "EX": {
      "group": "Existential there",
      "tag": "Existential there",
      "examples": []
    },
    "FW": {
      "group": "Foreign word",
      "tag": "Foreign word",
      "examples": []
    },
    "IN": {
      "group": "Preposition or subordinating conjunction",
      "tag": "Preposition or subordinating conjunction",
      "examples": []
    },
    "JJ": {
      "group": "Adjective",
      "tag": "Adjective",
      "examples": []
    },
    "JJR": {
      "group": "Adjective, comparative",
      "tag": "Adjective, comparative",
      "examples": []
    },
    "JJS": {
      "group": "Adjective, superlative",
      "tag": "Adjective, superlative",
      "examples": []
    },
    "LS": {
      "group": "List item marker",
      "tag": "List item marker",
      "examples": []
    },
    "MD": {
      "group": "Modal",
      "tag": "Modal",
      "examples": []
    },
    "NN": {
      "group": "Noun, singular or mass",
      "tag": "Noun, singular or mass",
      "examples": []
    },
    "NNS": {
      "group": "Noun, plural",
      "tag": "Noun, plural",
      "examples": []
    },
    "NNP": {
      "group": "Proper noun, singular",
      "tag": "Proper noun, singular",
      "examples": []
    },
    "NNPS": {
      "group": "Proper noun, plural",
      "tag": "Proper noun, plural",
      "examples": []
    },
    "PDT": {
      "group": "Predeterminer",
      "tag": "Predeterminer",
      "examples": []
    },
    "POS": {
      "group": "Possessive ending",
      "tag": "Possessive ending",
      "examples": []
    },
    "PRP": {
      "group": "Personal pronoun",
      "tag": "Personal pronoun",
      "examples": []
    },
    "PRP$": {
      "group": "Possessive pronoun (prolog version PRP-S)",
      "tag": "Possessive pronoun (prolog version PRP-S)",
      "examples": []
    },
    "RB": {
      "group": "Adverb",
      "tag": "Adverb",
      "examples": []
    },
    "RBR": {
      "group": "Adverb, comparative",
      "tag": "Adverb, comparative",
      "examples": []
    },
    "RBS": {
      "group": "Adverb, superlative",
      "tag": "Adverb, superlative",
      "examples": []
    },
    "RP": {
      "group": "Particle",
      "tag": "Particle",
      "examples": []
    },
    "SYM": {
      "group": "Symbol",
      "tag": "Symbol",
      "examples": []
    },
    "TO": {
      "group": "to",
      "tag": "to",
      "examples": []
    },
    "UH": {
      "group": "Interjection",
      "tag": "Interjection",
      "examples": []
    },
    "VB": {
      "group": "Verb, base form",
      "tag": "Verb, base form",
      "examples": []
    },
    "VBD": {
      "group": "Verb, past tense",
      "tag": "Verb, past tense",
      "examples": []
    },
    "VBG": {
      "group": "Verb, gerund or present participle",
      "tag": "Verb, gerund or present participle",
      "examples": []
    },
    "VBN": {
      "group": "Verb, past participle",
      "tag": "Verb, past participle",
      "examples": []
    },
    "VBP": {
      "group": "Verb, non-3rd person singular present",
      "tag": "Verb, non-3rd person singular present",
      "examples": []
    },
    "VBZ": {
      "group": "Verb, 3rd person singular present",
      "tag": "Verb, 3rd person singular present",
      "examples": []
    },
    "WDT": {
      "group": "Wh-determiner",
      "tag": "Wh-determiner",
      "examples": []
    },
    "WP": {
      "group": "Wh-pronoun",
      "tag": "Wh-pronoun",
      "examples": []
    },
    "WP$": {
      "group": "Possessive wh-pronoun (prolog version WP-S)",
      "tag": "Possessive wh-pronoun (prolog version WP-S)",
      "examples": []
    },
    "WRB": {
      "group": "Wh-adverb",
      "tag": "Wh-adverb",
      "examples": []
    }
  }
}

},{}],26:[function(require,module,exports){
module.exports={
  "source": "https://nlp.stanford.edu/software/spanish-faq.shtml#tagset",
  "tagset": {
    "ao0000": {
      "group": "Adjectives",
      "tag": "Adjective (ordinal)",
      "examples": ["primera", "segunda", "últimos"]
    },
    "aq0000": {
      "group": "Adjectives",
      "tag": "Adjective (descriptive)",
      "examples": ["populares", "elegido", "emocionada", "andaluz"]
    },
    "cc": {
      "group": "Conjunctions",
      "tag": "Conjunction (coordinating)",
      "examples": ["y", "o", "pero"]
    },
    "cs": {
      "group": "Conjunctions",
      "tag": "Conjunction (subordinating)",
      "examples": ["que", "como", "mientras"]
    },
    "da0000": {
      "group": "Determiners",
      "tag": "Article (definite)",
      "examples": ["el", "la", "los", "las"]
    },
    "dd0000": {
      "group": "Determiners",
      "tag": "Demonstrative",
      "examples": ["este", "esta", "esos"]
    },
    "de0000": {
      "group": "Determiners",
      "tag": "Exclamative (TODO)",
      "examples": ["qué (¡Qué pobre!)"]
    },
    "di0000": {
      "group": "Determiners",
      "tag": "Article (indefinite)",
      "examples": ["un", "muchos", "todos", "otros"]
    },
    "dn0000": {
      "group": "Determiners",
      "tag": "Numeral",
      "examples": ["tres", "doscientas"]
    },
    "do0000": {
      "group": "Determiners",
      "tag": "Numeral (ordinal)",
      "examples": ["el 65 aniversario"]
    },
    "dp0000": {
      "group": "Determiners",
      "tag": "Possessive",
      "examples": ["sus", "mi"]
    },
    "dt0000": {
      "group": "Determiners",
      "tag": "Interrogative",
      "examples": ["cuántos", "qué", "cuál"]
    },
    "f0": {
      "group": "Punctuation",
      "tag": "Other",
      "examples": ["&", "@"]
    },
    "faa": {
      "group": "Punctuation",
      "tag": "Inverted exclamation mark",
      "examples": ["¡"]
    },
    "fat": {
      "group": "Punctuation",
      "tag": "Exclamation mark",
      "examples": ["!"]
    },
    "fc": {
      "group": "Punctuation",
      "tag": "Comma",
      "examples": [","]
    },
    "fca": {
      "group": "Punctuation",
      "tag": "Left bracket",
      "examples": ["["]
    },
    "fct": {
      "group": "Punctuation",
      "tag": "Right bracket",
      "examples": ["]"]
    },
    "fd": {
      "group": "Punctuation",
      "tag": "Colon",
      "examples": [":"]
    },
    "fe": {
      "group": "Punctuation",
      "tag": "Double quote",
      "examples": ["\""]
    },
    "fg": {
      "group": "Punctuation",
      "tag": "Hyphen",
      "examples": ["-"]
    },
    "fh": {
      "group": "Punctuation",
      "tag": "Forward slash",
      "examples": ["/"]
    },
    "fia": {
      "group": "Punctuation",
      "tag": "Inverted question mark",
      "examples": ["¿"]
    },
    "fit": {
      "group": "Punctuation",
      "tag": "Question mark",
      "examples": ["?"]
    },
    "fp": {
      "group": "Punctuation",
      "tag": "Period / full-stop",
      "examples": ["."]
    },
    "fpa": {
      "group": "Punctuation",
      "tag": "Left parenthesis",
      "examples": ["("]
    },
    "fpt": {
      "group": "Punctuation",
      "tag": "Right parenthesis",
      "examples": [")"]
    },
    "fra": {
      "group": "Punctuation",
      "tag": "Left guillemet / angle quote",
      "examples": ["«"]
    },
    "frc": {
      "group": "Punctuation",
      "tag": "Right guillemet / angle quote",
      "examples": ["»"]
    },
    "fs": {
      "group": "Punctuation",
      "tag": "Ellipsis",
      "examples": ["...", "etcétera"]
    },
    "ft": {
      "group": "Punctuation",
      "tag": "Percent sign",
      "examples": ["%"]
    },
    "fx": {
      "group": "Punctuation",
      "tag": "Semicolon",
      "examples": [";"]
    },
    "fz": {
      "group": "Punctuation",
      "tag": "Single quote",
      "examples": ["'"]
    },
    "i": {
      "group": "Interjections",
      "tag": "Interjection",
      "examples": ["ay", "ojalá", "hola"]
    },
    "nc00000": {
      "group": "Nouns",
      "tag": "Unknown common noun (neologism, loanword)",
      "examples": ["minidisc", "hooligans", "re-flotamiento"]
    },
    "nc0n000": {
      "group": "Nouns",
      "tag": "Common noun (invariant number)",
      "examples": ["hipótesis", "campus", "golf"]
    },
    "nc0p000": {
      "group": "Nouns",
      "tag": "Common noun (plural)",
      "examples": ["años", "elecciones"]
    },
    "nc0s000": {
      "group": "Nouns",
      "tag": "Common noun (singular)",
      "examples": ["lista", "hotel", "partido"]
    },
    "np00000": {
      "group": "Nouns",
      "tag": "Proper noun",
      "examples": ["Málaga", "Parlamento", "UFINSA"]
    },
    "p0000000": {
      "group": "Pronouns",
      "tag": "Impersonal se",
      "examples": ["se"]
    },
    "pd000000": {
      "group": "Pronouns",
      "tag": "Demonstrative pronoun",
      "examples": ["éste", "eso", "aquellas"]
    },
    "pe000000": {
      "group": "Pronouns",
      "tag": "Exclamative pronoun",
      "examples": ["qué"]
    },
    "pi000000": {
      "group": "Pronouns",
      "tag": "Indefinite pronoun",
      "examples": ["muchos", "uno", "tanto", "nadie"]
    },
    "pn000000": {
      "group": "Pronouns",
      "tag": "Numeral pronoun",
      "examples": ["dos miles", "ambos"]
    },
    "pp000000": {
      "group": "Pronouns",
      "tag": "Personal pronoun",
      "examples": ["ellos", "lo", "la", "nos"]
    },
    "pr000000": {
      "group": "Pronouns",
      "tag": "Relative pronoun",
      "examples": ["que", "quien", "donde", "cuales"]
    },
    "pt000000": {
      "group": "Pronouns",
      "tag": "Interrogative pronoun",
      "examples": ["cómo", "cuánto", "qué"]
    },
    "px000000": {
      "group": "Pronouns",
      "tag": "Possessive pronoun",
      "examples": ["tuyo", "nuestra"]
    },
    "rg": {
      "group": "Adverbs",
      "tag": "Adverb (general)",
      "examples": ["siempre", "más", "personalmente"]
    },
    "rn": {
      "group": "Adverbs",
      "tag": "Adverb (negating)",
      "examples": ["no"]
    },
    "sp000": {
      "group": "Prepositions",
      "tag": "Preposition",
      "examples": ["en", "de", "entre"]
    },
    "va00000": {
      "group": "Verbs",
      "tag": "Verb (unknown)",
      "examples": ["should"]
    },
    "vag0000": {
      "group": "Verbs",
      "tag": "Verb (auxiliary, gerund)",
      "examples": ["habiendo"]
    },
    "vaic000": {
      "group": "Verbs",
      "tag": "Verb (auxiliary, indicative, conditional)",
      "examples": ["habría", "habríamos"]
    },
    "vaif000": {
      "group": "Verbs",
      "tag": "Verb (auxiliary, indicative, future)",
      "examples": ["habrá", "habremos"]
    },
    "vaii000": {
      "group": "Verbs",
      "tag": "Verb (auxiliary, indicative, imperfect)",
      "examples": ["había", "habíamos"]
    },
    "vaip000": {
      "group": "Verbs",
      "tag": "Verb (auxiliary, indicative, present)",
      "examples": ["ha", "hemos"]
    },
    "vais000": {
      "group": "Verbs",
      "tag": "Verb (auxiliary, indicative, preterite)",
      "examples": ["hubo", "hubimos"]
    },
    "vam0000": {
      "group": "Verbs",
      "tag": "Verb (auxiliary, imperative)",
      "examples": ["haya"]
    },
    "van0000": {
      "group": "Verbs",
      "tag": "Verb (auxiliary, infinitive)",
      "examples": ["haber"]
    },
    "vap0000": {
      "group": "Verbs",
      "tag": "Verb (auxiliary, participle)",
      "examples": ["habido"]
    },
    "vasi000": {
      "group": "Verbs",
      "tag": "Verb (auxiliary, subjunctive, imperfect)",
      "examples": ["hubiera", "hubiéramos", "hubiese"]
    },
    "vasp000": {
      "group": "Verbs",
      "tag": "Verb (auxiliary, subjunctive, present)",
      "examples": ["haya", "hayamos"]
    },
    "vmg0000": {
      "group": "Verbs",
      "tag": "Verb (main, gerund)",
      "examples": ["dando", "trabajando"]
    },
    "vmic000": {
      "group": "Verbs",
      "tag": "Verb (main, indicative, conditional)",
      "examples": ["daría", "trabajaríamos"]
    },
    "vmif000": {
      "group": "Verbs",
      "tag": "Verb (main, indicative, future)",
      "examples": ["dará", "trabajaremos"]
    },
    "vmii000": {
      "group": "Verbs",
      "tag": "Verb (main, indicative, imperfect)",
      "examples": ["daba", "trabajábamos"]
    },
    "vmip000": {
      "group": "Verbs",
      "tag": "Verb (main, indicative, present)",
      "examples": ["da", "trabajamos"]
    },
    "vmis000": {
      "group": "Verbs",
      "tag": "Verb (main, indicative, preterite)",
      "examples": ["dio", "trabajamos"]
    },
    "vmm0000": {
      "group": "Verbs",
      "tag": "Verb (main, imperative)",
      "examples": ["da", "dé", "trabaja", "trabajes", "trabajemos"]
    },
    "vmn0000": {
      "group": "Verbs",
      "tag": "Verb (main, infinitive)",
      "examples": ["dar", "trabjar"]
    },
    "vmp0000": {
      "group": "Verbs",
      "tag": "Verb (main, participle)",
      "examples": ["dado", "trabajado"]
    },
    "vmsi000": {
      "group": "Verbs",
      "tag": "Verb (main, subjunctive, imperfect)",
      "examples": ["diera", "diese", "trabajáramos", "trabajésemos"]
    },
    "vmsp000": {
      "group": "Verbs",
      "tag": "Verb (main, subjunctive, present)",
      "examples": ["dé", "trabajemos"]
    },
    "vsg0000": {
      "group": "Verbs",
      "tag": "Verb (semiauxiliary, gerund)",
      "examples": ["siendo"]
    },
    "vsic000": {
      "group": "Verbs",
      "tag": "Verb (semiauxiliary, indicative, conditional)",
      "examples": ["sería", "serían"]
    },
    "vsif000": {
      "group": "Verbs",
      "tag": "Verb (semiauxiliary, indicative, future)",
      "examples": ["será", "seremos"]
    },
    "vsii000": {
      "group": "Verbs",
      "tag": "Verb (semiauxiliary, indicative, imperfect)",
      "examples": ["era", "éramos"]
    },
    "vsip000": {
      "group": "Verbs",
      "tag": "Verb (semiauxiliary, indicative, present)",
      "examples": ["es", "son"]
    },
    "vsis000": {
      "group": "Verbs",
      "tag": "Verb (semiauxiliary, indicative, preterite)",
      "examples": ["fue", "fuiste"]
    },
    "vsm0000": {
      "group": "Verbs",
      "tag": "Verb (semiauxiliary, imperative)",
      "examples": ["sea", "sé"]
    },
    "vsn0000": {
      "group": "Verbs",
      "tag": "Verb (semiauxiliary, infinitive)",
      "examples": ["ser"]
    },
    "vsp0000": {
      "group": "Verbs",
      "tag": "Verb (semiauxiliary, participle)",
      "examples": ["sido"]
    },
    "vssf000": {
      "group": "Verbs",
      "tag": "Verb (semiauxiliary, subjunctive, future)",
      "examples": ["fuere"]
    },
    "vssi000": {
      "group": "Verbs",
      "tag": "Verb (semiauxiliary, subjunctive, imperfect)",
      "examples": ["fuera", "fuese", "fuéramos"]
    },
    "vssp000": {
      "group": "Verbs",
      "tag": "Verb (semiauxiliary, subjunctive, present)",
      "examples": ["sea", "seamos"]
    },
    "w": {
      "group": "Dates",
      "tag": "Date",
      "examples": ["octubre", "jueves", "2002"]
    },
    "z0": {
      "group": "Numerals",
      "tag": "Numeral",
      "examples": ["547.000", "04", "52,52"]
    },
    "zm": {
      "group": "Numerals",
      "tag": "Numeral qualifier (currency)",
      "examples": ["dólares", "euros"]
    },
    "zu": {
      "group": "Numerals",
      "tag": "Numeral qualifier (other unites)",
      "examples": ["km", "cc"]
    },
    "word": {
      "group": "Other",
      "tag": "Emoticon or other symbol",
      "examples": [":)", "®"]
    }
  }
}

},{}],27:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _annotator = require('../annotator');

var _annotator2 = _interopRequireDefault(_annotator);

var _tokenize = require('./tokenize');

var _tokenize2 = _interopRequireDefault(_tokenize);

var _ssplit = require('./ssplit');

var _ssplit2 = _interopRequireDefault(_ssplit);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @class
 * @classdesc Class representing an RegexNERAnnotator.
 * @extends Annotator
 * @memberof CoreNLP/simple/annotator
 * @requires tokenize, ssplit, pos, regexner
 * @see {@link https://stanfordnlp.github.io/CoreNLP/regexner.html|RegexNERAnnotator}
 */
class RegexNERAnnotator extends _annotator2.default {
  /**
   * Create an Annotator
   * @param {Object} [options] a key-value map of options, without the annotator prefix
   */
  constructor(options = {}) {
    super('regexner', _extends({
      validpospattern: '(([ner:PERSON]*) /es/ /una/ /buena/ /persona/)',
      // ignorecase: null,
      // mapping: null,
      // mapping.header: null,
      // mapping.field.<fieldname>: null,
      // commonWords: null,
      // backgroundSymbol: null,
      // posmatchtype: null,
      // validpospattern: null,
      // noDefaultOverwriteLabels: null,
      verbose: true
    }, options), [new _tokenize2.default(), new _ssplit2.default()]);
  }
}

exports.default = RegexNERAnnotator;

},{"../annotator":15,"./ssplit":29,"./tokenize":30}],28:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _annotator = require('../annotator');

var _annotator2 = _interopRequireDefault(_annotator);

var _tokenize = require('./tokenize');

var _tokenize2 = _interopRequireDefault(_tokenize);

var _ssplit = require('./ssplit');

var _ssplit2 = _interopRequireDefault(_ssplit);

var _pos = require('./pos');

var _pos2 = _interopRequireDefault(_pos);

var _lemma = require('./lemma');

var _lemma2 = _interopRequireDefault(_lemma);

var _ner = require('./ner');

var _ner2 = _interopRequireDefault(_ner);

var _depparse = require('./depparse');

var _depparse2 = _interopRequireDefault(_depparse);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @class
 * @classdesc Class representing an RelationExtractorAnnotator.
 * @extends Annotator
 * @memberof CoreNLP/simple/annotator
 * @requires tokenize, ssplit, pos, lemma, ner, depparse, relation
 * @see {@link https://stanfordnlp.github.io/CoreNLP/relation.html|RelationExtractorAnnotator}
 */
class RelationExtractorAnnotator extends _annotator2.default {
  /**
   * Create an Annotator
   * @param {Object} [options] a key-value map of options, without the annotator prefix
   */
  constructor(options = {}) {
    super('relation', _extends({}, options), [new _tokenize2.default(), new _ssplit2.default(), new _pos2.default(), new _lemma2.default(), new _ner2.default(), new _depparse2.default()]);
  }
}

exports.default = RelationExtractorAnnotator;

},{"../annotator":15,"./depparse":17,"./lemma":19,"./ner":20,"./pos":24,"./ssplit":29,"./tokenize":30}],29:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _annotator = require('../annotator');

var _annotator2 = _interopRequireDefault(_annotator);

var _tokenize = require('./tokenize');

var _tokenize2 = _interopRequireDefault(_tokenize);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @class
 * @classdesc Class representing an WordsToSentenceAnnotator.
 *            Combines multiple {@link Token}s into sentences
 * @extends Annotator
 * @memberof CoreNLP/simple/annotator
 * @requires tokenize, ssplit
 * @see {@link https://stanfordnlp.github.io/CoreNLP/ssplit.html|WordsToSentenceAnnotator}
 */
class WordsToSentenceAnnotator extends _annotator2.default {
  /**
   * Create an Annotator
   * @param {Object} [options] a key-value map of options, without the annotator prefix
   */
  constructor(options = {}) {
    super('ssplit', _extends({
      eolonly: false,
      isOneSentence: false,
      newlineIsSentenceBreak: 'never',
      boundaryMultiTokenRegex: null,
      boundaryTokenRegex: '\\.|[!?]+',
      boundariesToDiscard: null,
      htmlBoundariesToDiscard: null,
      tokenPatternsToDiscard: null,
      boundaryFollowersRegex: null
    }, options), [new _tokenize2.default()]);
  }
}

exports.default = WordsToSentenceAnnotator;

},{"../annotator":15,"./tokenize":30}],30:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _annotator = require('../annotator');

var _annotator2 = _interopRequireDefault(_annotator);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @class
 * @classdesc Class representing an TokenizerAnnotator. Identifies {@link Token}s
 * @extends Annotator
 * @memberof CoreNLP/simple/annotator
 * @requires tokenize
 * @see {@link https://stanfordnlp.github.io/CoreNLP/tokenize.html|TokenizerAnnotator}
 */
class TokenizerAnnotator extends _annotator2.default {
  /**
   * Create an Annotator
   * @param {Object} [options] a key-value map of options, without the annotator prefix
   */
  constructor(options = {}) {
    super('tokenize', _extends({
      language: 'Unspecified',
      // class: null, // throws error on CoreNLP server
      whitespace: false,
      keepeol: false,
      // options: null, // throws error on CoreNLP server
      verbose: false
    }, options));
  }
}

exports.default = TokenizerAnnotator;

},{"../annotator":15}],31:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _corefMention = require('./coref-mention');

var _corefMention2 = _interopRequireDefault(_corefMention);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @class
 * @classdesc Class representing an CorefChain
 */
class CorefChain {
  /**
   * Create an CorefChain
   * @param {Array.<CorefMention>} mentions
   */
  constructor(mentions) {
    this._mentions = mentions;
  }

  /**
   * Retrieves all the contained CorefMention instances
   * @returns {Array.<CorefMention>} mentions
   */
  mentions() {
    return this._mentions;
  }

  /**
   * Retrieves a CorefMention at the index specified
   * @param {number} index
   * @returns {CorefMention} mention
   */
  mention(index) {
    return this._mentions[index];
  }

  /**
   * Retrieves the first representative mention
   * @returns {CorefMention} mention
   */
  representative() {
    return this._mentions.find(mention => mention.isRepresentativeMention());
  }

  /**
   * Retrieves all the non-representative mentions
   * @returns {Array.<CorefMention>} mentions
   */
  nonRepresentatives() {
    return this._mentions.filter(mention => !mention.isRepresentativeMention());
  }

  /**
   * Gets or sets a Document reference for the current coref-chain
   * @param {Document} doc
   * @returns {Document} doc
   */
  document(doc = null) {
    if (doc) {
      this._document = doc;
    }

    return this._document;
  }

  /**
   * Update an instance of CorefChain with Document references to Sentence(s) and their Token(s)
   * @param {Document} doc - a Document object, the same one used to generate corefs annotations
   * @returns {CorefChain} chain - The current chain instance
   */
  fromDocument(doc) {
    this._mentions.forEach(mention => {
      const sentence = doc.sentence(mention.sentNum() - 1);
      const token = sentence.token(mention.startIndex() - 1);
      mention.sentence(sentence);
      mention.token(token);
    });
    return this;
  }

  /**
   * Update an instance of CorefChain with data provided by a JSON
   * @param {Array.<CorefMentionJSON>} data - A sentence corefs mentions chain, as 
   *  returned by CoreNLP API service
   * @returns {CorefChain} chain - The current chain instance
   */
  fromJSON(data) {
    this._mentions = data.map(mention => _corefMention2.default.fromJSON(mention));
    return this;
  }

  toJSON() {
    return [...this._mentions];
  }

  /**
   * Get an instance of CorefChain from a given JSON of sentence corefs
   * @param {Array.<CorefMentionJSON>} data - The sentence corefs data, as
   *  returned by CoreNLP API service
   * @returns {CorefChain} sentenchain - A new CorefChain instance
   */
  static fromJSON(data) {
    const instance = new this();
    return instance.fromJSON(data);
  }
}

exports.default = CorefChain;

},{"./coref-mention":32}],32:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

/**
 * A CorefMention.
 * @typedef CorefMentionJSON
 * @property {number} id - Mention ID
 * @property {string} text - The text (literal word) of the mention
 * @property {number} sentNum - 1-based index of the sentence containinng this mention
 * @property {number} headIndex - 1-based index
 * @property {number} startIndex - 1-based index
 * @property {number} endIndex - 1-based index
 * @property {boolean} isRepresentativeMention - Wehther the mention word is representative or not
 * @property {("ANIMATE"|"INANIMATE"|"UNKNOWN")} animacy - Mention's animacy
 * @property {("FEMALE"|"MALE"|"NEUTRAL"|"UNKNOWN")} gender - Gender of the mention
 * @property {("SINGULAR"|"PLURAL"|"UNKNOWN")} number - Cardinality of the mention
 * @property {("PRONOMINAL"|"NOMINAL"|"PROPER"|"LIST")} type - Mention type
 * @property {Array} position - Position is a binary tuple of 
 *    (sentence number, mention number in that sentence). This is used for indexing by mention.
 * 
 * @see {@link https://github.com/stanfordnlp/CoreNLP/blob/7cfaf869f9500da16b858ab1a2835234ae46f96e/src/edu/stanford/nlp/dcoref/CorefChain.java#L148}
 * @see {@link https://github.com/stanfordnlp/CoreNLP/blob/master/src/edu/stanford/nlp/dcoref/Dictionaries.java} for enum definitions
 */

/**
 * @class
 * @classdesc Class representing an CorefMention
 */
class CorefMention {
  /**
   * Retrieves the mention ID
   * @returns {string} id
   */
  id() {
    return this._data.id;
  }

  /**
   * Retrieves the mention text
   * @returns {string} text
   */
  text() {
    return this._data.text;
  }

  /**
   * Retrieves the mention sentence number
   * @see {@link CorefMention.sentence()} for simplicity
   * @returns {number} sentNum
   */
  sentNum() {
    return this._data.sentNum;
  }

  /**
   * Retrieves the mention headIndex
   * @returns {number} headIndex
   */
  headIndex() {
    return this._data.headIndex;
  }

  /**
   * Retrieves the mention startIndex
   * @returns {number} startIndex
   */
  startIndex() {
    return this._data.startIndex;
  }

  /**
   * Retrieves the mention endIndex
   * @returns {number} endIndex
   */
  endIndex() {
    return this._data.endIndex;
  }

  /**
   * Tells you if the mentions is representative or not
   * @returns {boolean} isRepresentativeMention
   */
  isRepresentativeMention() {
    return this._data.isRepresentativeMention;
  }

  /**
   * Retrieves the mention animacy
   * @returns {("ANIMATE"|"INANIMATE"|"UNKNOWN")} animacy
   */
  animacy() {
    return this._data.animacy;
  }

  /**
   * Retrieves the mention gender
   * @returns {("FEMALE"|"MALE"|"NEUTRAL"|"UNKNOWN")} gender
   */
  gender() {
    return this._data.gender;
  }

  /**
   * Retrieves the mention number
   * @returns {("SINGULAR"|"PLURAL"|"UNKNOWN")} number
   */
  number() {
    return this._data.number;
  }

  /**
   * Retrieves the mention type
   * @returns {("PRONOMINAL"|"NOMINAL"|"PROPER"|"LIST")} type
   */
  type() {
    return this._data.type;
  }

  /**
   * Retrieves the mention's sentence container
   * @returns {Sentence} sentence
   */
  sentence(sentence = null) {
    if (sentence) {
      this._sentence = sentence;
    }

    return this._sentence;
  }

  /**
   * Retrieves the mention's associated token
   * @returns {Token} token
   */
  token(token = null) {
    if (token) {
      this._token = token;
    }

    return this._token;
  }

  /**
   * Update an instance of CorefMention with data provided by a JSON
   * @param {CorefMentionJSON} data - The mention data, as returned by CoreNLP API service
   * @returns {CorefMention} mention - The current mention instance
   */
  fromJSON(data) {
    this._data = data;
    return this;
  }

  toJSON() {
    return _extends({}, this._data);
  }

  /**
   * Get an instance of CorefMention from a given JSON
   * @param {CorefMentionJSON} data - The match data, as returned by CoreNLP API service
   * @returns {CorefMention} mention - A new CorefMention instance
   */
  static fromJSON(data) {
    const instance = new this();
    return instance.fromJSON(data);
  }
}

exports.default = CorefMention;

},{}],33:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _annotable = require('./annotable');

var _annotable2 = _interopRequireDefault(_annotable);

var _ssplit = require('./annotator/ssplit');

var _ssplit2 = _interopRequireDefault(_ssplit);

var _sentence = require('./sentence');

var _sentence2 = _interopRequireDefault(_sentence);

var _corefChain = require('./coref-chain');

var _corefChain2 = _interopRequireDefault(_corefChain);

var _coref = require('./annotator/coref');

var _coref2 = _interopRequireDefault(_coref);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * The CoreNLP API JSON structure representing a document
 * @typedef DocumentJSON
 * @property {number} index
 * @property {Array.<Sentence>} sentences
 */

/**
 * @class
 * @classdesc Class representing a Document
 * @extends Annotable
 * @memberof CoreNLP/simple
 */
class Document extends _annotable2.default {
  /**
   * Create a Document
   * @param {string} text
   */
  constructor(text) {
    super(text);
    this._sentences = [];
  }

  /**
   * Get a string representation
   * @return {string} document
   */
  toString() {
    return this._text || this._sentences.map(sent => sent.toString()).join('. ');
  }

  /**
   * Get a list of sentences
   * @returns {Array.<Sentence>} sentences - The document sentences
   */
  sentences() {
    if (!this.hasAnnotator(_ssplit2.default)) {
      throw new Error('Asked for sentences on Document, but there are unmet annotator dependencies.');
    }
    return this._sentences;
  }

  /**
   * Get the sentence for a given index
   * @param {number} index - The position of the sentence to get
   * @returns {Sentence} sentence - The document sentences
   */
  sentence(index) {
    return this.sentences()[index];
  }

  /**
   * @todo Missing implementation
   * @see https://stanfordnlp.github.io/CoreNLP/dcoref.html
   * @returns {Array.<CorefChain>}
   */
  corefs() {
    if (!this.hasAnnotator(_coref2.default)) {
      throw new Error('Asked for corefs on Document, but there are unmet annotator dependencies.');
    }
    return this._corefs;
  }

  /**
   * Get the coreference for a given index
   * @param {number} index - 0-based index of the coref chain list
   * @see https://stanfordnlp.github.io/CoreNLP/dcoref.html
   * @returns {CorefChain}
   */
  coref(index) {
    return this.corefs()[index];
  }

  /**
   * Sets the language ISO (given by the pipeline during the annotation process)
   * This is solely to keep track of the language chosen for further analysis
   * @return {string} text
   */
  setLanguageISO(iso) {
    super.setLanguageISO(iso);
    this._sentences.forEach(sentence => sentence.setLanguageISO(iso));
  }

  /**
   * Update an instance of Document with data provided by a JSON
   * @param {DocumentJSON} data - The document data, as returned by CoreNLP API service
   * @returns {Document} document - The current document instance
   */
  fromJSON(data) {
    if (data.sentences) {
      this.addAnnotator(_ssplit2.default);
      this._sentences = data.sentences.map(sent => _sentence2.default.fromJSON(sent, true));
    }
    if (data.corefs) {
      this.addAnnotator(_coref2.default);
      this._corefs = Object.keys(data.corefs).filter(chainIndex => chainIndex !== 'length').map(chainIndex => _corefChain2.default.fromJSON(data.corefs[chainIndex]).fromDocument(this));
    }
    return this;
  }

  toJSON() {
    return {
      text: this._text,
      sentences: this._sentences
    };
  }

  /**
   * Get an instance of Document from a given JSON
   * @param {DocumentJSON} data - The document data, as returned by CoreNLP API service
   * @returns {Document} document - A new Document instance
   */
  static fromJSON(data) {
    const instance = new this();
    return instance.fromJSON(data);
  }
}

exports.default = Document;

},{"./annotable":14,"./annotator/coref":16,"./annotator/ssplit":29,"./coref-chain":31,"./sentence":38}],34:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

/**
 * @typedef ExpressionSentenceMatchGroup
 * @property {string} label - group label
 * @property {number} begin - 0-based index of the matched group, relative to the given text
 * @property {number} end - 0-based index of the matched group, relative to the given text
 * @property {Token} [token] - onluy given if aggregated with an annotated Sentence or Document
 * @property {ExpressionSentenceMatchGroup} [$label] - other groups inside
 */

/**
 * A ExpressionSentenceMatch of either `TokensRegex`, `Semgrex` or `Tregex`.
 * @typedef ExpressionSentenceMatchJSON
 * @property {number} begin - word begin position, starting from zero
 * @property {number} end - word end position, starting from zero (no match ends at 0)
 * @property {string} text - matched text
 * @property {string} [$label] - any label, as defined in the expression pattern
 */

/**
 * @class
 * @classdesc Class representing an ExpressionSentenceMatch
 */
class ExpressionSentenceMatch {
  /**
   * Returns the main and labeled groups as a list of ExpressionSentenceMatchGroup
   * @returns {Array.<ExpressionSentenceMatchGroup>} groups
   */
  groups() {
    return [this._data].concat(this.labels().map(label => this.group(label)));
  }

  /**
   * Returns the labeled group as ExpressionSentenceMatchGroup from a given label
   * @description
   * Nodes in a Macthed expression can be named, we call them groups here, and
   * the labels are the name of the nodes.
   * @see {@link https://nlp.stanford.edu/nlp/javadoc/javanlp/edu/stanford/nlp/semgraph/semgrex/SemgrexPattern.html#Naming_nodes}
   * @param {string} label - The label name, not prefixed wih $
   * @returns {ExpressionSentenceMatchGroup} group
   */
  group(label) {
    return this._data[`$${label}`];
  }

  /**
   * Retrieves the list of labels (aliases) available for the current sentence match.
   * @description
   * Labels are those aliases you can add to a group match expression, for example,
   * in Semgrex, you can do {ner:/PERSON/=good_guy}, from where "good_guy" would be the label
   * and internally it will come as $good_guy as a member of {@link ExpressionSentenceMatchGroup}.
   * @returns {Array.<string>} labels
   */
  labels() {
    return Object.keys(this._data).filter(key => /^\$/.test(key)).map(label => label.substr(1));
  }

  /**
   * Update an instance of ExpressionSentenceMatch with data provided by a JSON
   * @param {ExpressionSentenceMatchJSON} data - The match data, as returned by CoreNLP API service
   * @returns {ExpressionSentenceMatch} expression - The current match instance
   */
  fromJSON(data) {
    this._data = data;
    return this;
  }

  toJSON() {
    return _extends({}, this._data);
  }

  /**
   * Get an instance of ExpressionSentenceMatch from a given JSON
   * @param {ExpressionSentenceMatchJSON} data - The match data, as returned by CoreNLP API service
   * @returns {ExpressionSentenceMatch} match - A new ExpressionSentenceMatch instance
   */
  static fromJSON(data) {
    const instance = new this();
    return instance.fromJSON(data);
  }
}

exports.default = ExpressionSentenceMatch;

},{}],35:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _expressionSentenceMatch = require('./expression-sentence-match');

var _expressionSentenceMatch2 = _interopRequireDefault(_expressionSentenceMatch);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @class
 * @classdesc Class representing an ExpressionSentence
 */
class ExpressionSentence {
  /**
   * Create an ExpressionSentence
   * @param {Array.<ExpressionSentenceMatch>} matches
   */
  constructor(matches) {
    this._matches = matches;
  }

  /**
   * Retrieves all the contained ExpressionSentenceMatch instances
   * @returns {Array.<ExpressionSentenceMatch>} matches
   */
  matches() {
    return this._matches;
  }

  /**
   * Retrieves a ExpressionSentenceMatch at the index specified
   * @param {number} index
   * @returns {ExpressionSentenceMatch} match
   */
  match(index) {
    return this._matches[index];
  }

  /**
   * Hydrates the current ExpressionSentence match groups with Token objects.
   * @description
   * The Expression / ExpressionSentence objects comes from outside the standard CoreNLP pipelines.
   * This mean that neither `TokensRegex`, `Semgrex` nor `Tregex` will tag the nodes with POS,
   * lemma, NER or any otehr annotation data.  This is sometimes a usful resource to count with, if
   * you can apart of getting the matching groups, get the annotated tokens for each word in the
   * match group.
   * @returns {ExpressionSentence} instance = The current instance
   */
  mergeTokensFromSentence(sentence) {
    const findToken = group => sentence.tokens().find(token =>
    // match group attributes with token attributes
    token.index() === group.begin + 1 && token.toString() === group.text);

    this.matches().forEach(match => match.groups().forEach(group => {
      // eslint-disable-next-line no-param-reassign
      group.token = findToken(group) || group.token;
    }));
    return this;
  }

  /**
   * Update an instance of ExpressionSentence with data provided by a JSON
   * @param {ExpressionSentenceJSON} data - The expression data, as returned by CoreNLP API service
   * @returns {ExpressionSentenceJSON} sentence - The current sentence instance
   */
  fromJSON(data) {
    this._matches = Object.keys(data).filter(matchIndex => matchIndex !== 'length').map(matchIndex => _expressionSentenceMatch2.default.fromJSON(data[matchIndex]));
    return this;
  }

  toJSON() {
    return [...this._matches];
  }

  /**
   * Get an instance of ExpressionSentence from a given JSON of sentence matches
   * @param {ExpressionSentenceJSON} data - The sentence data, as returned by CoreNLP API service
   * @returns {ExpressionSentence} sentence - A new ExpressionSentence instance
   */
  static fromJSON(data) {
    const instance = new this();
    return instance.fromJSON(data);
  }
}

exports.default = ExpressionSentence;

},{"./expression-sentence-match":34}],36:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TregexAnnotator = exports.SemgrexAnnotator = exports.TokensRegexAnnotator = undefined;

var _annotable = require('./annotable');

var _annotable2 = _interopRequireDefault(_annotable);

var _annotator = require('./annotator');

var _annotator2 = _interopRequireDefault(_annotator);

var _expressionSentence = require('./expression-sentence');

var _expressionSentence2 = _interopRequireDefault(_expressionSentence);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

class TokensRegexAnnotator extends _annotator2.default {}
exports.TokensRegexAnnotator = TokensRegexAnnotator;
class SemgrexAnnotator extends _annotator2.default {}
exports.SemgrexAnnotator = SemgrexAnnotator;
class TregexAnnotator extends _annotator2.default {}

exports.TregexAnnotator = TregexAnnotator; /**
                                            * The CoreNLP API JSON structure representing an expression
                                            * This expression structure can be found as the output of `TokensRegex`,
                                            * `Semgrex` and `Tregex`.
                                            * @typedef ExpressionJSON
                                            * @property {number} index
                                            * @property {Array.<Array.<ExpressionSentenceMatch>>} sentences
                                            */

/**
 * @class
 * @classdesc Class representing an Expression
 * @extends Annotable
 * @memberof CoreNLP/simple
 */

class Expression extends _annotable2.default {
  /**
   * Create an Expression
   * @param {string} text
   * @param {string} pattern - Either `TokensRegex`, `Semgrex` or `Tregex` valid pattern
   */
  constructor(text, pattern) {
    super(text);
    this._pattern = pattern;
    this._sentences = [];
  }

  /**
   * Get a string representation
   * @return {string} expression
   */
  toString() {
    return this._text;
  }

  /**
   * Get the pattern
   * @returns {string} pattern - The expression pattern
   */
  pattern() {
    return this._pattern;
  }

  /**
   * Get a list of sentences
   * @returns {Array.<ExpressionSentence>} sentences - The expression sentences
   */
  sentences() {
    return this._sentences;
  }

  /**
   * Get the sentence for a given index
   * @param {number} index - The position of the sentence to get
   * @returns {ExpressionSentence} sentence - An expression sentence
   */
  sentence(index) {
    return this.sentences()[index];
  }

  /**
   * Hydrate the Expression instance with Token objects from an annotated Document
   * @see {@link ExpressionSentence#mergeTokensFromSentence}
   * @param {Document} document - An annotated document from where to extract the tokens
   * @returns {Expression} expression - The current expression instance
   */
  mergeTokensFromDocument(document) {
    document.sentences().forEach((sentence, i) => this.sentence(i).mergeTokensFromSentence(sentence));
    return this;
  }

  /**
   * Update an instance of Expression with data provided by a JSON
   * @param {ExpressionJSON} data - The expression data, as returned by CoreNLP API service
   * @returns {Expression} expression - The current expression instance
   */
  fromJSON(data) {
    if (data.sentences) {
      this._sentences = data.sentences.map(sent => _expressionSentence2.default.fromJSON(sent));
    }
    return this;
  }

  toJSON() {
    return {
      text: this._text,
      sentences: this._sentences // TODO this._sentences is an array of array of objects
    };
  }

  /**
   * Get an instance of Expression from a given JSON
   * @param {ExpressionJSON} data - The expression data, as returned by CoreNLP API service
   * @returns {Expression} expression - A new Expression instance
   */
  static fromJSON(data) {
    const instance = new this();
    return instance.fromJSON(data);
  }
}

exports.default = Expression;

},{"./annotable":14,"./annotator":15,"./expression-sentence":35}],37:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _service = require('../service');

var _service2 = _interopRequireDefault(_service);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * The CoreNLP API JSON structure representing a governor
 * @typedef GovernorJSON
 * @property {string} dep
 * @property {number} governor
 * @property {string} governorGloss
 * @property {number} dependent
 * @property {string} dependentGloss
 */

/**
 * @class
 * @classdesc Class representing a Governor
 * @memberof CoreNLP/simple
 */
class Governor {
  /**
   * Create a Governor
   * @param {string} dep
   * @param {Token} dependentToken
   * @param {Token} [governorToken]
   */
  // constructor(dep, dependentToken, governorToken = Token.fromJSON({ index: 0, word: 'ROOT' })) {
  constructor(dep, dependentToken, governorToken = null) {
    this._dep = dep;
    this._dependentToken = dependentToken;
    this._governorToken = governorToken;
  }

  /**
   * Get a string representation
   * @return {string} governor
   */
  toString() {
    return this._dep;
  }

  governor() {
    return this._governorToken;
  }

  governorGloss() {
    return this._governorToken ? this._governorToken.word() : '';
  }

  dependent() {
    return this._dependentToken;
  }

  dependentGloss() {
    return this._dependentToken.word() ? this._dependentToken.word() : '';
  }

  dep() {
    return this._dep;
  }

  depInfo() {
    return _service2.default.getGovernorDepInfo(this._dep);
  }

  toJSON() {
    return {
      dep: this._dep,
      governor: this._governorToken ? this._governorToken.index() : 0,
      governorGloss: this._governorToken ? this._governorToken.word() : 'ROOT',
      dependent: this._dependentToken.index(),
      dependentGloss: this._dependentToken.word()
    };
  }

  /**
   * Get an instance of Governor from a given JSON
   *
   * @todo It is not possible to properly generate a Governor from a GovernorJSON
   *       the Governor requires references to the Token instances in order to work
   * @param {GovernorJSON} data - The token data, as returned by CoreNLP API service
   * @returns {Governor} governor - A new Governor instance
   */
  static fromJSON() {
    throw Error('Not implemented');
  }
}

exports.default = Governor;

},{"../service":13}],38:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _lodash = require('lodash.head');

var _lodash2 = _interopRequireDefault(_lodash);

var _annotable = require('./annotable');

var _annotable2 = _interopRequireDefault(_annotable);

var _tokenize = require('./annotator/tokenize');

var _tokenize2 = _interopRequireDefault(_tokenize);

var _parse = require('./annotator/parse');

var _parse2 = _interopRequireDefault(_parse);

var _depparse = require('./annotator/depparse');

var _depparse2 = _interopRequireDefault(_depparse);

var _token = require('./token');

var _token2 = _interopRequireDefault(_token);

var _governor = require('./governor');

var _governor2 = _interopRequireDefault(_governor);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * The CoreNLP API JSON structure representing a sentence
 * @typedef SentenceJSON
 * @property {number} index - 1-based index, as they come indexed by StanfordCoreNLP
 * @property {Array.<Token>} tokens
 */

/**
 * @class
 * @classdesc Class representing a Sentence
 * @extends Annotable
 * @memberof CoreNLP/simple
 * @see {@link https://github.com/stanfordnlp/CoreNLP/blob/master/src/edu/stanford/nlp/simple/Sentence.java}
 */
class Sentence extends _annotable2.default {
  /**
   * Create a Sentence
   * @param {string} text
   */
  constructor(text) {
    super(text);
    this._tokens = [];
    this._governors = [];
  }

  /**
   * Get a string representation
   * @returns {string} sentence
   */
  toString() {
    return this._text || this._tokens.map(token => token.toString()).join(' ');
  }

  /**
   * Get the index relative to the parent document
   * @returns {number} index
   */
  index() {
    return this._index;
  }

  /**
   * Get a string representation of the parse tree structure
   * @returns {string} parse
   */
  parse() {
    return this._parse;
  }

  /**
   * Get an array of string representations of the sentence words
   * @requires {@link TokenizerAnnotator}
   * @throws {Error} in case the require annotator was not applied to the sentence
   * @returns {Array.<string>} words
   */
  words() {
    if (!this.hasAnnotator(_tokenize2.default)) {
      throw new Error('Asked for words on Sentence, but there are unmet annotator dependencies.');
    }
    return this._tokens.map(token => token.word());
  }

  /**
   * Get a string representations of the Nth word of the sentence
   * @requires {@link TokenizerAnnotator}
   * @throws {Error} in case the require annotator was not applied to the sentence
   * @throws {Error} in case the token for the given index does not exists
   * @param {number} index - 0-based index as they are arranged naturally
   * @returns {string} word
   */
  word(index) {
    if (!this.hasAnnotator(_tokenize2.default)) {
      throw new Error('Asked for a word on Sentence, but there are unmet annotator dependencies.');
    }
    if (!this._tokens[index]) {
      throw new Error(`Sentence instance does not contain a token with index ${index}`);
    }
    return this._tokens[index].word();
  }

  [Symbol.iterator]() {
    return this._tokens.values();
  }

  /**
   * Get a string representations of the tokens part of speech of the sentence
   * @returns {Array.<string>} posTags
   */
  posTags() {
    return this._tokens.map(token => token.pos());
  }

  /**
   * Get a string representations of the Nth token part of speech of the sentence
   * @throws {Error} in case the token for the given index does not exists
   * @param {number} index - 0-based index as they are arranged naturally
   * @returns {string} posTag
   */
  posTag(index) {
    if (!this._tokens[index]) {
      throw new Error(`Sentence instance does not contain a token with index ${index}`);
    }
    return this._tokens[index].pos();
  }

  /**
   * Get a string representations of the tokens lemmas of the sentence
   * @returns {Array.<string>} lemmas
   */
  lemmas() {
    return this._tokens.map(token => token.lemma());
  }

  /**
   * Get a string representations of the Nth token lemma of the sentence
   * @throws {Error} in case the token for the given index does not exists
   * @param {number} index - 0-based index as they are arranged naturally
   * @returns {string} lemma
   */
  lemma(index) {
    if (!this._tokens[index]) {
      throw new Error(`Sentence instance does not contain a token with index ${index}`);
    }
    return this._tokens[index].lemma();
  }

  /**
   * Get a string representations of the tokens nerTags of the sentence
   * @returns {Array.<string>} nerTags
   */
  nerTags() {
    return this._tokens.map(token => token.ner());
  }

  /**
   * Get a string representations of the Nth token nerTag of the sentence
   * @throws {Error} in case the token for the given index does not exists
   * @param {number} index - 0-based index as they are arranged naturally
   * @returns {string} nerTag
   */
  nerTag(index) {
    if (!this._tokens[index]) {
      throw new Error(`Sentence instance does not contain a token with index ${index}`);
    }
    return this._tokens[index].ner();
  }

  /**
   * Get a list of annotated governors by the dependency-parser
   * @requires {@link DependencyParseAnnotator}
   * @throws {Error} in case the require annotator was not applied to the sentence
   * @returns {Array.<Governor>} governors
   */
  governors() {
    if (!this.hasAnnotator(_depparse2.default)) {
      throw new Error('Asked for governors on Sentence, but there are unmet annotator dependencies.');
    }
    return this._governors;
  }

  /**
   * Get the N-th annotated governor by the dependency-parser annotator
   * @requires {@link DependencyParseAnnotator}
   * @throws {Error} in case the require annotator was not applied to the sentence
   * @returns {Governor} governor
   */
  governor(index) {
    if (!this.hasAnnotator(_depparse2.default)) {
      throw new Error('Asked for a governor on Sentence, but there are unmet annotator dependencies.');
    }
    return this._governors[index];
  }

  // TODO
  // eslint-disable-next-line class-methods-use-this, no-unused-vars
  incommingDependencyLabel(index) {}

  // TODO
  // eslint-disable-next-line class-methods-use-this
  natlogPolarities() {}

  // TODO
  // eslint-disable-next-line class-methods-use-this, no-unused-vars
  natlogPolarity(index) {}

  // TODO
  // eslint-disable-next-line class-methods-use-this
  openie() {}

  // TODO
  // eslint-disable-next-line class-methods-use-this, no-unused-vars
  openieTriples(index) {}

  /**
   * Get an array of token representations of the sentence words
   * @requires {@link TokenizerAnnotator}
   * @throws {Error} in case the require annotator was not applied to the sentence
   * @returns {Array.<Token>} tokens
   */
  tokens() {
    if (!this.hasAnnotator(_tokenize2.default)) {
      throw new Error('Asked for tokens on Sentence, but there are unmet annotator dependencies.');
    }
    return this._tokens;
  }

  /**
   * Get the Nth token of the sentence
   * @requires {@link TokenizerAnnotator}
   * @throws {Error} in case the require annotator was not applied to the sentence
   * @returns {Token} token
   */
  token(index) {
    if (!this.hasAnnotator(_tokenize2.default)) {
      throw new Error('Asked for a token on Sentence, but there are unmet annotator dependencies.');
    }
    return this._tokens[index];
  }

  // TODO
  // eslint-disable-next-line class-methods-use-this
  algorithms() {}

  /**
   * Sets the language ISO (given by the pipeline during the annotation process)
   * This is solely to keep track of the language chosen for further analysis
   * @return {string} text
   */
  setLanguageISO(iso) {
    super.setLanguageISO(iso);
    this._tokens.forEach(token => token.setLanguageISO(iso));
  }

  /**
   * Get a JSON representation of the current sentence
   * @description
   * The following arrow function `data => Sentence.fromJSON(data).toJSON()` is idempontent, if
   * considering shallow comparison, not by reference.
   * This JSON will respects the same structure as it expects from {@see Sentence#fromJSON}.
   * @returns {SentenceJSON} data
   */
  toJSON() {
    let json = {
      index: this._index,
      tokens: this._tokens.map(token => token.toJSON()),
      basicDependencies: this._governors.map(governor => governor.toJSON()),
      enhancedDependencies: this._enhancedDependencies,
      enhancedPlusPlusDependencies: this._enhancedPlusPlusDependencies
    };

    if (this._parse) {
      json = _extends({}, json, { parse: this._parse });
    }
    return json;
  }

  /**
   * Update an instance of Sentence with data provided by a JSON
   * @param {SentenceJSON} data - The document data, as returned by CoreNLP API service
   * @param {boolean} [isSentence] - Indicate if the given data represents just the sentence
   * or a full document with just a sentence inside
   * @returns {Sentence} sentence - The current sentence instance
   */
  fromJSON(data, isSentence = false) {
    const sentence = isSentence ? data : (0, _lodash2.default)(data.sentences);
    this._index = data.index;
    if (sentence.tokens) {
      this.addAnnotator(_tokenize2.default);
      this._tokens = sentence.tokens.map(tok => _token2.default.fromJSON(tok));
    }
    if (sentence.parse) {
      this.addAnnotator(_parse2.default);
      this._parse = sentence.parse;
    }
    if (sentence.basicDependencies) {
      this.addAnnotator(_depparse2.default);
      this._governors = sentence.basicDependencies.map(gov => new _governor2.default(gov.dep, this._tokens[gov.dependent - 1], this._tokens[gov.governor - 1]));
      // @see relation annotator...
      this._basicDependencies = sentence.basicDependencies;
      this._enhancedDependencies = sentence.enhancedDependencies;
      this._enhancedPlusPlusDependencies = sentence.enhancedPlusPlusDependencies;
    }
    return this;
  }

  /**
   * Get an instance of Sentence from a given JSON
   * @param {SentenceJSON} data - The document data, as returned by CoreNLP API service
   * @param {boolean} [isSentence] - Indicate if the given data represents just the sentence of a
   * full document
   * @returns {Sentence} document - A new Sentence instance
   */
  static fromJSON(data, isSentence = false) {
    const instance = new this();
    return instance.fromJSON(data, isSentence);
  }
}

exports.default = Sentence;

},{"./annotable":14,"./annotator/depparse":17,"./annotator/parse":21,"./annotator/tokenize":30,"./governor":37,"./token":39,"lodash.head":4}],39:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _service = require('../service');

var _service2 = _interopRequireDefault(_service);

var _annotable = require('./annotable');

var _annotable2 = _interopRequireDefault(_annotable);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

// @see {@link https://github.com/stanfordnlp/CoreNLP/blob/master/src/edu/stanford/nlp/simple/Token.java}

/**
 * The CoreNLP API JSON structure representing a token
 * @typedef TokenJSON
 * @property {number} index
 * @property {string} word
 * @property {string} originalText
 * @property {number} characterOffsetBegin
 * @property {number} characterOffsetEnd
 * @property {string} before
 * @property {string} after
 */

/**
 * @typedef PosInfo
 * @description
 * PosInfo does not come as part of the CoreNLP.  It is an indexed reference of POS tags
 * by language provided by this library.  It's only helpful for analysis and study.  The
 * data was collected from different documentation resources on the Web.
 * The PosInfo may vary depending on the POS annotation types used, for example, CoreNLP
 * for Spanish uses custom POS tags developed by Stanford, but this can also be changed
 * to Universal Dependencies, which uses different tags.
 * @property {string} group
 * @property {string} tag
 * @property {Array.<string>} examples
 */

/**
 * @class
 * @classdesc Class representing a Token
 * @extends Annotable
 * @memberof CoreNLP/simple
 */
class Token extends _annotable2.default {
  /**
   * Create a Token
   * @param {string} word
   */
  // eslint-disable-next-line no-useless-constructor
  constructor(word) {
    super(word);
  }

  /**
   * Get a string representation
   * @returns {string} token
   */
  toString() {
    return this._text;
  }

  /**
   * Get the `inde ` number associated by the StanfordCoreNLP
   * This index is relative to the sentence it belongs to, and is a 1-based (possitive integer).
   * This number is useful to match tokens within a sentence for depparse, coreference, etc.
   * @returns {number} index
   */
  index() {
    return this._index;
  }

  /**
   * Get the original word
   * @returns {string} word
   */
  word() {
    return this._text;
  }

  /**
   * Get the original text
   * @returns {string} originalText
   */
  originalText() {
    return this._originalText;
  }

  /**
   * Get the characterOffsetBegin relative to the parent sentence
   * @description
   * A 0-based index of the word's initial character within the sentence
   * @returns {number} characterOffsetBegin
   */
  characterOffsetBegin() {
    return this._characterOffsetBegin;
  }

  /**
   * Get the characterOffsetEnd relative to the parent sentence
   * A 0-based index of the word's ending character within the sentence
   * @returns {number} characterOffsetEnd
   */
  characterOffsetEnd() {
    return this._characterOffsetEnd;
  }

  /**
   * Get the `before` string relative to the container sentence
   * @returns {string} before
   */
  before() {
    return this._before;
  }

  /**
   * Get the `after` string relative to the container sentence
   * @returns {string} after
   */
  after() {
    return this._after;
  }

  /**
   * Get the annotated lemma
   * @returns {string} lemma
   */
  lemma() {
    return this._lemma;
  }

  /**
   * Get the annotated part-of-speech for the current token
   * @returns {string} pos
   */
  pos() {
    return this._pos;
  }

  /**
   * Get additional metadata about the POS annotation
   * NOTE: Do not use this method other than just for study or analysis purposes.
   * @see {@link PosInfo} for more details
   * @returns {PosInfo} posInfo
   */
  posInfo() {
    return _service2.default.getTokenPosInfo(this._pos, this.getLanguageISO());
  }

  /**
   * Get the annotated named-entity for the current token
   * @returns {string} ner
   */
  ner() {
    return this._ner;
  }

  /**
   * Get the annotated speaker for the current token
   * @see {@link CorefAnnotator}
   * @returns {string} speaker
   */
  speaker() {
    return this._speaker;
  }

  /**
   * Get a JSON representation of the current token
   * @description
   * The following arrow function `data => Token.fromJSON(data).toJSON()` is idempontent, if
   * considering shallow comparison, not by reference.
   * This JSON will respects the same structure as it expects from {@see Token#fromJSON}.
   * @returns {TokenJSON} data
   */
  toJSON() {
    return {
      index: this._index,
      word: this._text,
      originalText: this._originalText,
      characterOffsetBegin: this._characterOffsetBegin,
      characterOffsetEnd: this._characterOffsetEnd,
      before: this._before,
      after: this._after,
      // annotations metadata
      pos: this._pos,
      lemma: this._lemma,
      ner: this._ner,
      speaker: this._speaker
    };
  }

  /**
   * Get an instance of Token from a given JSON
   * @param {TokenJSON} data - The token data, as returned by CoreNLP API service
   * @returns {Token} token - A new Token instance
   */
  static fromJSON(data) {
    const instance = new this();
    instance._index = data.index;
    instance._text = data.word;
    instance._originalText = data.originalText;
    instance._characterOffsetBegin = data.characterOffsetBegin;
    instance._characterOffsetEnd = data.characterOffsetEnd;
    instance._before = data.before;
    instance._after = data.after;
    // annotations metadata
    instance._pos = data.pos;
    instance._lemma = data.lemma;
    instance._ner = data.ner;
    instance._speaker = data.speaker;
    return instance;
  }
}

exports.default = Token;

},{"../service":13,"./annotable":14}],40:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Node = undefined;

var _service = require('../service');

var _service2 = _interopRequireDefault(_service);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @class
 * @classdesc Class representing a Sentence Tree Node
 */
class Node {
  constructor(pos = '', word = '', children = [], paren = null) {
    this._pos = pos;
    this._word = word;
    this._children = children;
    this._parent = paren;
  }

  pos() {
    return this._pos;
  }

  posInfo() {
    // itf it's a subtree
    if (this._children.length) {
      return _service2.default.getSentenceParseInfo(this._pos, this.getLanguageISO());
    }

    // if it's a leaf
    return _service2.default.getTokenPosInfo(this._pos, this.getLanguageISO());
  }

  token(token = null) {
    if (token) {
      this._token = token;
    }

    return this._token;
  }

  word() {
    return this._word;
  }

  /**
   * Sets the language ISO (given by the pipeline during the annotation process)
   * This is solely to keep track of the language chosen for further analysis
   * @return {string} text
   */
  setLanguageISO(iso) {
    this._language = iso;
  }

  /**
   * Retrieves the language ISO
   * @return {string} text
   */
  getLanguageISO() {
    return this._language;
  }

  children() {
    return this._children;
  }

  appendChild(node) {
    this._children.push(node);
  }

  parent(paren = null) {
    if (paren) {
      this._parent = paren;
    }

    return this._parent;
  }

  toJSON() {
    return {
      pos: this._pos,
      posInfo: this.posInfo(),
      word: this._word,
      token: this._token,
      children: this._children
    };
  }
}

exports.Node = Node; /**
                      * @class
                      * @classdesc Class representing a Parse tree structure
                      * @memberof CoreNLP/util
                      * @description
                      * The nodes are given in order left to right as the words in a sentence appears
                      * The leaves are grouped into semantic representations provided by the Annotator
                      * This class is pretty useful to use along with the ParserAnnotator
                      * @see inspired on {@link http://www.nltk.org/howto/tree.html|Tree}
                      * @see the lecture {@link http://www.cs.cornell.edu/courses/cs474/2004fa/lec1.pdf|Tree Syntax of Natural Language}
                      */

class Tree {
  /**
   * Create a Tree
   * @param {Node} node
   */
  constructor(node) {
    this.rootNode = node;
  }

  /**
   * Get a Tree string representation for debugging purposes
   * @returns {string} tree
   */
  dump() {
    return JSON.stringify(this.rootNode, (key, val) => {
      if (val instanceof Node) {
        return val.children().length ? {
          pos: val.pos(),
          info: val.info(),
          children: val.children()
        } : {
          pos: val.pos(),
          info: val.info(),
          word: val.word(),
          token: val.token()
        };
      }

      return val;
    }, 2);
  }

  /**
   * Performs Deep-first Search calling a visitor for each node
   * @see {@link https://en.wikipedia.org/wiki/Depth-first_search|DFS}
   */
  visitDeepFirst(visitor, node = this.rootNode) {
    node.children().forEach(childNode => {
      this.visitDeepFirst(visitor, childNode);
      visitor(childNode);
    });

    visitor(node);
  }

  /**
   * Performs Deep-first Search calling a visitor for each node, from right to left
   * @see {@link https://en.wikipedia.org/wiki/Depth-first_search|DFS}
   */
  visitDeepFirstRight(visitor, node = this.rootNode) {
    node.children().reverse().forEach(childNode => {
      this.visitDeepFirstRight(visitor, childNode);
      visitor(childNode);
    });

    visitor(node);
  }

  /**
   * Performs Deep-first Search calling a visitor only over leaves
   * @see {@link https://en.wikipedia.org/wiki/Depth-first_search|DFS}
   */
  visitLeaves(visitor, node = this.rootNode) {
    node.children().forEach(childNode => {
      if (childNode.children().length) {
        this.visitLeaves(visitor, childNode);
      } else {
        visitor(childNode);
      }
    });

    if (!node.children().length) {
      visitor(node);
    }
  }

  /**
   * @param {Sentence} sentence
   * @param {boolean} [doubleLink] whether the child nodes should have a reference
   * to their parent or not - this allows the use of {@link Node.parent()}
   * @returns {Tree} tree
   */
  static fromSentence(sentence, doubleLink = false) {
    const parse = sentence.parse();
    if (!parse) {
      throw new Error('Unable to create Tree from Sentence, did you run ParserAnnotator first?');
    }

    const tree = Tree.fromString(parse, doubleLink);
    // link nodes with tokens
    let visitedLeaves = 0;
    // eslint-disable-next-line no-plusplus
    tree.visitLeaves(node => node.token(sentence.token(visitedLeaves++)));

    const languageIso = sentence.getLanguageISO();
    if (languageIso) {
      tree.visitDeepFirst(node => node.setLanguageISO(languageIso));
    }

    return tree;
  }

  /**
   * @param {string} str
   * @param {boolean} [doubleLink] whether the child nodes should have a reference
   * to their parent or not - this allows the use of {@link Node.parent()}
   * @returns {Tree} tree
   */
  static fromString(str, doubleLink = false) {
    return new Tree(this._transformTree(this._buildTree(str), doubleLink));
  }

  static _buildTree(str) {
    let currentNode = { children: [] };
    const openNodes = [currentNode];
    const l = str.length;
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < l; i++) {
      if (str[i] === '(') {
        currentNode = { str: '', children: [] };
        openNodes[openNodes.length - 1].children.push(currentNode);
        openNodes.push(currentNode);
      } else if (str[i] === ')') {
        this._cleanNode(currentNode);
        openNodes.pop();
        currentNode = openNodes[openNodes.length - 1];
      } else {
        currentNode.str += str[i];
      }
    }
    return currentNode.children[0];
  }

  static _cleanNode(node) {
    const str = node.str.trim();
    const delimiterPos = str.indexOf(' ');
    if (delimiterPos > -1) {
      // eslint-disable-next-line no-param-reassign
      node.pos = str.substr(0, delimiterPos);
      // eslint-disable-next-line no-param-reassign
      node.word = str.substr(delimiterPos + 1);
    } else {
      // eslint-disable-next-line no-param-reassign
      node.pos = str;
    }
  }

  static _transformTree(node, doubleLink) {
    if (doubleLink) {
      const parentNode = new Node(node.pos, node.word);
      node.children.forEach(n => {
        const childNode = this._transformTree(n);
        childNode.parent(parentNode);
        parentNode.appendChild(childNode);
      });

      return parentNode;
    }

    return new Node(node.pos, node.word, node.children.map(n => this._transformTree(n)));
  }
}

exports.default = Tree;

},{"../service":13}]},{},[9])(9)
});